\documentclass[a4paper]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{hyperref}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{apacite}
\usepackage{multicol}
\usepackage{bbm}

\title{Fast shared response model for fMRI data}
\author{Hugo Richard, Bertrand Thirion, Jonathan Pillow}

\begin{document}
\maketitle

\begin{abstract}
When exposed to naturalistic stimuli (i.e. movie watching), subjects' experience is closer to their every-day life. Although it is our concern to understand how the brain reacts in real life,the recorded brain activity is difficult to interpret and differs a lot across subjects. 

The shared response model (SRM) \cite{chen2015reduced} extracts a common response from different subjects exposed to the same stimuli. However for a large dataset, computation requires a large amount of memory and computational power which limits the use of the algorithm in practice. 

In this work, we introduce the FastSRM algorithm. It has comparable performance than the original SRM algorithm while providing considerable speed-up in time and memory usage.

The code is freely available at \url{____.com}.
\end{abstract}

\section{Introduction}
\begin{itemize}
\item SRM allows us to projects subjects data in a low dimensional shared space in which inter-subject variability is reduced
\item Particularly useful for the analysis of realistic stimuli (movie, audio track,...) \cite{vodrahalli2018mapping}
\item One spatial map per subject
\item \cite{chen2015reduced} makes SRM work on ROIs.
\item \cite{anderson2016enabling} makes it scalable with the number of voxels
\item We make it even faster: from 2 hours to 12 min and 10Gb consumption to less than 2Gb. Now computation can be performed on a modern laptop.
\item These computations usually need to be performed several times (for example inside a cross validation loop to set the optimal number of components for SRM) so it is really useful that they are fast. 
\end{itemize}

\section{Related work}
\begin{itemize}
\item Work around template estimation hyperalignment \cite{guntupalli2016model}, work of thomas baseille
\item Variants of SRM includes \cite{chen2016convolutional} with autoencoder, \cite{shvartsman2017matrix} using matrix normal priors, SRM with individual component \cite{turek2018capturing}, Semi-supervised SRM \cite{turek2017semi}
\item Works about atlases include \cite{mensch2018extracting}, \cite{bellec2010multi}, \cite{schaefer2017local}
\end{itemize}

\section{The shared response model (SRM)}
The shared response model is a latent factor model. The brain images of subject $i$ during session $s$ are stored in a matrix $X_{s,i} \in\mathbb{R}^{T, V}$ where $V$ is the number of voxels and $T$ the number of timeframes (the number of acquired brain images). Each brain image $X_{s,i}[t]$ of subject $i$ during session $s$ at time $t$ is seen as a weighted sum of its $K$ orthogonal spatial maps stored in $W_i \in\mathbb{R}^{K, V}$. The weights $S_s \in \mathbb{R}^{T, K}$ are shared between subjects but depend on sessions. 

Keeping things simple, we assume in this paper that all $N$ subjects have the same number $V$ of voxels and all $M$ sessions have the same number $T$ of timeframes. The extension to the more general case where each session has its own number of timeframe and each subject its own number of voxel is straightforward. 

In practice the same masker is used for all subjects so all subjects do have the same number of voxels. In our implementation the number of voxels per subject is the same but the number of timeframes per sessions can vary.

Typicall values for the number of voxels $V$, the number of timeframes $T$, the number of sessions $M$, the number of subjects $N$ and the number of components are:

\begin{tabular}{ccccc}
$V \simeq 10^5, 10^6$ & $T \simeq 10^3 $ & $M \simeq 10^1$ & $N \simeq 10^1, 10^2$ & $K \simeq 10^1, 10^2$
\end{tabular}

\begin{itemize}

\item The concatenation $X \in \mathbb{R}^{MT, NV}$ of the brain acquisition of all subjects for all sessions:
\begin{equation*}
X = 
\begin{bmatrix}
    X_{1,1} & X_{1,2} & \cdots & X_{1,N} \\
    X_{2,1} & X_{2,2} & \cdots & X_{2,N}\\
    \vdots & \vdots &\ddots & \vdots\\
    X_{M,1} & X_{M,2} & \cdots & X_{M,N} \\
\end{bmatrix}
\end{equation*}
where $X_{s, i} \in \mathbb{R}^{T, V}$ contains the brain activity of subject $i$ during session $s$.

\item The concatenation $W \in \mathbb{R}^{K, NV}$ of the spatial maps of all subjects:
\begin{equation*}
W = 
\begin{bmatrix}
    W_{1} &  W_{2} & \cdots & W_{N}
\end{bmatrix}
\end{equation*}
where $W_{i} \in \mathbb{R}^{K, V}$ contains the $K$ spatial maps of subject $i$.

\item The concatenation of the weights $S \in \mathbb{MT, K}$ of all sessions:
\begin{equation*}
S = 
\begin{bmatrix}
    S_{1} \\
    S_{2} \\
    \vdots \\
    S_{M}
\end{bmatrix}
\end{equation*}
where $S_{s} \in \mathbb{R}^{T, K}$ contains the $K$ weights of session $s$ across time.  

\item For each subject $i$, the concatenation $X_i \in \mathbb{R}^{MT, V}$ of the acquisition data for all sessions:
\begin{equation*}
X_i = 
\begin{bmatrix}
	X_{1, i} \\
	X_{2, i} \\
	\vdots \\
	X_{M, i}
\end{bmatrix}
\end{equation*}
\end{itemize}

Formally any shared response model is defined by: 
\[
X = SW + \epsilon
\]

where the model for the spatial maps $W$, the shared response $S$ and the noise $\epsilon$ needs to be defined. Figure~\ref{fig:conceptual_figure1} illustrates how shared response models are defined.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/conceptual_figure3}
\caption{The raw fmri data are seen as a weighted combination of subject specific spatial maps with additive noise. The weights are shared between subjects and constitute the shared response to the stimuli.}
\label{fig:conceptual_figure1}
\end{figure}


\subsection{Deterministic SRM model (DetSRM)}
The deterministic orthogonal SRM model assumes Gaussian noise with the same variance for all subjects.

Formally the model reads:
\begin{align*}
&X_{i}[t] \sim \mathcal{N}(S[t] W_i, \sigma^2) \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_i W_i^T = I_K
\end{align*}
where $I_K \in \mathbb{R}^K$ is the identity matrix.

Minimizing the log-likelihood we obtain the following optimization problem:
\begin{align*}
&\text{argmin}_{W_1, \cdots, W_N, S} \sum_{i=1}^N ||X_{i} - SW_i||^2 \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_i^T W_i = I_K
\end{align*}

This can be solved efficiently using alternate minimization on 
$(W_1, \cdots , W_N )$ and $S$. At each iteration we have 2 problems to solve that have a closed form solution:
\begin{align*}
\forall k, &\text{argmin}_{W_k, W_k^TW_k = I_K} \sum_{i=1}^N ||X_{i} - SW_i||^2 = U_k V_k  \\
& \text{where} \ U_k, D_k, V_k = SVD(S^T X_k)
\end{align*}
where $SVD$ means singular value decomposition

and
\begin{align*}
&\text{argmin}_{S} \sum_{i=1}^N ||X_{i} - SW_i||^2 = \frac{\sum_{i=1}^N X_i W_i^T}{N}  \\
\end{align*}


Assuming $K$ is small, the time-complexity of such approach is in $O(n_{iter} N MT V K)$ and storage requirements are in $O(VNT)$. For computing the storage requirements we assumed that only one subject and one session is loaded at a time. 
In our experiments we chose $n_{iter}=10$.

\subsection{Probabilistic SRM model (ProbSRM)}
In the probabilistic model the shared response is modeled by its covariance matrix $\Sigma_s$ and the variance of the Gaussian noise $\sigma_1, \cdots , \sigma_N$ is assumed different for different subjects. In the original paper, an intercept is also learned but since we remove the mean of each time-course as a preprocessing step it is of no use here.

Formally the model reads:
\begin{align*}
&S[t] \sim \mathcal{N}(0, \Sigma_s) \\
&X_i[t] \sim \mathcal{N}(S[t]W_i, \sigma_i^2 I_K) \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_iW_i^T = I_K
\end{align*}

The optimization is done using an expectation maximization algorithm (see \cite{anderson2016enabling}) where the E-step is given by:

\begin{align*}
&\sigma_0 = \sum_{i=1}^N \sigma_i^{-2} \\
&\Psi = diag(\sigma_1 I_V, ..., \sigma_N I_V) \\
&G = \Sigma_s^T [I_K - \sigma_0 (\Sigma_s^{-1} + \sigma_0 I_K)^{-1}]W \Psi^{-1} \\
&E[S[t]] = G X[t] \\
&E[S[t]S[t]^T] =\Sigma_s - G W^T \Sigma_s + E[S[t]] E[S[t]]^T \\
\end{align*}

and the parameters are updated during the M-step by:
\begin{align*}
&A_i = 0.5 X_i[t] E[S[t]]^T \\
&W_i = (A_i^T A_i)^{-\frac{1}{2}} A_i^T \\
&\sigma_i^2 = \frac{1}{MTV} (||X_i||^2 + \sum_{t=1}^{TM} tr(E[S[t]S[t]^T]) - 2\sum_{t=1}^{TM} <X_i[t] | W_i^T E[S[t]]> \\
& \Sigma_s = \frac{1}{MT} E[S[t]S[t]^T]
\end{align*}

Assuming $K$ is small, the time-complexity of this approach is in $O(n_{iter} K NV TM)$ and storage requirements are in $O(NVK)$. For computing the storage requirements we assumed that only one subject and one session is loaded at a time. In our experiments we chose $n_{iter}=10$.


The current implementation of ProbSRM and DetSRM in brainiak \cite{brainiak} loads all subjects in memory and finds the basis of all subject in parallel. We slightly modified the implementation so that only one subject is loaded at a time (otherwise we lack memory when processing large datasets).

\section{FastSRM model}
When we deal with large dataset ($N, M, V, T$ large), above implementations require huge computational power. We introduce FastSRM, a faster and more memory-efficient algorithm.

In a first step, we project our data $X$ on a chosen atlas $A$ with $C$ regions. The atlas $A \in \mathbb{R}^{C, V}$ can be either probabilistic or deterministic but the number of regions $C$ of the atlas should be big compared to the number of components $K$ of the FastSRM model and small compared to the number $V$ of voxels. In typical settings $C \simeq 10^2, 10^3$.

Projection onto the atlas yields reduced data $\hat{X} \in \mathbb{R}^{TM, NC}$.

\[
\forall i \in \{1 \cdots N\}, s \in \{1 \cdots M \} \ \hat{X}_{i, s} = X_{i,s} A^T (A A^T)^{-1}
\]

In cases where the atlas is deterministic projecting onto the atlas is equivalent to averaging brain activation in each region of the atlas. 
Formally, denoting $R_k$ the region $k$ of atlas $A$, we compute the projection over the atlas by: 
\[
\forall i \in \{1 \cdots N\}, s \in \{1 \cdots M \} \ \hat{X}_{i, s}[t, k] = \frac{\sum_{v \in R_k} X_{i,s}[t, v]}{|R_k|}
\]

In a second step we apply our preferred SRM algorithm on reduced data to find the shared response $S$ (in our implementation we use a deterministic SRM). Since $C$ is small compared to $V$, this step is very fast even if the number of iteration is high.
 
\[
S = DetSRM(\hat{X})
\]

If needed we compute the spatial maps of each subjects using the shared response $S$ and the data $X$ using:
\begin{align*}
&\forall k, W_k= U_k V_k  \\
& \text{where} \ U_k, D_k, V_k = SVD(\sum_{s=1}^M S[s]^T \hat{X}_{s, k})
\end{align*}

In our implementation, we use subjects and sessions one by one trading time for memory-efficiency. Again for memory efficiency reasons, we write each basis on the disk during their computation. 

The time complexity is $O(N M T V (C + K))$ in case of a probabilistic atlas but only $O(N M T V K)$ in case of a deterministic atlas. Note that the complexity does not depend any more on the number of iterations.

The memory complexity is $O(V (C + T)$ in case of a probabilistic atlas but only $O(VT)$ in case of a deterministic atlas. 




\begin{figure}
\centering
\includegraphics[scale=0.4]{figures/conceptual_figure2}
\caption{In a first step, data are projected onto an atlas. In a second step we apply an SRM algorithm on reduced data to compute the shared response. If needed spatial maps can be recovered from the shared response using the full data.}
\label{fig:conceptual_figure2}
\end{figure}
 


\section{Experiments}
\subsection{Dataset}
The data of sherlock, raiders and gallant datasets were acquired at Neurospin using a 3T scanner which has a spatial resolution of 1.5mm (more details in \cite{ibc}. Data from the Forrest dataset were acquired using a 7T scanner with a spatial resolution of 1mm (see more details in \cite{hanke2014high}. They are available in openfmRI \cite{poldrack2013toward}. More information about the forrest project can be found at \url{http://studyforrest.org}.

\paragraph{SHERLOCK Dataset} 16 Participants are scanned while watching the BBC TV show "Sherlock". They were asked to recall the movie afterwards using as much time as they wanted.


\paragraph{RAIDERS Dataset} 10 Participants are scanned while watching the movie "Raiders of the lost ark".


\paragraph{GALLANT Dataset} A video stimuli is presented to participants. It was first introduced in \cite{nishimoto2011reconstructing} and was used to study different levels of representation of visual content in brain activity. We focus on 17 sessions and 10 participants so that all 10 participants have participated in the 17 sessions.

\paragraph{FORREST Dataset} Participants were listening to an audio transcription of the movie Forrest Gump. This dataset contains 19 subjects and 8 sessions


\subsection{fMRI reconstruction: Evaluate the ability to recover brain activations in a co-smoothing setting}

We denote $X^{-s}$ brain recordings of all sessions but session $s$:
\[
X^{-s} = X_1^{-s}, \cdots, X_N^{-s}
\]
 where $X_i^{-s}$ refers to the brain recordings of subject $i$ using all session but session $s$.
 
Spatial maps computed using data of all sessions but session $s$ are denoted:
\[
W^{-s} = W_1^{-s}, \cdots, W_N^{-s}
\]

Similarly the data from all subject but $i$ acquired during session $s$ are denoted:
\[
X^{s}_{-i} = X_1^{s},\cdots,X^{s}_{i-1}, X^{s}_{i+1}, \cdots X_N^{s}
\]

We evaluate our model using a co-smoothing setting. 

In a first step all brain recordings for all sessions but one $X^{-s}$ are used for learning subjects spatial maps $W^{-s}$. 

In a second step we focus on the leftout session $s$ and use all subjects but one $X^{s}_{-i}$ to compute a shared response for the leftout session $S^{s}$.
\[
S^{s} = \frac{\sum_{n=1, n\neq i}^N W_n^T X_n}{N - 1}
\]

From $S^{s}$ and $W_i^{-s}$ we compute $\hat{X_i}{s}$ which the prediction for the brain activity of the leftout subject during the leftout session $X_i{s}$.
\[
\hat{X_i}{s} = W_i S^{s}
\]


An illustration of the cosmoothing procedure is available in Figure~\ref{fig:experiment}

%\subsection{Scene classification}
%\todo{Describe how that work}


\begin{figure}
\centering
\includegraphics[scale=0.35]{figures/experiment}
\caption{All sessions but one (red $A, B, C$) are used to compute spatial maps $W$ for every subjects (bottom left). The unused session of all subjects but one (blue $A, B$) and corresponding spatial maps (red $W_a$ and $W_b$) are used to compute the shared response for the unused session (blue $S^4$ top right). Then using the spatial maps of the left-out subject ($W_c$) and the shared response of the unused session ($\widehat{S}^4$) we compute a prediction for the data of the left-out subject during the unused session (violet $\widehat{C}^4$).
The performance of the model is measured by comparing the prediction $\widehat{C}^4$ and true data $C^4$ using the coefficient of determination.}
\label{fig:experiment}
\end{figure}


The performance is measured voxel-wise using the coefficient of determination $c$ as a similarity measure.
For any two timecourses $x$ and $y$ we define the coefficient of determination by:

\[
c(x, y) = 1 - \frac{\sum_t (x[t] - y[t])^2}{\sum_t (y[t] - \overline{y}[t])^2} 
\]

where $\overline{y} = \frac{\sum_{t=1}^T y[t]}{T}$.

Following the leave-one-out cross validation scheme, all our experiments are done several times with a different leftout subject to reconstruct. We report the average of the results for all left out subjects.

\section{Results}
We show that FastSRM leads to comparable reconstruction error than ProbSRM while being considerably faster and requiring less memory. We also show that the atlas used to reduce the data does not have a huge impact on performance. 

We use the full brain for our experiments and report an r2 per voxel. All voxels do not carry an equal level of information. For example in the forrest subjects are exposed to a purely auditive task so most of the well predicted voxels are in the auditive cortex (see Figure~\ref{fig:example_r2}). 

Although all algorithms are run using the full brain, we measure the performance in terms of mean coefficient of determination inside a region of interest (in order to leave out regions where there is no useful information). In order to determine the region of interest, we focus on the results of ProbSRM with $10, 20, 50 $ and $100$ components and keep only those regions where the coefficient of determination is above $0.05$. This means of selecting regions favors ProbSRM.


In Figure~\ref{fig:experiment}, we plotted the mean coefficient of determination against the number of components used in the algorithm for ProbSRM and FastSRM algorithm with different atlases. The coefficient of determination tends to increase with the number of components (which is what is expected as more information can be retrieved when the number of components is high). There are few difference in performance between FastSRM and ProbSRM and this holds for any atlas we used (probabilistic or deterministic). 

In Figure~\ref{fig:fit_time}, we can compare the running time of FastSRM and ProbSRM. FastSRM is always faster than ProbSRM and it is the fastest when a deterministic atlas is used. In that case FastSRM takes on average 12 times less time than ProbSRM to fit (averaging accross datasets) reducing the fitting time from more than 2 hours to less than 15 minutes.

In Figure~\ref{fig:memory_usage}, we can compare the memory consumption of FastSRM and ProbSRM. FastSRM is only more memory friendly than ProbSRM when a small probabilistic atlas is used or when the atlas is deterministic. When we use deterministic atlases the memory consumption is on average reduced by a factor 6 (averaging accross dataset) reducing the memory usage from 10 Gb to less than 2Gb so that the computation can be performed on a modern laptop. 
 



\begin{figure}
\centering
\includegraphics[scale=0.75]{figures/mean_r2_score}
\caption{We compare the performance (measured in terms of average r2 score) of ProbSRM and FastSRM with different atlases in function of the number of components used. Atlases tested are MODL with 512 and 1024 regions, Basc with 444 regions and Shaeffer with 800 regions. Datasets tested are FORREST (top left), GALLANT (bottom left), RAIDERS (top right) and SHERLOCK (bottom right). 
As we can see, no matter which atlas is chosen, FastSRM is competitive with ProbSRM.}
\label{fig:experiment}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.65]{figures/fit_time}
\caption{We compare the fitting time of ProbSRM and FastSRM with different atlases in function of the number of components used. Atlases tested are MODL with 512 and 1024 regions, Basc with 444 regions and Shaeffer with 800 regions. Datasets tested are GALLANT, FORREST, SHERLOCK and RAIDERS.
\textbf{Left}: Fitting time (as a fraction of ProbSRM fitting time) averaged over the four datasets
\textbf{Right}: Fitting time (in seconds) for each of the four different datasets.
FastSRM is up to 12 times faster than ProbSRM on average.}
\label{fig:fit_time}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.65]{figures/memory_usage}
\caption{We compare the memory usage of ProbSRM and FastSRM with different atlases in function of the number of components used. Atlases tested are MODL with 512 and 1024 regions, Basc with 444 regions and Shaeffer with 800 regions. Datasets tested are SHERLOCK, GALLANT, FORREST and RAIDERS.
\textbf{Left}: Memory usage (as a fraction of ProbSRM memory usage) averaged over the four datasets
\textbf{Right}: Memory usage (in Mo) for each of the four different datasets.
FastSRM uses up to 6 times less memory than ProbSRM making it possible to compute a shared response on a large dataset using a modern laptop.}
\label{fig:memory_usage}
\end{figure}


\begin{figure}
\centering
\includegraphics[scale=0.65]{figures/FastSRM_shaeffer_800_forrest}
\includegraphics[scale=0.65]{figures/ProbSRM_forrest}
\caption{The stimuli in the forrest dataset is purely auditive. We show the full brain r2 score obtained with ProbSRM (top) and FastSRM (bottom) using the deterministic atlas of Shaeffer (with 800 regions) with 20 components. 
As expected the voxels that are better reconstructed are in the auditive cortex.}
\label{fig:example_r2}
\end{figure}

\section{Discussion}
Our algorithm provide same performance as ProbSRM while requiring about 6 times less memory and 10 times less time. This allows the computation to be performed efficiently on a modern laptop in a short amount of time.

\begin{itemize}
\item Extension to "Interpretable" SRM is easy using a simple regression
\item The projection over the atlas can be paralellized: there is still room to trade memory for speed.
\end{itemize}
\bibliographystyle{apacite}
\bibliography{biblio}

\end{document}