"""
Standalone memory efficient version of deterministic SRM
"""

from __future__ import division
import itertools
import numpy as np
import nilearn
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.externals.joblib import Memory, Parallel, delayed
from nilearn._utils.niimg_conversions import _resolve_globbing
from nilearn._utils.compat import _basestring
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
import scipy
from sklearn.decomposition import IncrementalPCA
import os
import nibabel
import joblib
import glob
from nilearn.input_data import MultiNiftiMasker, NiftiLabelsMasker
from time import time
import pdb
from nilearn.image import new_img_like, high_variance_confounds


def check_img(img):
    """
    Help function to correctly format image
    Parameters
    ----------
    img: Niimg-like objects
        See http://nilearn.github.io/manipulating_images/input_output.html
    Returns
    -------
    img: Niimg-like object
        correctly formated images
    """
    if isinstance(img, _basestring):
        if nilearn.EXPAND_PATH_WILDCARDS and glob.has_magic(img):
            img = _resolve_globbing(img)

    if len(img) == 0:
        raise ValueError('The input contains empty sessions')
    return img


def check_imgs(imgs, confounds):
    """
    Help function to correctly format images
    Parameters
    ----------
    imgs: list of list of Niimg-like objects
            See http://nilearn.github.io/manipulating_images/input_output.html
            First axis refers to the subject, second to the section
            Data on which the mask is calculated.
            The affine is considered the same for all.
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with imgs
    Returns
    -------
    imgs: np array (n_subjects, n_sessions) of 4D niimg
        correctly formated images
    """

    if isinstance(imgs, _basestring) or not hasattr(imgs, '__iter__'):
        # This class is meant for list of list of images
        imgs = [[check_img(imgs), ], ]
        confounds = [[confounds, ], ]
    else:
        if confounds is None:
            confounds = itertools.repeat(confounds)

        imgs_ = []
        confounds_ = []
        for sessions, confound in zip(imgs, confounds):
            if isinstance(sessions, _basestring) or not hasattr(imgs, '__iter__'):
                imgs_.append([check_img(sessions),])
                confounds_.append([confound])
            else:
                imgs_.append([session for session in sessions])
                if confound is None:
                    confounds_.append([None for _ in sessions])
                else:
                    confounds_.append([ses_confound for ses_confound in confound])
        imgs = imgs_
        confounds = confounds_
    imgs = np.array(imgs)
    confounds = np.array(confounds)
    return imgs, confounds


def reduce_data_single(sessions, masker, confounds, n_reduced_dimensions, atlas, inv_atlas):
    """
    Reduce data using online PCA
    Parameters
    ----------
    sessions: np array (n_sessions,) of 4D Niimgs
        all sessions for one subject
    masker : instance of MultiNiftiMasker
            Masker used to filter and mask data
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with sessions
    n_reduced_dimensions: int
        size of reduced space where data are projected
    atlas:  None or np array of size (n_atlas_dimensions, n_voxels) or n_voxels (deterministic atlas)
        See http://nilearn.github.io/manipulating_images/input_output.html
        Atlas on which to project the data
    inv_atlas: None or np array
        Pseudo inverse of the atlas (only for probabilistic atlases)
    Returns
    -------
    reduced_data: np array of shape (n_timeframes, n_reduced_dimensions)
        reduced data
    index: np array of shape n_sessions
        slice of each sessions
    """
    wait = None
    reduced_data = []
    if atlas is None and inv_atlas is None:
        pca = IncrementalPCA(n_components=n_reduced_dimensions)
        for session, confound in zip(sessions, confounds):
            # confound = high_variance_confounds(session)
            data = masker.transform(session, confound)
            if wait is None and data.shape[0] < n_reduced_dimensions:
                wait = n_reduced_dimensions / data.shape[0] + 1
                data_ = []

            if wait > 0:
                data_.append(data)
                wait -= 1

            if wait == 0 and len(data_) != 0:
                data = np.concatenate(data_, axis=0)
                data_ = []

            if (wait == 0 and len(data_) == 0) or wait is None:
                pca.partial_fit(data)

        for session, confound in zip(sessions, confounds):
            # confound = high_variance_confounds(session)
            data = masker.transform(session, confound)
            reduced_data.append(pca.transform(data))
    else:
        if inv_atlas is None:
            for session, confound in zip(sessions, confounds):
                # confound = high_variance_confounds(session)
                data = masker.transform(session, confound)
                reduced_data.append(np.array([np.mean(data[:, atlas == c], axis=1) for c in np.unique(atlas)[1:]]).T)
        else:
            for session, confound in zip(sessions, confounds):
                # confound = high_variance_confounds(session)
                data = masker.transform(session, confound)
                reduced_data.append(data.dot(inv_atlas))

    index = [0] + [rd_ses.shape[0] for rd_ses in reduced_data]
    index = np.cumsum(index)
    index = np.array([slice(index[i], index[i+1])
                      for i in range(len(index) - 1)])
    reduced_data = np.concatenate(reduced_data, axis=0)
    return reduced_data, index


def reduce_data(imgs, masker, confounds, n_reduced_dimensions=300, n_jobs=1, atlas=None):
    """
    Reduce data using online PCA. Parallelize accross subject
    Parameters
    ----------
    imgs: np array (n_subjects, n_sessions) of Niimg-like objects
        See http://nilearn.github.io/manipulating_images/input_output.html
        First axis refers to the subject, second to the section
        Data on which the mask is calculated.
        The affine is considered the same for all.
    masker : instance of MultiNiftiMasker
        Masker used to filter and mask data
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with imgs
    n_reduced_dimensions: int
        size of reduced space where data are projected
    n_jobs: integer, optional, default=1
        The number of CPUs to use to do the computation.
         -1 means all CPUs, -2 all CPUs but one, and so on.
    atlas:  Niimg-like objects
        See http://nilearn.github.io/manipulating_images/input_output.html
        Atlas on which to project the data
    Returns
    -------
    reduced_data_list: list of np array of shape (n_timeframes, n_reduced_dimensions)
        each subject's reduced data (concatenated for all sessions)
    index: np array shape (n_sessions, )
        slice of each session
    """

    A = None
    A_inv = None

    if atlas is not None:
        atlas_masker = MultiNiftiMasker(mask_img=masker.mask_img_).fit()
        X = nibabel.load(atlas).get_data()
        if len(X.shape) == 3:
            # We do not want to resample the data again so we resample the atlas instead (faster)
            n_components = len(np.unique(X)) - 1
            xa, ya, za = X.shape
            A = np.zeros((xa, ya, za, n_components + 1))
            for c in np.unique(X)[1:].astype(int):
                X_ = np.copy(X)
                X_[X_ != c] = 0.
                X_[X_ == c] = 1.
                A[:, :, :, c] = X_
            A = atlas_masker.transform(new_img_like(atlas, A))
            A = np.argmax(A, axis=0)
        else:
            A_ = atlas_masker.transform(atlas)
            A_inv = A_.T.dot(np.linalg.inv(A_.dot(A_.T)))

    reduced_data_list = Parallel(n_jobs=n_jobs)(
        delayed(reduce_data_single)(
            sessions,
            masker,
            confound,
            n_reduced_dimensions,
            A,
            A_inv
        ) for sessions, confound in zip(imgs, confounds))

    reduced_data_list, indexes = zip(*reduced_data_list)

    for i in range(len(indexes) - 1):
        if len(indexes[i]) != len(indexes[i+1]):
            ValueError("Subject %i and Subject %i "
                       "do not have the same number of sessions" % (i, i + 1))

        for k in range(len(indexes[i])):
            if indexes[i][k] != indexes[i+1][k]:
                ValueError("Subject %i and Subject %i "
                           "do not have the same sessions" % (i, i + 1))

    index = indexes[0]
    return reduced_data_list, index


def _compute_shared_response(reduced_data_list, basis):
    """
    Compute shared response with basis fixed
    compute argmin_S ||SW - X||_F^2
    Parameters
    ----------
    reduced_data_list: list of np array of shape (n_timeframes, n_reduced_dimensions)
        each subject's reduced data (concatenated for all sessions)
    basis: list of np array (n_components, n_voxels)
        basis for each subject
    Returns
    -------
    shared_response: np array of shape (n_timeframes, n_components)
        shared response
    """
    s = None
    for m in range(len(basis)):
        data_m = reduced_data_list[m]
        if s is None:
            s = data_m.dot(basis[m].T)
        else:
            s = s + data_m.dot(basis[m].T)
    return s / float(len(basis))


def _compute_subject_basis(corr_mat):
    """
    From correlation matrix between shared response and subject data,
    Finds subject's basis
    Parameters
    ----------
    corr_mat: np array of shape n_component, n_voxels
        correlation matrix between shared response and subject data

    Returns
    -------
    basis: np array of shape n_components, n_voxels
        basis of subject
    """
    if corr_mat.shape[0] == corr_mat.shape[1]:
        U, _, V = scipy.linalg.svd(
            corr_mat + 1.e-18 * np.eye(corr_mat.shape[0]),
            full_matrices=False
        )
    else:
        U, _, V = scipy.linalg.svd(corr_mat, full_matrices=False)
    return U.dot(V)


def fast_srm(reduced_data_list, n_iter=10, n_components=None):
    """
    Computes shared response and basis in reduced space
    (or whenever all data fit in memory)
    Parameters
    ----------
    reduced_data_list: list of np array of shape (n_timeframes, n_reduced_dimensions)
        each subject's reduced data (concatenated for all sessions)
    n_iter: int
        Number of iterations performed
    n_components: int or None
        number of components if n_voxels != n_components
    Returns
    -------
    basis: list of arrays of shape n_components, n_voxels
        each subject basis
    shared_response: np array of shape n_timeframes, n_components
        shared response
    """

    n_subjects = len(reduced_data_list)
    basis = []
    n_timeframes, n_voxels = reduced_data_list[0].shape

    for subject in range(n_subjects):
        q = np.eye(n_components, n_voxels)
        basis.append(q)

    shared_response = _compute_shared_response(reduced_data_list, basis)
    for _ in range(n_iter):
        for i in range(n_subjects):
            X_i = reduced_data_list[i]
            R = _compute_subject_basis(shared_response.T.dot(X_i))
            basis[i] = R

        shared_response = _compute_shared_response(reduced_data_list, basis)

    return basis, shared_response


def _compute_basis_subject_online(sessions, masker, shared_response_list, confounds):
    """
    Computes basis online
    Parameters
    ----------
    sessions: np array (n_sessions,) of 4D Niimgs
        all sessions for one subject
    masker : instance of MultiNiftiMasker
            Masker used to filter and mask data
    shared_response_list: list of n_sessions np arrays of shape n_timeframes, n_components
        shared response for each session
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with sessions
    Returns
    -------
    basis: str or n_subjects list of np array of shape n_components, n_voxels
        path to basis or basis
    """

    basis_i = None
    i = 0
    for session, confound in zip(sessions, confounds):
        # confound = high_variance_confounds(session)
        data = masker.transform(session, confound)
        if basis_i is None:
            basis_i = shared_response_list[i].T.dot(data)
        else:
            basis_i += shared_response_list[i].T.dot(data)
        i += 1
        del data
    return _compute_subject_basis(basis_i)


def _compute_shared_response_online_single(subjects, masker, basis, temp_dir, confounds, subjects_indexes):
    """
    Computes shared response using only one session
    Parameters
    ----------
    subjects: np array (n_subjects,) of 4D Niimgs
        subjects to use to compute shared response
    masker : instance of MultiNiftiMasker
            Masker used to filter and mask data
    basis: list of arrays of shape n_components, n_voxels or str
        basis or path to basis
    temp_dir: None or str
        path to basis if basis is None
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with subjects
    subjects_indexes: list or None:
        list of indexes corresponding to the subjects to use to compute shared response
    Returns
    -------
    shared_response: np array of shape n_timeframes, n_components
        shared response
    """
    n = 0
    shared_response = None
    for k, i in enumerate(subjects_indexes):
        subject = subjects[k]
        confound = confounds[k]
        # confound = high_variance_confounds(subject)
        data = masker.transform(subject, confound)
        if temp_dir is None:
            basis_i = basis[i]
        else:
            basis_i = np.load(os.path.join(temp_dir, "basis_%i.npy" % i))

        if shared_response is None:
            shared_response = data.dot(basis_i.T)
        else:
            shared_response += data.dot(basis_i.T)

        n += 1
    return shared_response / float(n)


def _compute_shared_response_online(imgs, masker, basis, temp_dir, confounds, n_jobs, subjects_indexes):
    """

    Parameters
    ----------
    imgs: np array (n_subjects, n_sessions) of Niimg-like objects
            subjects and sessions to use to compute shared response
            See http://nilearn.github.io/manipulating_images/input_output.html
            First axis refers to the subject, second to the section
            Data on which the mask is calculated.
            The affine is considered the same for all.
    masker : instance of MultiNiftiMasker
            Masker used to filter and mask data
    basis: list of arrays of shape n_components, n_voxels or str
        basis or path to basis
    temp_dir: None or str
        path to basis if basis is None
    confounds : list of CSV file paths or 2D matrices
        This parameter is passed to nilearn.signal.clean. Please see the
        related documentation for details. Should match with imgs
    n_jobs: integer, optional, default=1
            The number of CPUs to use to do the computation.
             -1 means all CPUs, -2 all CPUs but one, and so on.
    subjects_indexes: list or None:
        list of indexes corresponding to the subjects to use to compute shared response

    Returns
    -------
    shared_response_list: list of np array of shape n_timeframes, n_components
        shared response for each session
    """
    shared_response_list = Parallel(n_jobs=n_jobs)(
    delayed(_compute_shared_response_online_single)(
        subjects,
        masker,
        basis,
        temp_dir,
        confound,
        subjects_indexes
    ) for subjects, confound in zip(imgs.T, confounds.T))

    return shared_response_list


class FastSRM(BaseEstimator, TransformerMixin):
    """
    SRM decomposition using a low to very low amount of memory
    """

    def __init__(self,
                 n_components=50,
                 n_reduced_dimensions=300,
                 n_iter=10,
                 atlas=None,
                 temp_dir=None,
                 verbose=False,
                 random_state=None,
                 mask=None,
                 smoothing_fwhm=None,
                 standardize=False,
                 detrend=False,
                 low_pass=None,
                 high_pass=None,
                 t_r=None,
                 target_affine=None,
                 target_shape=None,
                 mask_strategy='epi',
                 mask_args=None,
                 memory=Memory(),
                 memory_level=0,
                 n_jobs=1):
        """
        SRM decomposition using a very low amount of memory
        Parameters
        ----------
        n_components: int
            Number of timecourses of the shared coordinates
        n_reduced_dimensions: int
            Number of reduced dimension used to summarize voxels information (only used when dictionary is None)
        atlas:  Niimg-like objects
            See http://nilearn.github.io/manipulating_images/input_output.html
            Atlas on which to project the data
        n_iter: int
            Number of iterations to perform
        n_jobs: integer, optional, default=1
            The number of CPUs to use to do the computation.
             -1 means all CPUs, -2 all CPUs but one, and so on.
        temp_dir: str or None
            path to dir where we save basis. Otherwise everything is kept in memory.
        random_state: int or RandomState
            Pseudo number generator state used for random sampling.
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        detrend : boolean, optional, default=True
            If detrend is True, the time-series will be detrended before components extraction.
        mask_args: dict, optional
            If mask is None, these are additional parameters passed to masking.compute_background_mask
            or masking.compute_epi_mask to fine-tune mask computation.
            Please see the related documentation for details.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.

        Attributes
        ----------
        `basis`: list of n_subjects 2D numpy-arrays (n_components, n_voxels) or str
            List of masked basis of all subjects. They can be unmasked thanks to
            the `masker_` attribute
            or path to masked basis (when temp_dir is not None)
        `shared_coord`: 2D numpy-array of shape (n_components, n_timeframes)
            Array of shared coordinates, using the masked data of all subjects.
        `masker` : instance of MultiNiftiMasker
            Masker used to filter and mask data as first step. If an instance of
            MultiNiftiMasker is given in `mask` parameter,
            this is a copy of it. Otherwise, a masker is created using the value
            of `mask` and other NiftiMasker related parameters as initialization.
        """

        self.random_state = random_state
        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs
        self.verbose = verbose

        self.masker = None
        self.shared_response = None
        self.basis = None

        self.n_components = n_components
        self.n_reduced_dimensions = n_reduced_dimensions
        self.n_iter = n_iter
        if temp_dir is not None:
            paths = glob.glob(os.path.join(temp_dir, "basis") + "*")
            for path in paths:
                os.remove(path)
        self.temp_dir = temp_dir
        self.n_subjects = None
        self.n_sessions = None
        self.atlas = atlas

    def fit(self, imgs, confounds=None):
        """
        Compute basis accross subjects and shared response from input imgs
        Parameters
        ----------
        imgs: list of list of Niimg-like objects
            See http://nilearn.github.io/manipulating_images/input_output.html
            First axis refers to the subject, second to the section
            Data on which the mask is calculated.
            The affine is considered the same for all.
        confounds : list of CSV file paths or 2D matrices
            This parameter is passed to nilearn.signal.clean. Please see the
            related documentation for details. Should match with the list of list
            imgs given.
        Returns
         -------
         self : object
            Returns the instance itself. Contains attributes listed
            at the object level.
        """
        if self.temp_dir is not None:
            paths = glob.glob(os.path.join(self.temp_dir, "basis") + "*")
            for path in paths:
                os.remove(path)
        imgs, confounds = check_imgs(imgs, confounds)
        self.masker = check_embedded_nifti_masker(self)
        self.n_subjects, self.n_sessions = imgs.shape

        # if masker_ has been provided a mask_img
        if self.masker.mask_img is None:
            raise ValueError('The masker has not been provided a mask_img')

        self.masker = self.masker.fit()

        if self.temp_dir is None:
            basis = []
        else:
            basis = self.temp_dir

        if self.verbose:
            print("[FastSRM.fit] Reducing data")

        reduced_data, index = reduce_data(
            imgs,
            self.masker,
            confounds=confounds,
            n_reduced_dimensions=self.n_reduced_dimensions,
            n_jobs=self.n_jobs,
            atlas=self.atlas,
        )

        if self.verbose:
            print("[FastSRM.fit] Finds shared response using reduced data")

        _, shared_response = fast_srm(
            reduced_data,
            n_iter=self.n_iter,
            n_components=self.n_components
        )

        shared_response_list = [shared_response[s] for s in index]

        if self.verbose:
            print("[FastSRM.fit] Finds basis using full data and shared response")

        i = 0
        for sessions, confound in zip(imgs, confounds):
            basis_i = _compute_basis_subject_online(sessions, self.masker, shared_response_list, confound)
            if self.temp_dir is None:
                basis.append(basis_i)
            else:
                if not os.path.exists(self.temp_dir):
                    os.mkdir(self.temp_dir)
                np.save(os.path.join(self.temp_dir, "basis_%i" % i), basis_i)
            del basis_i
            i += 1

        self.basis = basis
        return self

    def fit_transform(self, imgs, confounds=None, **fit_params):
        """
        Compute basis accross subjects and shared response from input imgs
        Parameters
        ----------
        imgs: np array of Niimg-like objects of shape(n_subjects, n_sessions)
            See http://nilearn.github.io/manipulating_images/input_output.html
            First axis refers to the subject, second to the section
            Data on which the mask is calculated.
            The affine is considered the same for all.
        confounds : list of CSV file paths or 2D matrices
            This parameter is passed to nilearn.signal.clean. Please see the
            related documentation for details. Should match with imgs shape
        Returns
        --------
        shared_response: np array of shape n_timeframes, n_components
            shared response
        """
        self.fit(imgs, **fit_params)
        return self.transform(imgs, confounds=confounds)

    def transform(self, imgs, subjects_indexes=None, confounds=None):
        """
        From data in imgs and basis from training data,
         computes shared space from data in X.

        Parameters
        ----------
        imgs: np array of Niimg-like objects of shape(n_subjects, n_sessions)
            See http://nilearn.github.io/manipulating_images/input_output.html
            First axis refers to the subject, second to the section
            Data on which the mask is calculated.
            The affine is considered the same for all.
        confounds : list of CSV file paths or 2D matrices
            This parameter is passed to nilearn.signal.clean. Please see the
            related documentation for details. Should match with imgs shape
        subjects_indexes: list or None:
            indexes of subjects to use to construct shared response
        Returns
        -------
        shared_response: n_sessions list of np array of shape n_timeframes, n_components
            shared response
        """
        if subjects_indexes is None:
            subjects_indexes = np.arange(len(imgs))
        else:
            subjects_indexes = np.array(subjects_indexes)

        imgs, confounds = check_imgs(imgs, confounds)
        shared_response = _compute_shared_response_online(
            imgs,
            self.masker,
            self.basis,
            self.temp_dir,
            confounds,
            self.n_jobs,
            subjects_indexes
        )

        return shared_response

    def inverse_transform(self, shared_response, subjects_indexes=None, sessions_indexes=None, output_dir=None):
        """
        From common space and the basis from training data
        Retrieve subjects' data described by the common space

        Parameters
        ----------
        shared_response : n_sessions list of 2D numpy array of shape (n_components, n_timeframes)
            common space
        subjects_indexes: list or None:
            list of indexes corresponding to the subjects to retrieve
            None: take all available subjects
        sessions_indexes: list or None:
            list of indexes corresponding to sessions of shared_response to use
            None takes all sessions of shared response
        Returns
        -------
        reconstructed_data: list of 4D niimg
            Reconstructed data for chosen runs
        """
        if subjects_indexes is None:
            subjects_indexes = np.arange(self.n_subjects)
        else:
            subjects_indexes = np.array(subjects_indexes)
            
        if sessions_indexes is None:
            sessions_indexes = np.arange(len(shared_response))
        else:
            sessions_indexes = np.array(sessions_indexes)

        data = []
        for i in subjects_indexes:
            data_ = []
            if self.temp_dir is None:
                basis_i = self.basis[i]
            else:
                basis_i = np.load(os.path.join(self.temp_dir, "basis_%i.npy" % i))

            for j in sessions_indexes:
                data_.append(shared_response[j].dot(basis_i))

            data.append(np.concatenate(data_, axis=0))

        return np.array(data)