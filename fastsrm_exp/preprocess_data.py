import numpy as np
import glob
import pandas as pd
from nilearn.input_data import MultiNiftiMasker
import nilearn

nilearn.CHECK_CACHE_VERSION = False

def load(
        dataset="",
        subjects=None,
        runs=None,
        data_path="",
):
    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob(data_path % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)

    return np.array(X), dataset


for config in ["gallant", "forrest", "raiders", "sherlock"]:
    masker = '/home/parietal/hrichard/cogspaces_data/mask/hcp_mask.nii.gz'
    mask_memory = "/storage/tompouce/hrichard/general_cache/"

    exp_params = {
        "detrend": True,
        "standardize": True,
        "memory": mask_memory,
        "mask": masker,
        "mask_strategy": "epi",
        "n_jobs": 1,
        "memory_level": 5,
        "low_pass": 0.1,
        "high_pass": 0.01,
        "t_r": 2,
        "verbose": 1,
        "smoothing_fwhm": 5,
    }

    if config == "forrest":
        mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
        data_dir = "/storage/data/openfmri/ds113/"

        subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12,
                    13, 14, 15, 16, 17, 18, 19, 20]
        runs = [1, 2, 3, 4, 5, 6, 7]

        X, dataset = load(
            dataset="forrest",
            subjects=subjects,
            runs=runs,
            data_path=data_dir + "sub%03d/BOLD/task001_"
                                 "run%03d/"
                                 "bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz",
        )

    elif config == "gallant":
        data_dir = "/storage/store/data/ibc/derivatives/"
        data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

        subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
        runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05", "Trn06", "Trn07", "Trn08", "Trn09",
                "Val01", "Val02", "Val03", "Val04", "Val05", "Val06", "Val07", "Val08"]

        X, dataset = load(
            dataset="gallant",
            subjects=subjects,
            runs=runs,
            data_path=data_path,
        )

    elif config == "sherlock":
        subjects = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        runs = [0, 1, 2, 3, 4]
        data_path = "/storage/workspace/hrichard/" \
                    "sherlock_data/%02d_sherlock_movie_s%i.nii"
        X = [[data_path % (run, subject) for run in runs]
             for subject in subjects]
        X = np.array(X)
        dataset = "sherlock"

    elif config == "raiders":
        subjects = np.array([1, 4, 5, 6, 7, 9, 11, 12, 13, 14])
        # runs = np.array([1., 2., 3., 4., 5., 6., 7., 8., 10., 11., 12.])
        runs = np.array([1., 2., 3., 4., 5., 6., 7., 8., 10.])

        # We need to discard the last 5 fmri images for each run
        data = pd.read_csv("/home/parietal/hrichard/hugo-richard-M2"
                           "/fastsrm_exp/ibc.dat")
        paths = []
        for i, subject in enumerate(subjects):
            paths_ = []
            for j, run in enumerate(runs):
                paths_.append(data[
                    (data["task"] == "Raiders")
                    & (data["subject"] == subject)
                    & (data["run"] == run)
                ].tail(1)["path"].values[0])
            paths.append(paths_)

        X, dataset = np.array(paths), "raiders"

    mask_params = exp_params.copy()
    mask_params["mask_img"] = mask_params["mask"]
    del mask_params["mask"]

    n_subjects, n_sessions = X.shape
    mask = MultiNiftiMasker(**mask_params).fit()

    print("dataset", config)
    for i in range(n_subjects):
        for j in range(n_sessions):
            print("loading %s" % X[i, j])
            X_ij = mask.transform(X[i, j])
            np.save("/storage/workspace/hrichard/"
                    "masked_data/%s/subject_%i_session_%i" % (config, i, j),
                    X_ij)
            print("%s: %i / 100" %
                  (config,
                   (i * n_sessions + j) * 100  / (n_sessions * n_subjects))
                  )
