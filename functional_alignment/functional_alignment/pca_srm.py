import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import check_random_state
from sklearn.decomposition import IncrementalPCA
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from sklearn.utils.extmath import fast_dot
from nilearn.input_data import NiftiMasker
from joblib import Memory
import logging
import os
from sklearn.linear_model import Ridge
from copy import deepcopy
from functional_alignment.baseSRM import BaseSRM
from nilearn._utils.cache_mixin import CacheMixin, cache
import itertools
from functional_alignment.utils import load_img

logger = logging.getLogger(__name__)


def _pca_srm(n_components, imgs, masker, confounds, memory=None, memory_level=None):
    """
    Computes the mask and the subjects' basis.
    """

    masker.fit()
    n_subjects = len(imgs)
    if not hasattr(imgs[0], "__iter__"):
        n_runs = 1
    else:
        n_runs = len(imgs[0])

    subjects_v = None
    pca = IncrementalPCA(n_components=n_components)
    X_first = None
    for r in range(n_runs):
        logger.debug("Loading data for all subjects for run %i / %i" % (r, n_runs))
        X_ = None
        if not hasattr(imgs[0], "__iter__"):
            for i, confound in zip(range(n_subjects), confounds):
                X_ir, masker = cache(
                    load_img,
                    memory=memory,
                    memory_level=memory_level,
                    func_memory_level=2
                )(
                    masker,
                    imgs[i],
                    confound=confound
                )
                n_voxels, n_timeframes = X_ir.shape
                if X_ is None:
                    X_ = np.memmap(memory + "_memmap", dtype='float32', mode='w+', shape=(n_timeframes, n_voxels * n_subjects))
                X_[:, n_voxels * i: n_voxels * (i+1)] = X_ir.T
        else:
            for i, confound in zip(range(n_subjects), confounds):
                X_ir, masker = cache(
                    load_img,
                    memory=memory,
                    memory_level=memory_level,
                    func_memory_level=2
                )(
                    masker,
                    imgs[i][r],
                    confound=confound
                )
                n_voxels, n_timeframes = X_ir.shape
                if X_ is None:
                    X_ = np.memmap(memory + "_memmap", dtype='float32', mode='w+', shape=(n_timeframes, n_voxels * n_subjects))
                X_[:, n_voxels * i: n_voxels * (i+1)] = X_ir.T
        print("Data shape", X_.shape)
        n_timeframes, _ = X_.shape
        for k in range(int(np.ceil(n_timeframes / float(n_components)))):
            if (k+2) * n_components >= n_timeframes:
                X = X_[k * n_components: (k+2) * n_components, :]
                print(X.shape)
                pca.partial_fit(X)
                break
            else:
                X = X_[k * n_components: (k+1) * n_components, :]
                print(X.shape)
                pca.partial_fit(X)
        subjects_v = [int(X_.shape[1] / n_subjects)] * n_subjects
        del X_

    indices = np.cumsum(subjects_v)[:-1]

    W_T = pca.components_
    basis_ = np.split(W_T.T, indices)
    return masker, basis_


class PCASRM(BaseSRM):
    """
    PCA Shared Response Model
    Performs a low rank decomposition on each subject
    which consists of a common response and an orthogonal subject-specific basis.

    It finds the shared coordinates S and
    for any subject X_i, the basis W_i
    that the sum of Frobenius norms
    sum_i || X_i - W_i S ||^2
    is minimized

    X_i is the data of subject i
    S are the shared coordinates
    W_i is the basis of subject i
    """

    def __init__(self, n_components,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 transform_method="mean", n_jobs=1
                 ):
        """
        PCA Shared Response Model
        Performs a low rank decomposition on each subject
        which consists of a common response and an orthogonal subject-specific basis.

        It finds the shared coordinates S and
        for any subject X_i, the basis W_i
        that the sum of Frobenius norms
        sum_i || X_i - W_i S ||^2
        is minimized

        X_i is the data of subject i
        S are the shared coordinates
        W_i is the basis of subject i


        Parameters
        ----------
        See BaseSRM for description
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method)

    def _fit(self, imgs, confounds=None):
        """
        Computes subjects basis using incremental PCA on all subjects
        """
        self.masker, self.basis = cache(
            _pca_srm,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=2
        )(self.n_components, imgs, self.masker, confounds,
          memory=self.memory,
          memory_level=self.memory_level)
        return self

    def fit_transform(self, imgs, **fitparams):
        """
        Overrides BaseSrm to account for orthogonality of W.T = [W_1.T,...,W_n.T]
        """
        self.fit(imgs, **fitparams)

        if "confounds" in fitparams:
            confounds = fitparams["confounds"]
            if confounds is None:
                confounds = itertools.repeat(None)
        else:
            confounds = itertools.repeat(None)

        masker = self.masker
        shared = None
        for k, confound in zip(range(len(self.basis)), confounds):
            logger.debug("Transform on subject %i" % k)
            X_k, masker = cache(
                load_img,
                memory=self.memory,
                memory_level=self.memory_level,
                func_memory_level=2
            )(
                masker,
                imgs[k],
                axis=0,
                confound=confound
            )
            if shared is None:
                shared = self.basis[k].T.dot(X_k)
            else:
                shared += self.basis[k].T.dot(X_k)
        self.shared_response = shared
        return shared

    def set_components(self, n_components):
        """
        Allows us to use previous fit with a larger number of components to recover the basis
        we would have found with a smaller number of components
        Parameters
        ----------
        n_components: int
        """
        for i in range(len(self.basis)):
            self.basis[i] = self.basis[i][:, :n_components]
            self.n_components = n_components
