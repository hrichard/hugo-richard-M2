from sklearn.decomposition.dict_learning import sparse_encode
from sklearn.utils.validation import check_array
import numpy as np
import logging
from functional_alignment.baseSRM import BaseSRM
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img
from sklearn.utils import check_random_state
from sklearn.externals.joblib import Memory
from functional_alignment.isrm_encode import online_isrm_encode, isrm_encode, structure_matrix, laplacian_matrix, similarity_matrix
from time import time
import os
from sklearn.utils import gen_batches
from functional_alignment.deterministic_srm import DeterministicSRM
from scipy import linalg
from sklearn.decomposition.dict_learning import _update_dict
import sys


logger = logging.getLogger(__name__)


def _online_isrm_fit(masker,
                     imgs,
                     confounds=None,
                     n_components=10,
                     random_state=None,
                     memory=None,
                     memory_level=0,
                     l1_weight=1.,
                     grad_weight=1,
                     n_iter=10,
                     positive=True,
                     batch_size=None,
                     exp_params={}):
    """
    Fit the model
    Parameters
    ----------
    ToDo: description of parameters
    Returns
    -------

    """

    alpha = l1_weight
    beta = grad_weight
    shared_response = None
    masker.fit()
    random_state = check_random_state(random_state)

    n_subjects = len(imgs)
    confounds = [confound for _, confound in zip(range(n_subjects), confounds)]

    subjects = np.arange(n_subjects)
    for subject in subjects:
        confound = confounds[subject]
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )
        n_voxels, n_timeframes = x_subject.shape
        n_batches = n_voxels / batch_size
        voxels = np.arange(n_voxels)
        np.random.shuffle(voxels)
        batches = np.array_split(voxels, n_batches)

        if shared_response is None:
            print("Compute intialisation via DetSRM")
            t0 = time()
            fit_det = DeterministicSRM(n_components=n_components, **exp_params).fit(imgs)
            shared_response = fit_det.shared_response
            W = fit_det.basis
            print(W[0].shape)

            # The covariance of the dictionary
            A = np.zeros((n_components, n_components))
            # The data approximation
            B = np.zeros((n_timeframes, n_components))

            print("Done", time() - t0)
            t0 = time()
            print("Compute Structure matrix")
            Lx, Ly, Lz = laplacian_matrix(masker.mask_img_.get_data().astype(bool))
            G = Lx.T.dot(Lx) + Ly.T.dot(Ly) + Lz.T.dot(Lz)
            G = beta * n_timeframes / float(n_components) * G
            alpha = alpha * n_timeframes / float(n_components)
            print("Done", time() - t0)

        batch_done = 0
        for batch in batches:
            print("Subjects done %i / %i. Voxels done %i / %i " % (subject, n_subjects, batch_done, n_voxels))
            this_X = x_subject[batch].astype(float, ord("F"))
            W[subject] = online_isrm_encode(batch, this_X.T, shared_response.T, W[subject].T, alpha, G,
                                   random_state.randint(0, 1e3), iter=n_iter, positive=positive).T
            this_code = W[subject][batch, :].T

            A_batch = np.dot(this_code, this_code.T)
            B_batch = np.dot(this_X.T, this_code.T)

            A *= batch_done
            A += A_batch
            A /= batch_done + len(batch)

            B *= batch_done
            B += B_batch
            B /= batch_done + len(batch)

            batch_done += len(batch)

            # Update dictionary
            shared_response = _update_dict(shared_response.T, B, A, verbose=False,
                                           random_state=random_state,
                                           positive=False
                                           ).T

    print("Finding code")
    for subject in range(n_subjects):
        print("subject", subject)
        confound = confounds[subject]
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )
        W[subject], _, _, _ = isrm_encode(x_subject.T, shared_response.T, alpha, G,
                                          random_state.randint(0, 1e3), tol=1e-6, max_iter=5, positive=positive)
        W[subject] = W[subject].T

    return masker, W, shared_response


class OnlineISRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=None, memory_level=0,
                 transform_method="mean", n_jobs=1,
                 l1_weight=1.,
                 grad_weight=1.,
                 max_iter=100.,
                 positive=True,
                 batch_size=300,
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            Sparsity controlling parameter.
        init_method: str
            "cca" uses cca as initialization
            "ortho_srm" uses fast deterministic srm as initialization
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method,
                 )

        self.exp_params = {
            "random_state": random_state,
            "mask": mask,
            "smoothing_fwhm": smoothing_fwhm,
            "standardize": standardize,
            "detrend": detrend,
            "low_pass": low_pass,
            "high_pass": high_pass,
            "t_r": t_r,
            "target_affine": target_affine,
            "target_shape": target_shape,
            "mask_strategy": mask_strategy,
            "mask_args": mask_args,
            "n_jobs": n_jobs,
            "verbose": verbose,
            "memory": memory,
            'memory_level': memory_level
        }

        self.l1_weight = l1_weight
        self.grad_weight = grad_weight
        self.n_iter = max_iter
        self.positive = positive

        if batch_size is None:
            self.batch_size = n_components
        else:
            self.batch_size = batch_size

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response = cache(
            _online_isrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          l1_weight=self.l1_weight,
          grad_weight=self.grad_weight,
          n_iter=self.n_iter,
          positive=self.positive,
          batch_size=self.batch_size,
          exp_params=self.exp_params,
          )
        return self

