import nibabel as nib
import numpy as np
import logging
import sys
sys.path.append("../")
from functional_alignment.load_data import load
from functional_alignment.pca_srm import PCASRM
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.dictionary_srm import DictionarySRM
from functional_alignment.cca_srm import CCASRM
from functional_alignment.online_dictionary_srm import OnlineDictionarySRM
from functional_alignment.online_isrm import OnlineISRM
from functional_alignment.graphnet_srm import GraphNetSRM
from functional_alignment.batch_isrm import BatchISRM
from nilearn.input_data import MultiNiftiMasker
import os.path
import itertools

#Forrest_config
logging.basicConfig(level=logging.DEBUG, filename="logfile_alignment_compare_srm", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

for config in ["ibc", "forrest"]:
    logging.info("Start experiment with config %s" % config)

    if config == "forrest":
        mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
        data_dir = "/storage/data/openfmri/ds113/"

        subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        runs = [1, 2, 3, 4, 5, 6, 7]

        X, dataset = load(
            dataset="forrest",
            subjects=subjects,
            runs=runs,
            data_path=data_dir + "sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz",
        )

        masker = mask_dir + "bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"
        mask_memory = "/storage/tompouce/hrichard/forrest/forrest_cache"

        exp_params = {
            "detrend": True,
            "standardize": True,
            "memory": mask_memory,
            "mask": masker,
            "mask_strategy": "epi",
            "n_jobs": 1,
            "memory_level": 5,
            "t_r": 2,
            "verbose": 1
        }

    elif config == "ibc":
        mask_dir = "/storage/store/data/ibc/derivatives/"
        data_dir = "/storage/store/data/ibc/derivatives/"
        data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

        subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
        runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05", "Trn06", "Trn07", "Trn08", "Trn09",
                "Val01", "Val02", "Val03", "Val04", "Val05", "Val06", "Val07", "Val08"]

        X, dataset = load(
            dataset="ibc",
            subjects=subjects,
            runs=runs,
            data_path=data_path,
        )

        masker = mask_dir + "group/gm_mask.nii.gz"
        mask_memory = "/storage/tompouce/hrichard/ibc/ibc_cache"

        exp_params = {
            "detrend": True,
            "standardize": True,
            "memory": mask_memory,
            "mask": masker,
            "mask_strategy": "epi",
            "n_jobs": 1,
            "memory_level": 5,
            "low_pass": 0.1,
            "high_pass": 0.01,
            "t_r": 2,
            "verbose": 1
        }

    else:
        ValueError("No valid configuration was specified")


    mask_params = exp_params.copy()
    mask_params["mask_img"] = mask_params["mask"]
    del mask_params["mask"]

    splits = []
    for i in range(len(subjects)):
        split_train = []
        split_test = [i]
        for j in range(len(subjects)):
            if j != i:
                split_train.append(j)
        split_test = np.array(split_test)
        split_train = np.array(split_train)
        splits.append((split_train, split_test))

    for i in range(len(splits)):
        logging.debug(splits[i])

    X_train = []
    X_test = []
    for i in range(len(X)):
        logging.debug("Masking Data for subject %i / %i "%(i + 1, len(X)))
        Xi_train = []
        for j in range(len(X[i])):
            X_ij = X[i][j]
            if j == len(X[i]) - 1:
                Xi_test = [X_ij]
            else:
                Xi_train.append(X_ij)
        X_train.append(Xi_train)
        X_test.append(Xi_test)


    mask = MultiNiftiMasker(**mask_params).fit()


    def corr(X, Y):
        X_ = X - np.mean(X, axis=1, keepdims=True)
        Y_ = Y - np.mean(Y, axis=1, keepdims=True)
        X_ = X_ / (np.linalg.norm(X_, axis=1, keepdims=True) + 1e-12)
        Y_ = Y_ / (np.linalg.norm(Y_, axis=1, keepdims=True) + 1e-12)
        return np.sum(X_ * Y_, axis=1)


    for mes in ["corr", "r2"]:
        for k in [5, 10, 20]:
            logging.info("Number of components: %i" % k)
            # l1_weight = 0.1
            # grad_weight = 0.1
            # common_weight = 0.1
            #
            # algorithms = [
            #     (GraphNetSRM(n_components=k, l1_weight=l1_weight,
            #                 grad_weight=grad_weight, common_weight=common_weight,
            #                 max_iter=10, **exp_params),
            #     "GN_10_la%.3f_g%.3f_c%.3f" % (l1_weight, grad_weight, common_weight))
            # ]

            # algorithms = [
            #     (
            #         BatchISRM(n_components=k, l1_weight=l1_weight, grad_weight=grad_weight, common_weight=common_weight,
            #                   max_iter=iter, positive=positive, shared_init="DetSRM", random_state=0, transform_method=transform,**exp_params),
            #         transform.title() + "BatchISRM_la%.3f_g%.3f_c%.3f_positive_%i_initDetSRM_meandict_iter%.3f" % (
            #             l1_weight, grad_weight, common_weight, int(positive), iter)
            #     )
            #     for l1_weight, grad_weight in [(0., 0.), (10., 100.)]
            #     for iter in [0]
            #     for common_weight in [0.]
            #     for positive in [True]
            #     for transform in ["mean", "stack"]
            # ]

            algorithms = []

            algorithms.append(
                (
                    PCASRM(n_components=k, transform_method="stack", **exp_params),
                    "StackPCA"
                )
            )

            algorithms.append(
                (
                    PCASRM(n_components=k, transform_method="mean", **exp_params),
                    "PCA"
                )
            )

            # algorithms.append(
            #     (
            #         OnlineISRM(n_components=k, l1_weight=1., grad_weight=100., positive=True, **exp_params),
            #         "OnlineISRM"
            #     )
            # )
            #
            # algorithms.append(
            #     (
            #         OnlineISRM(n_components=k, l1_weight=1., grad_weight=0., positive=True, **exp_params),
            #         "PositiveDictOnlineISRM"
            #     )
            # )
            #
            # algorithms.append(
            #     (
            #         OnlineISRM(n_components=k, l1_weight=0., grad_weight=100., **exp_params),
            #         "SmoothOnlineISRM"
            #     )
            # )
            #
            # algorithms.append(
            #     (
            #         OnlineISRM(n_components=k, l1_weight=0., grad_weight=0., positive=True, **exp_params),
            #         "PositiveOnlineISRM"
            #     )
            # )
            #
            # algorithms.append(
            #     (
            #         OnlineISRM(n_components=k, l1_weight=0., grad_weight=0., positive=False, **exp_params),
            #         "NoConsOnlineISRM"
            #     )
            # )

            algorithms.append(
                (DictionarySRM(n_components=k, alpha=1., transform_method="stack", **exp_params), "StackFastDict")
            )

            algorithms.append(
                (DictionarySRM(n_components=k, alpha=1., transform_method="mean", **exp_params), "MeanFastDict")
            )

            algorithms.append(
            (OnlineDictionarySRM(n_components=k, alpha=1., positive_code=False, transform_method="stack", **exp_params), "StackOnlineDict"))

            algorithms.append(
            (OnlineDictionarySRM(n_components=k, alpha=1., positive_code=True, transform_method="stack", **exp_params), "StackPositiveOnlineDict"))

            algorithms.append(
                (OnlineDictionarySRM(n_components=k, alpha=1., positive_code=False, **exp_params),
                 "OnlineDict"))

            algorithms.append(
                (OnlineDictionarySRM(n_components=k, alpha=1., positive_code=True, **exp_params),
                 "PositiveOnlineDict"))

            algorithms.append((ProbabilisticSRM(n_components=k, **exp_params), "ProbSRM"))
            algorithms.append((DeterministicSRM(n_components=k, **exp_params), "DetSRM"))

            for i in range(len(algorithms)):
                algorithm, name = algorithms.pop()
                logging.info(name)
                logging.info("Fitting data")
                algorithm.fit(X_train)

                for train_index, test_index in splits:
                    result_directory = "/storage/workspace/hrichard/M2_internship/results/exp10/k_%i/" % k
                    result_path = (result_directory +
                            dataset +
                            "alignment_srm" +
                            "_algo" +
                            name +
                            "subject_" + str(test_index[0]) +
                            "mes_" + mes +
                            "_exp10.nii")

                    if not os.path.exists(result_path):
                        logging.info("train_index", train_index)
                        logging.info("test_index", test_index)
                        x_test = [X_test[i] for i in train_index]
                        logging.info("Transforming Data")
                        shared_response = algorithm.transform(x_test, index=train_index)
                        logging.info("Reconstruction")
                        Y = algorithm.inverse_transform(shared_response, index=test_index)[0]
                        logging.info("Done")
                        X_true = mask.transform(X_test[test_index[0]][0]).T
                        if mes == "r2":
                            var_e = (X_true - Y).var(axis=1)
                            exp_var = 1 - var_e
                        elif mes == "corr":
                            exp_var = corr(X_true, Y)
                        else:
                            raise ValueError("how to measure error ?")

                        logging.info("Score")
                        logging.info(exp_var.mean())
                        if not os.path.exists(result_directory):
                            os.makedirs(result_directory)
                        nib.save(
                            mask.inverse_transform(exp_var),
                            result_directory +
                            dataset +
                            "alignment_srm" +
                            "_algo" +
                            name +
                            "subject_" + str(test_index[0]) +
                            "mes_" + mes +
                            "_exp10.nii"
                        )
                    logging.info("Done")
                del algorithm