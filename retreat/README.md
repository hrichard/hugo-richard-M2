RETREAT 2019
--------------
Yesterday


![Alt text](./MAE_pred.png)
![Alt text](./classif.png)

  Today
  

*Use fonctional connectivity matrix to predict age*


![Alt text](./MAE_fc_pred.png)


*Use a neural network to predict age from correlation between SRM basis*


![Alt text](./fig_NN.png)



*Use ridge to predict age from SRM basis*


![Alt text](./smooth_ridge.png)
