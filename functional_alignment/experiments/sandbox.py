import numpy as np
from sklearn.utils.testing import assert_array_almost_equal


def _projection(x, y):
    """Compute scalar d minimizing ||dx-y||
    Parameters
    ----------
    x: (n_features) nd array
        source vector
    y: (n_features) nd array
        target vector

    Returns
    ----------
    d: int
        scaling factor
    """
    return np.dot(x, y) / np.linalg.norm(x)**2


def _voxelwise_signal_projection(X, Y, n_jobs=1):
    """Compute D, list of scalar d_i minimizing ||d_i x_i-y_i|| for every x_i,y_i in X,Y
    Parameters
    ----------
    X: (n_samples, n_features) nd array
        source data
    Y: (n_samples, n_features) nd array
        target data

    Returns
    ----------
    D: list of ints
        List of optimal scaling factors
    """
    return Parallel(n_jobs)(delayed(_projection)(
        voxel_source, voxel_target)
        for voxel_source, voxel_target in zip(X, Y))


_voxelwise_signal_projection(X, Y)
from fmralign.alignment_methods import _voxelwise_signal_projection
import numpy as np
X = np.random.randn(100, 500000)
Y = np.random.randn(100, 500000)

%timeit - n 100 _voxelwise_signal_projection(X, Y, 1)
%timeit - n 100 np.sum(X * Y, 1) / np.sum(X * X, 1)


a = _voxelwise_signal_projection(X, Y)
b = np.sum(X * Y, 1) / np.sum(X * X, 1)
assert_array_almost_equal(a, b)
np.shape(a)
np.shape(b)
n_samples = 4
n_features = 6
A = np.random.rand(n_samples, n_features)
C = []
for i, a in enumerate(A):
    C.append((i + 1) * a)
c = np.sum(A * C, 1) / np.sum(A * A, 1)
c
assert_array_almost_equal(c, [i + 1 for i in range(n_samples)])

# %%


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from nilearn.image import index_img
from nilearn.input_data import NiftiMasker
from sklearn.metrics import r2_score
from fm

from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from ibc_public.utils_data import (
data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
CONDITIONS, THREE_MM)
from nilearn.plotting import plot_anat, plot_stat_map, plot_roi
% matplotlib inline

    subject_list = SUBJECTS
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
               'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
               'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm', ]

    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                   conditions=CONTRASTS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)

    path_train = []
    path_test = []

    for subject in subject_list:
    path = []
    for i, condition in enumerate(conditions):
        ap_path = df[df.acquisition == 'ap'][df.subject == subject][
            df.contrast == condition].path.values[-1]
        path.append(ap_path)
    paths.append(path)

    from nilearn.regions import Parcellations
    mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
    mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
    masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
    masker.fit(mask_path)

    im = masker.inverse_transform(masker.transform(paths[3]))
    masker.transform(subs_path).shape
    im = masker.inverse_transform(masker.transform(subs_path))
    im.shape
    len(paths)
    len(conditions[chosen])
    n_pieces = 150
    subs_path = []
    for path in paths:
    subs_path.extend(path)

    ward = Parcellations(method='ward', n_parcels=n_pieces,
                       standardize=False, smoothing_fwhm=0, memory_level=1,
                       verbose=1, mask=masker)
    ward.fit(im)
    im_labels = ward.labels_img_

    data = masker.transform(im_labels)
    labels, counts = np.unique(data, return_counts=True)

    counts
    np.count_nonzero(counts > 1)
    display = plot_roi(im_labels, display_mode='x', cut_coords=8)

    im_labels.to_filename(os.path.join(
        path_to_results, "%s_%s_parcellation.nii.gz" % (str(i), clustering_method)))

    def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=2):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = path + 'preloaded_masker/mask_all_subj_movie.nii'

    return image_list, movie_labels, masker_path

    def make_train_test_sherlock(image_list, labels, masker):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Extract scenes
        Parameters :
        - List of int fold out of 50 scenes
        - Dataframe labels (scene, onset, offset)
        - List of nii files subject_imgs

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list

    def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test

    # %%
    import numpy as np
    import nibabel as nib
    from nilearn.input_data import NiftiMasker
    from scipy.spatial.distance import cdist

    def make_coordinates_grid(input_shape):
    ''' Make a grid of coordinates

    '''
    x_ = range(0, input_shape[0])
    y_ = range(0, input_shape[1])
    z_ = range(0, input_shape[2])
    print(input_shape[0])
    coordinates_points = np.vstack(np.meshgrid(
        x_, y_, z_, indexing='ij')).reshape(3, -1).T
    coordinates_matrice = coordinates_points.reshape(
        (input_shape[0], input_shape[1], input_shape[2], 3), order='C')
    return coordinates_matrice

    def make_coordinates_image(input_shape):
    coordinates_matrice = make_coordinates_grid(input_shape)
    affine = np.eye(4)
    coordinates_image = nib.nifti1.Nifti1Image(
        coordinates_matrice, mask.affine)
    plain_mask = nib.nifti1.Nifti1Image(np.ones(input_shape), mask.affine)
    return coordinates_image, plain_mask

    co_im, _ = make_coordinates_image(mask.shape)

    co_im.affine
    for image, coord in zip(iter_img(co_im), ['x', 'y', 'z']):
    plot_stat_map(image, display_mode=coord,
                  cut_coords=10)

    masker = NiftiMasker(mask)
    masker.fit()
    ref = data[0]
    ref
    data = masker.transform(co_im).T
    from scipy.spatial.distance import euclidean
    distance_to_origin = [euclidean(ref, point) for point in data]
    distance_im = masker.inverse_transform(distance_to_origin)

    plot_stat_map(distance_im)

    def make_affine_coordinates(mask):
    coordinates_image, plain_mask = make_coordinates_image(mask.shape)

    coordinates_vector = masker.transform(coordinates_image)
    affine_coordinates = image.coord_transform(
        coordinates_vector[1], coordinates_vector[0], coordinates_vector[2], mask.affine)
    return affine_coordinates

    #    distance_matrix = cdist(coordinates_vector, coordinates_vector)
    #    return distance_matrix

    import matplotlib.pyplot as plt
    % matplotlib inline
    from nilearn.image import iter_img
    from nilearn.plotting import plot_stat_map
    from nilearn import datasets, image

    input_shape
    niimg = datasets.fetch_haxby()
    mask = image.load_img(niimg.mask)
    mask.shape
    mask.affine

    niimg.mask.shape
    coordinates_image, plain_mask = make_coordinates_image(mask.shape)
    coordinates_image.shape
    input_shape = niimg.shape

    masker = NiftiMasker(plain_mask)
    masker.fit()
    coordinates_vector = masker.transform(coordinates_image)
    coordinates_vector[2].shape
    niimg.affine
    # Find the MNI coordinates of the voxel (50, 50, 50)
    affine_coordinates = image.coord_transform(
    coordinates_vector[1], coordinates_vector[0], coordinates_vector[2], mask.affine)
    np.shape(affine_coordinates)

    _test_affine_coordinates_image(mask)

    def _test_coordinates_image(input_shape):
    coordinates = make_coordinates_image(input_shape, mask)
    # check that each image contains coordinates matching slices height
    for image, coord in zip(iter_img(grid_image), ['y', 'x', 'z']):
        plot_stat_map(image, display_mode=coord,
                      cut_coords=np.max(input_shape))

    def _test_affine_coordinates_image(mask):
    affine_coordinates = make_affine_coordinates(mask)
    masker = NiftiMasker(plain_mask)
    masker.fit()
    affine_image = masker.inverse_transform(affine_coordinates)
    # check that each image contains coordinates matching slices height
    for image, coord in zip(iter_img(affine_image), ['x', 'y', 'z']):
        plot_stat_map(image, display_mode=coord,
                      cut_coords=10)

    def _test_distance_matrix(input_shape, mask):
    dist_matrix = make_distance_matrix(input_shape, mask)
    plt.imshow(dist_matrix)
    plt.colorbar()
    print('Max distance : %s ' % (np.max(dist_matrix)))

    # %%
    import numpy as np
    import os
    from nilearn.image import iter_img, concat_imgs, load_img
    from functional_alignment.utils import score_table
    from nilearn.image import index_img, load_img, math_img
    from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
    import ibc_public
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
               'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                   conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    image_list = []
    i = 1
    for subject in subject_list:
    subject_path = []
    subject_path.extend([df[df.acquisition == 'pa'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions])

    image_list.append(subject_path)
    test = concat_imgs(subject_path)
    test.to_filename(os.path.join(
        "/storage/tompouce/tbazeill/ibc/antspy_ap_pa", "%s_test.nii.gz" % str(i)))
    i += 1
    # %%

    def paths_epsilon_ot(dataset, pairs_to_load="all", loss="r2_star", methods="all", regs=[0.3, 0.7, 2]):
    if dataset is "ibc_ap_pa":
        dataset = "ibc"
        subset = "ap_pa"
    else:
        subset = None
    if dataset is "ibc":
        all_pairs_done = [(12, 8), (2, 5), (2, 8), (3, 11),
                        (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
        mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
        mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
        masker = NiftiMasker(
            memory_level=5, memory=mask_memory)
        masker.fit(mask_path)
    if pairs_to_load == "all":
        pairs_to_load = all_pairs_done

    all_methods = ["1_bootstraps_piecewise_optimal_transport",
                 "20_bootstraps_piecewise_optimal_transport"]
    path_to_results = "/storage/tompouce/tbazeill/" + dataset + "/"
    if methods == "all":
        methods = all_methods
    methods_header = []
    paths = []
    for i, method in enumerate(methods_header):
        regs_path = []
        for reg in regs:
            method_paths = []
            for pair in pairs_to_load:
                subj_1, subj_2 = pair[0], pair[1]
                path = path_to_results + method + '/' + '_'.join(
                    filter(None, [str(subj_1), str(subj_2), str(reg), subset]))
                method_paths.append(path + "_" + loss + ".nii.gz")
            regs_path.append(method_paths)
            methods_header.append(method + "_" + str(reg))
        paths.append(regs_path)
        print("Method %s added in memory" % str(method))
    return paths, methods_header

    # %%
    n_bootstrap = 1
    # And other methods parameters
    n_splits = 4  # n_subjectsumber of folds for validations
    n_pieces = 100
    srm_components = 50  # number of pieces for piecewise alignement

    n_subjects = 2
    path = "/Users/thomasbazeille/Documents/Stage/thomas_bazeille/Retreat-Project-Sherlock/Sherlock/"
    path_to_results = "/storage/tompouce/tbazeill/sherlock_pair_alignment/"

    image_list, movie_labels, masker_path = load_sherlock_data(
    n_subjects=n_subjects)
    masker = NiftiMasker()
    masker.fit(masker_path)
    subject_pair = (0, 1)
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    X_train_list, X_test_list = make_train_test_sherlock(
    image_list, movie_labels, masker)
    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
    masker, X_1_train, X_2_train, X_1_test)
    from functional_alignment.piecewise_alignment import PieceWiseAlignment
    import time

    piecewise = PieceWiseAlignment(method='scaled_orthogonal',
                                 n_pieces=n_pieces, mask=masker, n_bootstrap=n_bootstrap)
    piecewise.fit(Im_1_train, Im_2_train)
    piecewise.labels_
    start = time.time()
    sparse_R = piecewise.sparse_first_transform()
    end = time.time()
    print("time to create CSR : %s" % str(end - start))
    # %%
    X_1_train.shape
    np.vstack([X_1_train, X_2_train]).shape

    test_labels = [[0, 0, 1, 2, 0, 2, 1]]
    fit_0 = np.asarray([[1, 2, 3], [2, 1, 3], [1, 2, 3]])
    fit_1 = np.asarray([[4, 4], [7, 7]])
    fit_2 = np.asarray([[5, 6], [6, 5]])
    test_fit = [fit_0, fit_1, fit_2]
    np.shape(test_fit[1])
    R_test = np.zeros((len(test_labels[0]), len(test_labels[0])))
    R_test.shape
    i
    test_unique_labels, test_counts = np.unique(
        test_labels, return_counts=True)
    # %%
    labels = piecewise.labels_[0]
    unique_labels
    unique_labels, counts = np.unique(labels, return_counts=True)
    # %%
    import scipy.sparse
    A = [[1, 1], [1, 1]]
    B = [[2, 2], [2, 2]]
    C = [[3, 3], [3, 3]]

    results = scipy.sparse.block_diag([A, B, C]).tocsr()
    results
    bresults = scipy.sparse.bsr_matrix(results)
    bresults.data[1]
    # %%
    norm = 0
    for i in range(5):
    A = np.ones((4, 7))
    norm += np.linalg.norm(A)
    C.append(A)
    norm / 5
    np.linalg.norm(A)
    5 * np.linalg.norm(A) / 5
    np.linalg.norm(C)

    # %%

    indexes = get_indexes(labels[0][3], labels[0])
    np.shape(indexes)
    indexes
    # %%
    Im_1_pred = piecewise_srm.transform([Im_1_test], index=0, index_inverse=1)
    X_pred = masker.transform(Im_1_pred)
    X_GT = X_2_test
    # voxelwise R2 score
    X_R2 = [max(r2_score(X_GT[:, j], X_pred[:, j]), -1)
            for j in range(np.shape(X_GT)[1])]
    Im_R2 = masker.inverse_transform(X_R2)

    Im_R2.to_filename(
    path_to_results + "/20_bootstraps_piecewise_srm_50_" + str(subj_1) + "_" + str(subj_2) + "_r2.nii.gz")

    # %%

    import os
    import time

    import numpy as np

    from nilearn.input_data import NiftiMasker
    from nilearn.image import index_img, load_img, math_img
    from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
    import ibc_public
    from sklearn.model_selection import ShuffleSplit

    from functional_alignment.hyperalignment import hyperalign
    from functional_alignment.ridge_cv import RidgeCV
    from functional_alignment.piecewise_alignment import PieceWiseAlignment
    from functional_alignment.deterministic_srm import DeterministicSRM
    from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
    from functional_alignment.utils import score_save_table, make_train_test_images
    from functional_alignment.load_data import load_sherlock_data, load_forrest_data, load_ibc_3_mm_data, make_train_test_sherlock, make_train_test_forrest, make_train_test_ibc_3_mm
    from functional_alignment.template import TemplateAlignment, euclidian_mean_with_masking
    from functional_alignment.utils import score_table
    from functional_alignment.ot_template_alignment import OptimalTransportTemplate

    data_dir = os.path.join(THREE_MM, 'group')
    dataset = 'ibc'
    n_bootstraps = [1, 20]
    n_jobs = 4
    scale_template = False
    method = "optimal_transport"
    template_method = "optimal"
    mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
    mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
    masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
    masker.fit(mask_path)
    path_to_results = "/storage/tompouce/tbazeill/ibc/template_alignment"
    mask = load_img(mask_path)

    # Access to the data
    if dataset == 'ibc':
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
               'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm', ]
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                    conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    rs = ShuffleSplit(n_splits=5, test_size=.4, random_state=0)

    template_folds, mapping_folds, gt_folds = [], [], []

    for train_subjects, test_subjects in rs.split(subject_list):
        template_paths = []
        for subject in subject_list[train_subjects]:
            subject_path = []
            subject_path.extend([df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            subject_path.extend([df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            template_paths.append(subject_path)
        template_folds.append(template_paths)

        test_mapping = []
        test_gt = []
        for subject in subject_list[test_subjects]:
            ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_mapping.append(ap_path)
            pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_gt.append(pa_path)
        mapping_folds.append(test_mapping)
        gt_folds.append(test_gt)

    # for template_train_imgs, mapping_test_imgs, gt_test_imgs in zip(template_folds, mapping_folds, gt_folds):
    df
    cv = 0
    train_index = np.asarray(range(53))
    test_index = np.asarray(range(53, 106))
    template_train_imgs, mapping_test_imgs, gt_test_imgs = template_folds[
    cv], mapping_folds[cv], gt_folds[cv]

    X_gts = []
    for i in range(len(gt_test_imgs)):
    X_gt = masker.transform(gt_test_imgs[i])
    X_gts.append(X_gt)

    '''
a = []
for i in [5, 6, 3, 1, 4, 0, 2]:
    a.append(template_train_imgs[i])
template_train_imgs = a
'''
    '''
for n_bags in n_bootstraps:
    if template_method == "optimal":
        template_estim = OptimalTransportTemplate(
            n_pieces=200, metric="euclidean", reg=1e-1, n_bootstrap=n_bags, joint_clustering=True, mask=masker, n_jobs=n_jobs)
        template_estim.fit(template_train_imgs, n_iter=4)

        '''
    X_list = []
    for img in template_train_imgs:
    X_ = masker.transform(img)
    if type(X_) == list:
        X_ = np.concatenate(X_, axis=0)
    X_ = X_.T
    X_list.append(X_)
    mean_X_ = np.mean(X_list, axis=0)

    # %%
    % matplotlib inline
    from scipy.sparse import block_diag, bsr_matrix
    from scipy.spatial.distance import cdist
    import matplotlib.pyplot as plt
    from functional_alignment.ot_pairwise_hyperalignment import hierarchical_k_means
    labels = hierarchical_k_means(X_list[0], 200)
    unique_labels, counts = np.unique(labels, return_counts=True)
    counts

    X = X_list[0]
    X_t = X_list[1]
    for j, cluster in enumerate(unique_labels[:20]):
    i = labels == cluster
    X_s_, contrasts, mean_, X_t_ = [], [], [], []
    # 2 contrasts
    contrasts.append("2_visual_contrasts")
    X_s_.append(X[i][:, 2:4])
    mean_.append(mean_X_[i][:, 2:4])
    X_t_.append(X_t[i][:, 2:4])

    # 10 contrast
    contrasts.append("10_visual_contrasts")
    a = [2, 3, 7, 9, 10, 16, 20, 23, 24, 25]
    X_s_.append(X[i][:, a])
    mean_.append(mean_X_[i][:, a])
    X_t_.append(X_t[i][:, a])
    # Archi contrasts
    contrasts.append("Archi_contrasts")
    X_s_.append(X[i][:, :29])
    mean_.append(mean_X_[i][:, :29])
    X_t_.append(X_t[i][:, :29])
    # All contrasts
    contrasts.append("All_contrasts")
    X_s_.append(X[i])
    mean_.append(mean_X_[i])
    X_t_.append(X_t[i])
    plt.figure(j)
    for X_, Xt_, mean_X, contrast in zip(X_s_, X_t_, mean_, contrasts):
        dists = cdist(zscore(X_), zscore(Xt_), metric='euclidean').flatten()
        # hamming_dist = cdist(X_, mean_X, metric='hamming').flatten()
        plt.hist(dists, bins=30, label=contrast)
        plt.legend()
    plt.title('Euc dists, region %s' %
              (cluster))
    # a[1].hist(hamming_dist, bins=30)
    # a[1].set_title('Hamming dists %s, region %s' % (contrast, cluster))
    X_.shape
    np.mean(X_, axis=1).shape

    from nilearn.plotting import plot_stat_map
    plot_stat_map(
    '/storage/tompouce/tbazeill/ibc/1_bootstraps_piecewise_optimal_transport/12_8_1_ap_pa_zero_mean_r2.nii.gz', vmax=.5)
    plot_stat_map(
    '/storage/tompouce/tbazeill/ibc/1_bootstraps_piecewise_optimal_transport/12_8_5_ap_pa_zero_mean_r2.nii.gz', vmax=.5)

    def zscore(X):
    X_ = X - X.mean(axis=1, keepdims=True)
    stds = X.std(axis=1)

    for i in range(len(X)):
        if stds[i] == 0:
            X_[i] = np.zeros_like(X[i])
        else:
            X_[i] /= stds[i]
    return X_

    X_z = zscore(X_)
    X_.mean()
    X_.std()
    X_z.mean()
    X_z.std()
    # %%

    data_mean_dist = []
    data_dist_std = []
    all_dists = []
    for X in X_list:
    label_mean_dist = []
    label_dist_std = []
    label_all_dists = []
    for label in unique_labels:
        i = labels == label
        M = cdist(X[i], mean_X_[i], metric='euclidean')
        label_mean_dist.append(M.mean())
        label_dist_std.append(M.std())
        label_all_dists.append(M.flatten())
    data_mean_dist.append(label_mean_dist)
    data_dist_std.append(label_dist_std)
    all_dists.append(label_all_dists)
    dists = np.asarray(data_mean_dist)
    stds = np.asarray(data_dist_std)
    all_dists = np.asarray(all_dists)
    M.size
    all_dists.shape

    import matplotlib.pyplot as plt
    import numpy as np
    % matplotlib inline
    plt.hist(np.concatenate(all_dists[0]), normed=True, bins=30)

    dists[0].mean()
    dists[1:].mean()
    stds[0].mean()
    stds[1:].mean()

    for X_i in X_i_list:
    M = cdist(X_i, template_i, metric='euclidean'))
    M.mean()
for M in M_list:
    print(M.mean())
    print(M.std())
M_list
reg=1e-6
n=len(template_i)
len(M_list)
s=len(M_list)
b=1 / n * np.ones(n * s)
# %%
Ks_list=[]
for M in M_list:
    print(M)
    M=np.asarray(M, dtype = np.float64)
    Ks=np.empty_like(M)
    np.divide(M, -reg, out = Ks)
    np.exp(Ks, out = Ks)
    Ks_list.append(Ks)
K=block_diag(Ks_list).tocsr()
u, v=1 / n * np.ones(n * s), 1 / n * np.ones(n * s)
loop=1
cpt=0
err=1
print_period=10
verbose=True
numItermax=100
while(loop):
    # sinkhorn update
    uprev=u
    vprev=v
    np.divide(b, K.T.dot(u) + 1e-16, out = v)
    a=np.ones(len(b))
    Kv=K.dot(v) + 1e-16
    for i in range(len(M_list)):
        a *= np.roll(Kv, int(i * (len(b) / len(M_list))))
    a
    np.divide(a, Kv, out = u)

    if np.any(np.isnan(u)) or np.any(np.isnan(v))or np.any(np.isinf(u)) or np.any(np.isinf(v)):
        # we have reached the machine precision
        # come back to previous solution and quit loop
        print('Warning: numerical errors at iteration', cpt)
        u=uprev
        v=vprev
        break

    if cpt % print_period == 0:
        # we can speed up the process by checking for the error only all
        # the 10th iterations
        err=np.linalg.norm(u - uprev)**2 / np.sum((u)**2) + \
            np.linalg.norm(v - vprev)**2 / np.sum((v)**2)
        if verbose:
            if cpt % (print_period * 20) == 0:
                print(
                    '{:5s}|{:12s}'.format('It.', 'Improvement') + '\n' + '-' * 19)
            print('{:5d}|{:8e}|'.format(cpt, err))

    # if err <= stopThr:
    #    loop = False
    if cpt >= numItermax:
        loop=False
    cpt=cpt + 1
bsr_matrix(K.T.multiply(u).T.multiply(v)).data
# %%
from functional_alignment.ot_template_alignment import make_template_one_parcellation, make_template_piece, sinkhorn_joint_epsilon_scaling, sinkhorn_joint, sinkhorn_joint_stabilized
P_list=sinkhorn_joint(n, M_list, reg, numItermax = 100)
P_list=sinkhorn_joint_epsilon_scaling(n, M_list, reg, numItermax = 1, epsilon0 = 1e4, numInnerItermax = 150,
                                        tau = 1e3, stopThr = 1e-9, warmstart = None, verbose = True, print_period = 10)
len(M_list)
P_list=sinkhorn_joint_stabilized(
    b, M_list, reg, numItermax = 10, verbose = True)
np.shape(P_list)
import scipy.linalg
# %%
Ks_list=[]
M_list[2]
for M in M_list:
    Ks=np.divide(M, -reg)
    np.exp(Ks, out = Ks)
    Ks_list.append(Ks)
np.shape(Ks_list)
K_base=scipy.linalg.block_diag(*Ks_list)
K_base.shape
b=np.asarray(b, dtype = np.float64)
tau=1e3
# init data
cpt=0
# we assume that no distances are null except those of the diagonal of
# distances


def get_K(alpha, beta):
    """log space computation"""
    Alp=np.empty_like(alpha)
    np.divide(alpha, reg, out = Alp)
    np.exp(Alp, out = Alp)
    Bet=np.empty_like(beta)
    np.divide(beta, reg, out = Bet)
    np.exp(Bet, out = Bet)
    return (K_base.T * Alp).T * Bet


def get_Gamma(alpha, beta, u, v):
    """log space gamma computation"""
    return (get_K(alpha, beta).T * u).T * v


warmstart=None
if warmstart is None:
    alpha, beta=np.zeros(len(b)), np.zeros(len(b))
    K=K_base
    transp=K_base
else:
    alpha, beta=warmstart
    K=get_K(alpha, beta)
    transp=K
u, v=np.ones(len(b)) / (len(b) / len(M_list)
                          ), np.ones(len(b)) / (len(b) / len(M_list))

# print(np.min(K))
stopThr=1e-11
loop=1
cpt=0
err=1
while loop:
    # sinkhorn update
    uprev=u
    vprev=v
    np.divide(b, K.T.dot(u) + 1e-16, out = v)
    a=np.ones(len(b))
    Kv=K.dot(v) + 1e-16
    for i in range(len(M_list)):
        a *= np.roll(Kv, int(i * (len(b) / len(M_list))))
    np.divide(a, Kv, out = u)

    # remove numerical problems and store them in K
    if np.abs(u).max() > tau or np.abs(v).max() > tau:
        alpha, beta=alpha + reg * np.log(u), beta + reg * np.log(v)
        u, v=np.ones(len(b)) / (len(b) / len(M_list)
                                  ), np.ones(len(b)) / (len(b) / len(M_list))
        K=get_K(alpha, beta)

    if cpt % print_period == 0:
        # we can speed up the process by checking for the error only all
        # the 10th iterations
        transp=get_Gamma(alpha, beta, u, v)
        err=np.linalg.norm((np.sum(transp, axis=0) - b))**2
        if verbose:
            if cpt % (print_period * 20) == 0:
                print(
                    '{:5s}|{:12s}'.format('It.', 'Err') + '\n' + '-' * 19)
            print('{:5d}|{:8e}|'.format(cpt, err))

    if err <= stopThr:
        loop = False
    if cpt >= numItermax:
        loop = False

    if np.any(np.isnan(u)) or np.any(np.isnan(v)):
        # we have reached the machine precision
        # come back to previous solution and quit loop
        print('Warning: numerical errors at iteration', cpt)
        u = uprev
        v = vprev
        break

    cpt = cpt + 1
G = get_Gamma(alpha, beta, u, v)
G
alpha += reg * np.log(u)
beta += reg * np.log(v)

# %%
from functional_alignment.ot_pairwise_hyperalignment import fit_piecewise_align
regs = [1000, 100, 10, 1, 0.5, 0.3, 0.2, 1e-1, 5e-2, 1e-2, 1e-3, 1e-4, 1e-5]
methods = ['regularized', 'exact', 'stabilized', 'epsilon_scaling']
metric = "euclidean"
method = methods[3]
for reg in regs:
    K, log = fit_piecewise_align(
        X_i_list[0], X_i_list[1], reg, method=method, metric=metric)
    regularization_metric = np.mean(
        [sum(1 for x in b if x > 3e-2) for b in K.T])
    print('%s : %s' % (method, str(reg)), regularization_metric)
# %%
from functional_alignment.ot_template_alignment import make_template_one_parcellation, make_template_piece, sinkhorn_joint_epsilon_scaling, sinkhorn_joint


template_i, template_history, = make_template_piece(
    X_i_list, template_i, reg=5e-1, metric='euclidean', n_iter=4)
% debug
# %%
M_list = []
from scipy.spatial.distance import cdist
from scipy.sparse import block_diag
for X_i in X_i_list:
    M_list.append(cdist(X_i, template_i, metric='euclidean'))

s = len(M_list)
b = np.ones(n * s) * 1 / n
# init data
# nrelative umerical precision with 64 bits
numItermin = 35
numItermax = 50  # ensure that last velue is exact
numInnerItermax = 100
cpt = 0
# we assume that no distances are null except those of the diagonal of
# distances
alpha, beta = np.zeros(n * s), np.zeros(n * s)
verbose = False
print_period = 30
reg = 1
epsilon0 = 1e4
tau = 1e3
stopThr = 1e-8


def get_reg(iter):  # exponential decreasing
    return (epsilon0 - reg) * np.exp(-iter) + reg


loop = 1
cpt = 0
err = 1
warmstart = None
while loop:
    n, M_list, reg,
    regi = get_reg(cpt)

    Ks_list = []
    for M in M_list:
        M = np.asarray(M, dtype=np.float64)
        Ks = np.empty_like(M)
        np.divide(M, -regi, out=Ks)
        np.exp(Ks, out=Ks)
        Ks_list.append(Ks)
    K_base = block_diag(Ks_list).tocsr()
    b = np.asarray(b, dtype=np.float64)
    # init data
    cpt = 0
    # we assume that no distances are null except those of the diagonal of
    # distances

    def get_K(alpha, beta):
        """log space computation"""
        Alp = np.empty_like(alpha)
        np.divide(alpha, regi, out=Alp)
        np.exp(Alp, out=Alp)
        Bet = np.empty_like(beta)
        np.divide(beta, regi, out=Bet)
        np.exp(Bet, out=Bet)
        return (K_base.T * Alp).T * Bet

    def get_Gamma(alpha, beta, u, v):
        """log space gamma computation"""
        return (get_K(alpha, beta).T * u).T * v

    if warmstart is None:
        alpha, beta = np.zeros(len(b)), np.zeros(len(b))
        K = K_base
        transp = K_base
    else:
        alpha, beta = warmstart
        K = get_K(alpha, beta)
        transp = K
    u, v = np.ones(len(b)) / (len(b) / len(M_list)
                              ), np.ones(len(b)) / (len(b) / len(M_list))

    # print(np.min(K))

    loop_bis = 1
    cpt_bis = 0
    err_bis = 1
    while loop_bis:
        # sinkhorn update
        uprev = u
        vprev = v
        np.divide(b, K.T.dot(u) + 1e-16, out=v)
        a = np.ones(len(b))
        Kv = K.dot(v) + 1e-16
        for i in range(len(M_list)):
            a *= np.roll(Kv, int(i * (len(b) / len(M_list))))
        np.divide(a, Kv, out=u)

        # remove numerical problems and store them in K
        if np.abs(u).max() > tau or np.abs(v).max() > tau:
            alpha, beta = alpha + regi * np.log(u), beta + regi * np.log(v)
            u, v = np.ones(len(b)) / (len(b) / len(M_list)
                                      ), np.ones(len(b)) / (len(b) / len(M_list))
            K = get_K(alpha, beta)

        if cpt_bis % print_period == 0:
            # we can speed up the process by checking for the error only all
            # the 10th iterations
            transp = get_Gamma(alpha, beta, u, v)
            err = np.linalg.norm((np.sum(transp, axis=0) - b))**2
            if verbose:
                if cpt_bis % (print_period * 20) == 0:
                    print(
                        '{:5s}|{:12s}'.format('It.', 'Err') + '\n' + '-' * 19)
                print('{:5d}|{:8e}|'.format(cpt, err))

        if err_bis <= stopThr:
            loop_bis = False
        if cpt_bis >= numInnerItermax:
            loop_bis = False

        if np.any(np.isnan(u)) or np.any(np.isnan(v)):
            # we have reached the machine precision
            # come back to previous solution and quit loop
            print('Warning: numerical errors at iteration', cpt)
            u = uprev
            v = vprev
            break

        cpt_bis = cpt_bis + 1
    G = get_Gamma(alpha, beta, u, v)
    alpha += regi * np.log(u)
    beta += regi * np.log(v)
    warmstart = (alpha, beta)
    if cpt >= numItermax:
        loop = False

    if cpt % (print_period) == 0:  # spsion nearly converged
        # we can speed up the process by checking for the error only all
        # the 10th iterations
        transp = G
        err = np.linalg.norm((np.sum(transp, axis=0) - b))**2

        if verbose:
            if cpt % (print_period * 10) == 0:
                print(
                    '{:5s}|{:12s}'.format('It.', 'Err') + '\n' + '-' * 19)
            print('{:5d}|{:8e}|'.format(cpt, err))

    if err <= stopThr and cpt > numItermin:
        loop = False

    cpt = cpt + 1
bsr_matrix(G).data
# %% Copying data for fmralign example

from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
subject_list = SUBJECTS
task_list = ["archi_standard", "archi_spatial", "archi_social", "archi_emotional",'hcp_language', 'hcp_social', 'hcp_gambling', 'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                 conditions=CONDITIONS, task_list=task_list)
conditions = df[df.modality == 'bold'].contrast.unique()

# copy data and mask
path_to_folder = "/storage/tompouce/tbazeill/fmralign/fmralign_pairwise_example"
from shutil import copy2
import os


def convert64niimg_in32(img):
    from nilearn.image import load_img
    from nilearn.image import new_img_like
    im_64 = load_img(img)
    data_32 = np.float32(im_64.get_data())
    im_32 = new_img_like(im_64,data_32)
    return im_32


new_df=pd.DataFrame(columns=["subject", "task", "condition", "acquisition",
                           "path"])
for subject in SUBJECTS:
    for condition in conditions:
        for phase in ['ap', 'pa']:
            relevant = df[df.acquisition == phase][df.subject == subject][
                df.contrast == condition]
            source_path = relevant.path.values[-1]
            task = relevant.task.values[-1]
            int_path = '%s/%s_%s.nii.gz' % (subject, condition, phase)
            dest_path = os.path.join(
                path_to_folder, int_path)
            im_32 = convert64niimg_in32(source_path)
            im_32.to_filename(dest_path)
            # copy2(source_path, dest_path)
            new_df=new_df.append({"subject": subject, "condition": condition, "task": task, "acquisition": phase,
                            "path": os.path.join("path_to_dir",int_path)}, ignore_index=True)
copy2(mask_path, os.path.join(path_to_folder, "gm_mask_3mm.nii.gz"))
new_df.to_csv(os.path.join(path_to_folder,"ibc_3mm_all_subjects_metadata.csv"))

# %%

see = pd.DataFrame.from_csv(os.path.join(path_to_folder,"ibc_3mm_all_subjects_metadata.csv"))
see
# write conditions
import json
# open output file for writing
with open(os.path.join(path_to_folder, 'conditions.txt'), 'w') as filehandle:
    json.dump(list(conditions), filehandle)

# load conditions
conditions_2 = []
# open output file for reading
with open(os.path.join(path_to_folder, 'conditions.txt'), 'r') as filehandle:
    conditions_2 = json.load(filehandle)
# %%
