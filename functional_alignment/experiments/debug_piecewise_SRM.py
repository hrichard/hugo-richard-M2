import numpy as np
import nibabel as nib
from functional_alignment.deterministic_srm import DeterministicSRM
from nilearn.input_data import NiftiMasker
from nilearn.image import index_img
from sklearn.metrics import r2_score
from functional_alignment.deterministic_srm import _deterministic_srm
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM, create_fit_matrix


mask = nib.Nifti1Image(np.random.binomial(
    1, 0.4, size=(10, 10, 16)), np.eye(4))

masker = NiftiMasker(mask_img=mask)
masker.fit()


subject_image_list = []
for i in range(10):
    image = nib.Nifti1Image(np.random.randn(10, 310, 16, 100), np.eye(4))
    subject_image_list.append(image)

image_fold_1 = list(range(47))
image_fold_1.extend([97, 98, 99])
image_fold_1
image_fold_2 = list(range(47, 97))
image_fold_2

X_train, Y_train = [], []
for i in range(5):
    X_train.append(index_img(subject_image_list[i], image_fold_1))
    Y_train.append(index_img(subject_image_list[i], image_fold_2))

X_test, Y_test = [], []
for i in range(5, 10):
    X_test.append(index_img(subject_image_list[i], image_fold_1))
    Y_test.append(index_img(subject_image_list[i], image_fold_2))


# Test piecewise deterministic_srm


n_pieces = 100
piecewise_srm = PieceWiseAlignment_DeterministicSRM(n_pieces=1, n_components=100,
                                                    mask=masker, verbose=2)
piecewise_srm.fit(X_train[0], X_train[0])
np.shape(piecewise_srm.labels_)
np.shape(piecewise_srm.basis_)
X_train_table = masker.transform(X_train[0])
Y_train_table = masker.transform(Y_train[0])
np.shape(piecewise_srm.basis_)

no_masker = None
det_masker, det_basis, det_scale, det_shared_response = _deterministic_srm(
    no_masker, [X_train_table, X_train_table])
np.shape(det_basis)
np.array_equal(piecewise_srm.basis_[0][0][1], det_basis[1])
np.shape(piecewise_srm.labels_[0][0])
type(piecewise_srm.labels_[0][0])
piecewise_srm.basis_[0][0][1]
det_basis[1]
pred = piecewise_srm.transform([X_test[0]], index=0, index_inverse=1)

# %%
X_pred = masker.transform(pred)
X_GT = masker.transform(X_test[0])
print(r2_score(X_pred, X_GT))


# test deterministic_srm function on table
srm_estim = DeterministicSRM(
    n_components=20, max_iter=10, detrend=False, mask=masker)
X_train_table = masker.transform(X_train[0])
Y_train_table = masker.transform(Y_train[0])
no_masker = None
b = _deterministic_srm(no_masker, [X_train_table, Y_train_table])
'''
piecewise_srm_2 = PieceWiseAlignment_DeterministicSRM(
    n_pieces=100, n_components=500, n_bootstrap=2, mask=masker)
piecewise_srm_2.fit(X_train[0], X_train[0])
pred = piecewise_srm_2.transform([X_test[0]], index=0, index_inverse=1)
Y_pred_2 = masker.transform(pred)
Y_GT = masker.transform(X_test[0])
print(r2_score(Y_pred_2, Y_GT))
'''
piecewise_srm.labels_
np.shape(piecewise_srm.basis_)

# TODO : try with the labels to see if we have the same components with deterministic_srm_ on each piece if yes, it's a problem with deterministic_srm, if not it is in between.
# and with the class

X_train_table = masker.transform(X_train[0])
Y_train_table = masker.transform(Y_train[0])
np.shape(Y_train_table[0])
labels = np.zeros_like(Y_train_table[0])
labels[0] = 1
X_train_table_0 = [X_t[1:] for X_t in X_train_table]
Y_train_table_0 = [X_t[1:] for X_t in Y_train_table]
np.shape(X_train_table_0)

unique_labels = np.unique(labels)
method = "srm"

no_masker = None
det_masker, det_basis, det_scale, det_shared_response = _deterministic_srm(
    no_masker, [X_train_table_0, Y_train_table_0])
piecewise_det_basis, piecewise_det_scale = create_fit_matrix(
    method, unique_labels, labels, X_train_table, Y_train_table)
np.assert_array_almost_equal(det_basis, piecewise_det_basis[0])

X_train_table.shape
labels.shape
type(labels)

debug
from functional_alignment.piecewise_alignment import generate_Xi_Yi
from functional_alignment.piecewise_deterministic_srm import fit_one_parcellation
infos = None

X_ = np.concatenate(X_train_table, axis=0).T
Y_ = np.concatenate(Y_train_table, axis=0).T
X_.shape
n_pieces = 2
'''outputs = fit_one_parcellation(X_, Y_,
                               masker.mask_img.get_data(), n_pieces, train_index=, mem,
                         n_jobs=1, verbose=True
'''
ex_labels_ = [output[0] for output in outputs]
ex_basis_ = [output[1] for output in outputs]
ex_scale_ = [output[2] for output in outputs]
# generate_Xi_Yi(unique_labels, labels, X_train_table, Y_train_table, infos)
