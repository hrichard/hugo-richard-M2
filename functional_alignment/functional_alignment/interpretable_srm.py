import numpy as np
from sklearn.externals.joblib import Memory
import logging
from functional_alignment.baseSRM import BaseSRM, mask_and_reduce
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img
from sklearn.utils import check_random_state
from nilearn.decoding.objective_functions import _gradient, _div
from nilearn.decoding.proximal_operators import _prox_l1
from functional_alignment.fista import mfista
from functional_alignment.deterministic_srm import fast_srm
from sklearn.externals.joblib import Memory, Parallel, delayed
from functional_alignment.pca_srm import _pca_srm


def _energy_datafitting(data_i, basis_i, shared_response):
    """
    Computes ||data_i - basis_i.dot(shared_response)||**2 + sum_k grad(basis_i[:, k]) ** 2
    Parameters
    ----------
    data_i: np array of shape n_voxels, n_timeframes
    basis_i: np array of shape n_voxels, n_components
    shared_response: np array of shape n_components, n_timeframes
    Returns
    -------
    cost: float
    """
    E = 0.5 * np.sum((data_i - basis_i.dot(shared_response)) ** 2)
    return E


def _energy_smoothness(basis_i, grad_weight, mask, parallel):
    """
    Computes ||data_i - basis_i.dot(shared_response)||**2 + sum_k grad(basis_i[:, k]) ** 2
    Parameters
    ----------
    Returns
    -------
    cost: float
    """

    if grad_weight == 0:
        return 0
    else:
        n_voxels, n_components = basis_i.shape
        grad_cost_list = parallel(
            delayed(compute_grad_cost)(
                mask,
                basis_i[:, k])
            for k in range(n_components))

        return 0.5 * grad_weight * np.sum(grad_cost_list)


def _energy_common(basis_i, basis_partial_sum, common_weight, n_subjects):
    """
    Computes the energy
    Parameters
    ----------

    Returns
    -------
    """
    if common_weight == 0:
        return 0
    else:
        return 0.5 * common_weight * np.sum((basis_i - (basis_partial_sum + basis_i) / n_subjects ) ** 2)


def compute_grad_cost(mask, basis_ik):
    grad_buffer = np.zeros(mask.shape)
    grad_buffer[mask] = basis_ik
    grad_mask = np.tile(mask, [mask.ndim] + [1] * mask.ndim)
    grad_section = _gradient(grad_buffer)[grad_mask]
    grad_cost = np.dot(grad_section, grad_section)
    return grad_cost


def compute_lap_col(mask, basis_ik):
    image_buffer = np.zeros(mask.shape)
    image_buffer[mask] = basis_ik
    d_k = _div(_gradient(image_buffer))[mask]
    return d_k


def _energy_smooth(data_i, basis_i, shared_response, basis_partial_sum, grad_weight, common_weight,
                   n_subjects, mask, parallel):
    """
    Computes ||data_i - basis_i.dot(shared_response)||**2 + sum_k grad(basis_i[:, k]) ** 2
    Parameters
    ----------
    data_i: np array of shape n_voxels, n_timeframes
    basis_i: np array of shape n_voxels, n_components
    shared_response: np array of shape n_components, n_timeframes
    Returns
    -------
    cost: float
    """
    E = _energy_datafitting(data_i, basis_i, shared_response) + \
        _energy_smoothness(basis_i, grad_weight, mask, parallel) + \
        _energy_common(basis_i, basis_partial_sum, common_weight, n_subjects)
    return E


def _derivative_smooth_part(data_i, basis_i, shared_response, basis_partial_sum, grad_weight,
                            common_weight, n_subjects, mask, parallel):
    """
    Computes the derivative of ||data_i - basis_i.dot(shared_response)||**2 + sum_k grad(basis_i[:, k]) ** 2
    with respect to basis_i
    Parameters
    ----------
    data_i: np array of shape n_voxels, n_timeframes
    basis_i: np array of shape n_voxels, n_components
    shared_response: np array of shape n_components, n_timeframes

    Returns
    -------
    d: derivative value
    """
    n_voxels, n_components = basis_i.shape
    d_l2 = (basis_i.dot(shared_response) - data_i).dot(shared_response.T)

    if grad_weight == 0:
        lap = np.zeros_like(d_l2)
    else:
        lap = parallel(
            delayed(compute_lap_col)(
                mask,
                basis_i[:, k])
            for k in range(n_components))

        lap = - grad_weight * np.array(lap).T

    if common_weight == 0:
        d_common = 0
    else:
        d_common = common_weight * (1 - 1 / n_subjects) * (basis_i - (basis_partial_sum + basis_i) / n_subjects)

    return (d_l2 + lap + d_common).T.flatten()


def _lipschitz_constant(data_i, shared_response, basis_partial_sum, grad_weight,
                        common_weight, n_subjects, mask, parallel, n_iter=100, verbose=False):
    """
    We find the lipschitz constant of the _derivative_smooth_part operator using power method:
    This is a_{k+1} = f(a_k) / ||a_k||
    Parameters
    ----------

    Returns
    -------
    """
    n_voxels, _ = data_i.shape
    n_components, _ = shared_response.shape

    def f(x):
        x = x.reshape(n_components, n_voxels).T
        return _derivative_smooth_part(data_i, x, shared_response, basis_partial_sum, grad_weight,
                                       common_weight, n_subjects, mask, parallel)

    b = np.random.rand(n_voxels * n_components)
    L_prec = f(b).dot(b) / b.dot(b)
    if verbose:
        print("Searching Lipschitz constant")
    for _ in range(n_iter):
        f_b = f(b)
        b = np.copy(f_b)
        b = b / np.linalg.norm(b)
        L = f_b.dot(b) / b.dot(b)
        if verbose:
            print(L, np.abs(L_prec - L) / L)
        if np.abs(L_prec - L) / L < 1e-5:
            break
        L_prec = L

    return L


def _compute_basis(data_i, shared_response, basis_partial_sum, grad_weight,
                   common_weight, n_subjects, l1_weight, mask, n_iter,
                   lipschitz=None, n_jobs=1, verbose=True):
    """
    Compute basis using FISTA iteration
    Parameters
    ----------

    Returns
    -------
    """
    n_voxels, _ = data_i.shape
    n_components, _ = shared_response.shape

    with Parallel(n_jobs=n_jobs, backend="threading") as parallel:

        if lipschitz is None:
            lipschitz_constant = _lipschitz_constant(data_i, shared_response, basis_partial_sum, grad_weight,
                                                     common_weight, n_subjects, mask, parallel, n_iter=100, verbose=verbose)

        else:
            lipschitz_constant = lipschitz

        # it's always a good idea to use somethx a bit bigger
        lipschitz_constant *= 1.05

        # smooth part of energy, and gradient thereof
        def f1(w):
            w_ = w.reshape(n_components, n_voxels).T
            return _energy_smooth(data_i, w_, shared_response,
                                  basis_partial_sum, grad_weight,
                                  common_weight, n_subjects,
                                  mask, parallel)

        def f1_grad(w):
            w_ = w.reshape(n_components, n_voxels).T
            return _derivative_smooth_part(data_i, w_, shared_response, basis_partial_sum, grad_weight,
                                           common_weight, n_subjects, mask, parallel)

        # prox of nonsmooth path of energy (account for the intercept)
        def f2(w):
            return np.sum(np.abs(w)) * l1_weight

        def f2_prox(w, l, *args, **kwargs):
            return _prox_l1(w, l * l1_weight), dict(converged=True)

        # total energy (smooth + nonsmooth)
        def total_energy(w):
            return f1(w) + f2(w)

        w_init_nonflat = data_i.dot(shared_response.T).dot(np.linalg.pinv(shared_response.dot(shared_response.T)))
        w_init = w_init_nonflat.T.flatten()

        if verbose:
            print("initial data fitting term", _energy_datafitting(data_i, w_init_nonflat, shared_response))
            print("initial smoothness penalty", _energy_smoothness(w_init_nonflat, grad_weight, mask, parallel))
            print("initial common basis penalty", _energy_common(w_init_nonflat, basis_partial_sum,
                                                                 common_weight, n_subjects))
            print("initial lasso penalty", f2(w_init))

        basis, _, _ = mfista(
            f1_grad, f2_prox, total_energy, lipschitz_constant,
            n_voxels * n_components, tol=1e-4, max_iter=n_iter, verbose=verbose, init={"w": w_init})

    return basis.reshape(n_components, n_voxels).T


def _reduced_data_sharedresponse(masker,
                                 imgs,
                                 confounds=None,
                                 n_components=10,
                                 random_state=None,
                                 memory=None,
                                 memory_level=0,
                                 n_jobs=1,
                                 ):
        """
        Uses PCA to reduce data from n_voxels, n_timeframes to n_components, n_timeframes
        In reduced space, uses cca or srm to find an initial guess for shared space
        this guess is refined using dictionary learning.

        Returns
        -------
        dictionary: np array of shape n_components, n_timeframes
            the dictionary which is also the shared space.
        """
        svd_data = mask_and_reduce(
            masker,
            imgs,
            confounds=confounds,
            n_components=n_components,
            random_state=random_state,
            memory=memory,
            memory_level=memory_level,
            n_jobs=n_jobs
        )

        reduced_data = []
        for i in range(len(svd_data)):
            U, S, V = svd_data[i]
            reduced_data.append(S[:, np.newaxis] * V)
            svd_data[i] = None

        _, _, shared_coord = fast_srm(reduced_data, random_state=None, max_iter=10, use_scaling=False)
        return shared_coord


def _graphnetsrm_fit(masker,
                     imgs,
                     confounds=None,
                     n_components=10,
                     random_state=None,
                     memory=None,
                     memory_level=0,
                     n_jobs=1,
                     l1_weight=1.,
                     grad_weight=1):

    logger = logging.getLogger(__name__)
    logger.info("Initialization")
    masker.fit()
    # Initializing with fastSRM
    shared_response = _reduced_data_sharedresponse(masker,
                                                   imgs,
                                                   confounds=confounds,
                                                   n_components=n_components,
                                                   random_state=random_state,
                                                   memory=memory,
                                                   memory_level=memory_level,
                                                   n_jobs=n_jobs,
                                                   )
    logger.info("Done")

    n_subjects = len(imgs)
    # In this case it does not cost too much to keep the statistics so we do it:

    basis = []
    for subject, confound in zip(range(n_subjects), confounds):
        logger.debug("Loading subject", subject)
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )
        logger.debug("Done")

        logger = logging.getLogger(__name__)
        logger.debug("Compute basis")
        n_voxels, n_timeframes = x_subject.shape
        n_components, _ = shared_response.shape

        basis_i = _compute_basis(x_subject, shared_response, np.zeros((n_voxels, n_components)),
                                 grad_weight,
                                 0., 0.,
                                 l1_weight, masker.mask_img_.get_data().astype(np.bool),
                                 500, n_jobs=1, verbose=True)
        basis.append(basis_i)

    return masker, basis, shared_response


class InterpretableSRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 transform_method="mean", n_jobs=1,
                 l1_weight=1.,
                 grad_weight=1.,
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            common map smoothness and sparsity controlling parameter.
        l1_ratio: float
            common map relative importance of sparsity / smoothness
            1: only sparsity penalty
            0: only smoothness penalty
        alpha_init: float
            Sparsity controlling parameter in the initial dictionary learning initialization
        mu: float
            Smoothness between maps controlling parameter

        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method,)

        self.l1_weight = l1_weight
        self.grad_weight = grad_weight

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response = cache(
            _graphnetsrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          n_jobs=self.n_jobs,
          l1_weight=self.l1_weight,
          grad_weight=self.grad_weight,
          )
        return self

