import numpy as np
import os
from brainiak.funcalign.fastsrm import FastSRM
from brainiak.funcalign.srm import SRM, DetSRM
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from memory_profiler import memory_usage


def generate_fake_data(n_voxels=100000,
                    n_supervoxels=500,
                    n_timeframes=400,
                    n_sessions=5,
                    n_subjects=20,
                    datadir="/storage/workspace/hrichard/temp/memory"):
    """
    Building fake data and atlas for SRM
    Parameters
    ----------
    n_voxels
    n_supervoxels
    n_timeframes
    n_sessions
    n_subjects
    datadir

    Returns
    -------
    det_atlas
    prob_atlas
    paths
    """
    timeframes = [n_timeframes] * n_sessions
    os.system("rm " + datadir + "/data*.npy")
    X = np.array([[None for _ in range(len(timeframes))]
                  for _ in range(n_subjects)])
    for i in range(n_subjects):
        for j, n_t in enumerate(timeframes):
            X_ij = np.random.rand(n_t, n_voxels)
            np.save(datadir + "/data_%i_%i" % (i, j),
                    X_ij)
            X[i, j] = datadir + "/data_%i_%i.npy" % (
                i,
                j
            )
    prob_atlas = np.random.rand(n_supervoxels, n_voxels)
    det_atlas = np.round(np.random.rand(n_voxels) * n_supervoxels)
    print("Deterministic atlas size: %i" % len(np.unique(det_atlas)))
    return det_atlas, prob_atlas, X


def load_and_concat(paths):
    """
    Take list of path and yields input data for ProbSRM
    Parameters
    ----------
    paths
    Returns
    -------
    X
    """
    X = []
    for i in range(len(paths)):
        X_i = np.concatenate([np.load(paths[i, j])
                              for j in range(len(paths[i]))], axis=0).T
        X.append(X_i)
    return X


def load_and_fit(model, algo_name, paths):
    if "Fast" not in algo_name:
        X = load_and_concat(paths)
    else:
        X = paths

    model.fit(X)
    return model


def run_exp(key, values):
    print(key)
    results = []
    # if not os.path.exists("/home/parietal/hrichard/hugo-richard-M2/"
    #                       "neuroimage/figures/speed_" + key + ".csv"):
    for n_repeat in range(3):
        for k in values:
            params = dict()
            if "components" in key:
                n_components = k
            else:
                n_components = 50
                params[key] = k
            det_atlas, prob_atlas, paths = generate_fake_data(**params)
            for algo_name, model in [
                ("FastSRM_prob_temp", FastSRM(
                    n_components=n_components,
                    atlas=prob_atlas,
                    temp_dir="/storage/workspace/hrichard/temp/memory/",
                    verbose=True,
                )),
                ("FastSRM_prob_temp_lowram", FastSRM(
                    n_components=n_components,
                    atlas=prob_atlas,
                    temp_dir="/storage/workspace/hrichard/temp/memory/",
                    low_ram=True,
                    verbose=True
                )),
                ("FastSRM_det", FastSRM(
                    n_components=n_components,
                    atlas=det_atlas,
                    verbose=True
                )),
                ("FastSRM_prob", FastSRM(
                    n_components=n_components,
                    atlas=prob_atlas,
                    verbose=True
                )),
                ("FastSRM_prob_5jobs", FastSRM(
                    n_components=n_components,
                    atlas=prob_atlas,
                    verbose=True,
                    n_jobs=5
                )),
                ("ProbSRM", SRM(features=n_components)),
                ("DetSRM", DetSRM(features=n_components))
            ]:
                memory = np.max(memory_usage(lambda: load_and_fit(model, algo_name, paths)))
                results.append([algo_name, n_repeat, k, memory])
                print(algo_name, n_repeat, k, memory)

        results = pd.DataFrame(results, columns=["algo", "repeat", key, "memory usage"])
        results.to_csv("/home/parietal/hrichard/hugo-richard-M2/"
                       "neuroimage/figures/memory_" + key + ".csv")

        plt.figure()
        sns.lineplot(x=key, y="memory usage", data=results, hue="algo")

        plt.savefig("/home/parietal/hrichard/hugo-richard-M2/"
                    "neuroimage/figures/memory_" + key + ".pdf")


run_exp("n_voxels", [1000, 10000, 100000])
run_exp("n_subjects", [5, 10, 15, 20, 25, 30])
run_exp("n_timeframes", [100, 500, 1000])
run_exp("n_sessions", [2, 5, 10, 15])
run_exp("n_supervoxels", [50, 100, 500, 1000])
run_exp("n_components", [10, 20, 50, 100, 200])