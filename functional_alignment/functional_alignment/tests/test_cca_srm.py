import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
import scipy as np
import nibabel
from nilearn.input_data import NiftiMasker
import os
from functional_alignment.cca_srm import CCASRM

n_features = 1000
n_components = 10
nb_time_frames = 200
n_samples = 5

# We generate train and test data (not normalized)
latent_variance = np.eye(n_components, n_components)

train_shared = np.random.multivariate_normal(
    np.zeros(n_components),
    latent_variance,
    nb_time_frames).T

train_shared = train_shared - np.mean(train_shared, axis=1, keepdims=True)

test_shared = np.random.multivariate_normal(
    np.zeros(n_components),
    latent_variance,
    nb_time_frames).T

test_shared = test_shared - np.mean(test_shared, axis=1, keepdims=True)

basis = [np.random.multivariate_normal(
    np.zeros(n_features),
    np.eye(n_features),
    n_components).T for _ in range(n_samples)]

train_X = []
for i in range(len(basis)):
    train_X.append(basis[i].dot(train_shared))

test_X = []
for i in range(len(basis)):
    test_X.append(basis[i].dot(test_shared))


affine = np.eye(4)
shape = (10, 10, 10, nb_time_frames)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine)).fit()

masked_train_X = []
for i in range(len(train_X)):
    masked_train_X.append([masker.inverse_transform(train_X[i][:, :int(nb_time_frames / 2)].T),
                           masker.inverse_transform(train_X[i][:, int(nb_time_frames / 2):].T)
                           ])

masked_test_X = []
for i in range(len(train_X)):
    masked_test_X.append([masker.inverse_transform(test_X[i][:, :int(nb_time_frames / 2)].T),
                          masker.inverse_transform(test_X[i][:, int(nb_time_frames / 2):].T)])


def test_shapes_input():
    assert len(train_X) == n_samples
    print(train_X[0].shape)
    assert train_X[0].shape == (n_features, nb_time_frames)


def test_shapes():
    srm = CCASRM(n_components=n_components, mask=masker)
    srm.fit(masked_train_X)
    w = srm.basis
    shared = srm.fit(masked_train_X).transform(masked_train_X)
    for i in range(n_samples):
        assert w[i].shape == (n_features, n_components)
    assert shared.shape == (n_components, nb_time_frames)


def test_reconstruction():
    srm = CCASRM(n_components=n_components, mask=masker)
    shared = srm.fit(masked_train_X).transform(masked_train_X)
    w = srm.basis
    for i in range(n_samples):
        assert_array_almost_equal(train_X[i], w[i].dot(shared), 4)


def test_findshared():
    srm = CCASRM(n_components=n_components, mask=masker)
    srm.inverse_basis = [np.linalg.pinv(b) for b in basis]
    assert_array_almost_equal(srm.transform(masked_train_X), train_shared)
    assert_array_almost_equal(srm.transform(masked_test_X), test_shared)


def test_transform_inverse_transform():
    srm = CCASRM(n_components=n_components, mask=masker)
    srm.basis = basis
    for i in range(len(train_X)):
        train_X_i = srm.inverse_transform(train_shared, index=[i])[0]
        assert_array_almost_equal(train_X_i, train_X[i])
    for i in range(len(test_X)):
        test_X_i = srm.inverse_transform(test_shared, index=[i])[0]
        assert_array_almost_equal(test_X_i, test_X[i])


def test_srm_property_testset():
    srm = CCASRM(n_components=n_components, mask=masker)
    srm.fit(masked_train_X)
    S = srm.transform(masked_test_X[:-1], index=range(len(test_X) - 1))
    Y = srm.inverse_transform(S, index=[len(test_X) - 1])[0]
    assert_array_almost_equal(Y, test_X[-1], 4)


def test_memory_property_testset():
    srm = CCASRM(n_components=n_components, mask=masker, fit_memory="./")
    srm.fit(masked_train_X)
    srm.basis, srm.inverse_basis = None, None
    srm.basis, srm.inverse_basis = srm.load(n_samples)
    S = srm.transform(masked_test_X[:-1], index=range(len(test_X) - 1))
    Y = srm.inverse_transform(S, index=[len(test_X) - 1])[0]
    for i in range(len(srm.basis)):
        assert os.path.isfile("CcaSrm_Basis%s.npy"%(str(i)))
        os.remove("CcaSrm_Basis%s.npy" % (str(i)))

        assert os.path.isfile("CcaSrm_InverseBasis%s.npy"%(str(i)))
        os.remove("CcaSrm_InverseBasis%s.npy"%(str(i)))
    assert_array_almost_equal(Y, test_X[-1], 4)