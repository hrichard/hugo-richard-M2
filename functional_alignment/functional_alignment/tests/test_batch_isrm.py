import numpy as np
import random
import nibabel
from nilearn.input_data import NiftiMasker
from functional_alignment.batch_isrm import BatchISRM


affine = np.eye(4)
shape = (3, 3, 3, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = 27
n_components = 3
nb_time_frames = 5
n_samples = 2


def create_X():
    X = []
    for i in range(n_samples):
        X_ = np.random.rand(n_features, nb_time_frames) * 10
        X.append([masker.inverse_transform(X_.T)])
    return X


def test_convergence():
    X = create_X()

    srm = BatchISRM(n_components,
                    max_iter=10,
                    mask=masker,
                    verbose=1,
                    l1_weight=0.1,
                    grad_weight=0.1,
                    common_weight=0.1)

    srm.fit(X)
    print("errors", srm.errors)

    for i in range(1, len(srm.errors) - 1):
        assert srm.errors[i] > srm.errors[i + 1]


def test_better_init():
    X = create_X()

    srm = BatchISRM(n_components,
                    max_iter=10,
                    mask=masker,
                    verbose=0,
                    l1_weight=0.1,
                    grad_weight=0.1,
                    common_weight=0.1)

    srm2 = BatchISRM(n_components,
                    max_iter=10,
                    mask=masker,
                    verbose=0,
                    l1_weight=0.1,
                    grad_weight=0.1,
                    common_weight=0.1,
                    shared_init="DetSRM")

    srm.fit(X)
    srm2.fit(X)

    print(srm.errors)
    print(srm2.errors)
    assert srm.errors[-1] > srm2.errors[-1]