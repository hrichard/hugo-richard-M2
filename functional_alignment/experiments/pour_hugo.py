import pandas as import pd
from nilearn.input_data import NiftiMasker


def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=2):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = path + 'preloaded_masker/mask_all_subj_movie.nii'

    return image_list, movie_labels, masker_path


def make_train_test_sherlock(image_list, labels, masker, n_max=None):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Split the images of each subject into two folds of even and odd scene numbers
        Parameters :
        - List of nii files subject_imgs
        - Dataframe labels (scene, onset, offset)

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []

    # Make folds out of images indexes to split data between even and odd scene numbers
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1

    # equalize the number of images in train and test sets
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass

    # if wanted limits the number of images in the train set
    if n_max is not None:
        train_session = [train_session[i] for i in range(n_max)]

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list


n_subjects = 17
mask_memory = "/storage/tompouce/tbazeill/sherlock_cache"

# Movie files, labels and masker loading
image_list, movie_labels, masker_path = load_sherlock_data(
    n_subjects=n_subjects)

masker = NiftiMasker(standardize=True, memory=mask_memory)
masker.fit(masker_path)

# Example use of make_train_test_sherlock : for each of 2 subjects split the images into train and test sets of equal dimensions
subject_pair = (1, 2)
subj_1, subj_2 = subject_pair[0], subject_pair[1]
pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
X_train_list, X_test_list = make_train_test_sherlock(
    pair_image_list, movie_labels, masker, n_max=n_max_sherlock)
