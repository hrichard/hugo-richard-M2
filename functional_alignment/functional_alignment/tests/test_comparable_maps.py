import numpy as np
from functional_alignment.utils import get_comparable_maps
from sklearn.utils.testing import assert_array_almost_equal

W0 = []
for i in range(5):
    W0.append(np.random.rand(100))

k_star = np.random.randint(0, 10)
W1 = []
for i in range(5):
    W1.append(np.random.rand(100, 10))


for i in range(5):
    W1[i][:, k_star] = W0[i]


def test_recover_same_map():
    assert k_star == get_comparable_maps(W0, W1)
