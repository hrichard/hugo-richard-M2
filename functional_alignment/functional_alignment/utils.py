import numpy as np
from time import time
import scipy.optimize
from scipy.stats import pearsonr
from sklearn.metrics import r2_score


def score_table(loss, X_gt, X_pred, multioutput='raw_values'):
    if loss is "r2":
        score = r2_score(X_gt, X_pred, multioutput=multioutput)
    elif loss is "zero_mean_r2":
        score = zero_mean_coefficient_determination(
            X_gt, X_pred, multioutput=multioutput)
    elif loss is "reconstruction_err":
        score = reconstruction_error(X_gt, X_pred, multioutput=multioutput)
    elif loss is "corr":
        score = np.array([pearsonr(X_gt[:, vox], X_pred[:, vox])[0]
                          for vox in range(X_pred.shape[1])])
    else:
        raise NameError(
            "Unknown loss, it should be 'r2' or 'zero_mean_r2' or 'corr' or 'reconstruction_err'")
    return np.maximum(score, -1)


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def score_save_table(masker, loss, path_to_results, header, termination, X_gt, X_pred,  multioutput='raw_values'):
    X_score = score_table(loss, X_gt, X_pred, multioutput)
    Im_score = masker.inverse_transform(X_score)
    Im_score.to_filename(path_to_results + "/" +
                         header + "/" + termination)


def zero_mean_coefficient_determination(y_true, y_pred, sample_weight=None, multioutput="uniform_average"):
    if y_true.ndim == 1:
        y_true = y_true.reshape((-1, 1))

    if y_pred.ndim == 1:
        y_pred = y_pred.reshape((-1, 1))

    if sample_weight is not None:
        weight = sample_weight[:, np.newaxis]
    else:
        weight = 1.

    numerator = (weight * (y_true - y_pred) ** 2).sum(axis=0, dtype=np.float64)
    denominator = (weight * (y_true) ** 2).sum(axis=0, dtype=np.float64)
    nonzero_denominator = denominator != 0
    nonzero_numerator = numerator != 0
    valid_score = nonzero_denominator & nonzero_numerator
    output_scores = np.ones([y_true.shape[1]])
    output_scores[valid_score] = 1 - (numerator[valid_score] /
                                      denominator[valid_score])
    output_scores[nonzero_numerator & ~nonzero_denominator] = 0

    if multioutput == 'raw_values':
        # return scores individually
        return output_scores
    elif multioutput == 'uniform_average':
        # passing None as weights results is uniform mean
        avg_weights = None
    elif multioutput == 'variance_weighted':
        avg_weights = (weight * (y_true - np.average(y_true, axis=0,
                                                     weights=sample_weight)) ** 2).sum(axis=0, dtype=np.float64)
        # avoid fail on constant y or one-element arrays
        if not np.any(nonzero_denominator):
            if not np.any(nonzero_numerator):
                return 1.0
            else:
                return 0.0
    else:
        avg_weights = multioutput

    return np.average(output_scores, weights=avg_weights)


def reconstruction_error(y_true, y_pred, sample_weight=None, multioutput="uniform_average"):
    if y_true.ndim == 1:
        y_true = y_true.reshape((-1, 1))

    if y_pred.ndim == 1:
        y_pred = y_pred.reshape((-1, 1))

    if sample_weight is not None:
        weight = sample_weight[:, np.newaxis]
    else:
        weight = 1.

    output_scores = (weight * (y_true - y_pred) **
                     2).sum(axis=0, dtype=np.float64)

    if multioutput == 'raw_values':
        # return scores individually
        return output_scores
    elif multioutput == 'uniform_average':
        # passing None as weights results is uniform mean
        avg_weights = None
    elif multioutput == 'variance_weighted':
        avg_weights = (weight * (y_true - np.average(y_true, axis=0,
                                                     weights=sample_weight)) ** 2).sum(axis=0, dtype=np.float64)
    else:
        avg_weights = multioutput

    return np.average(output_scores, weights=avg_weights)


def generate_train_test(s_list):
    train_test = []
    for i in range(len(s_list)):
        train = []
        test = []
        for j in range(len(s_list)):
            if i == j:
                test.append(i)
            else:
                train.append(j)
        train_test.append((np.array(train), np.array(test)))
    return train_test


def multislice(slices, index):
    l = (np.arange(slices[i].start, slices[i].stop) for i in index)
    l = np.hstack(l)
    return l


def load_img(masker, file_array, axis=0, confound=None):
    t0 = time()
    X = masker.transform(file_array,
                         confound)
    if type(X) == list:
        X = np.concatenate(X, axis=axis)
    X = X.T
    print("Time spent loading data", time() - t0)
    return X, masker


def generate_folds(X, n_folds):
    folds_cut = np.cumsum([int(len(X) / n_folds) for _ in range(n_folds)])
    folds_cut -= int(len(X) / n_folds)

    folds = []
    for i in range(len(folds_cut)):
        if i < len(folds_cut) - 1:
            folds.append(np.arange(folds_cut[i], folds_cut[i + 1]))
        else:
            folds.append(np.arange(folds_cut[i], len(X)))

    folds = np.array(folds)

    generated_folds = []
    for train, test in generate_train_test(folds):
        train_folds = np.concatenate(folds[train]).astype(np.int)
        test_folds = np.concatenate(folds[test]).astype(np.int)
        generated_folds.append([train_folds, test_folds])

    return generated_folds


def create_orthogonal_matrix(rows, cols, random_state=None):
    """
    Creates matrix W with orthogonal columns:
    W.T.dot(W) = I
    Parameters
    ----------
    rows: int
        number of rows
    cols: int
        number of columns
    random_state : int or RandomState
        Pseudo number generator state used for random sampling.
    Returns
    ---------
    Matrix W of shape (rows, cols) such that W.T.dot(W) = T
    """
    v = rows
    k = cols
    if random_state == None:
        rnd_matrix = np.random.rand(v, k)
    else:
        rnd_matrix = random_state.rand(v, k)
    q, r = np.linalg.qr(rnd_matrix)
    return q


def get_comparable_maps(W0, W1):
    n_W0 = []
    for i in range(len(W0)):
        n_W0.append(W0[i])

    C = None
    for i in range(len(W0)):
        n_W0_i = W0[i] / np.sqrt(np.sum(W0[i]**2, axis=0, keepdims=True))
        n_W1_i = W1[i] / np.sqrt(np.sum(W1[i] ** 2, axis=0, keepdims=True))
        if C is None:
            C = - n_W0_i.T.dot(n_W1_i)
        else:
            C -= n_W0_i.T.dot(n_W1_i)

    if len(C.shape) == 1:
        return np.argmin(C)
    else:
        row_ind, col_ind = scipy.optimize.linear_sum_assignment(C)
        return col_ind
