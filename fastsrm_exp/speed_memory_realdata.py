import numpy as np
import os.path
from brainiak.funcalign.fastsrm import FastSRM
from brainiak.funcalign.srm import SRM, DetSRM
import pandas as pd
from time import time
from sklearn.model_selection import KFold
import logging
import sys
from glob import glob
from memory_profiler import memory_usage

log = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
log.addHandler(out_hdlr)
log.setLevel(logging.INFO)


def load_and_concat(paths, transpose=False):
    """
    Take list of path and yields input data for ProbSRM
    Parameters
    ----------
    paths
    Returns
    -------
    X
    """
    X = []
    for i in range(len(paths)):
        if transpose:
            X_i = np.concatenate([np.load(paths[i, j])
                                  for j in range(len(paths[i]))], axis=0).T
        else:
            X_i = np.concatenate([np.load(paths[i, j])
                                  for j in range(len(paths[i]))], axis=0)
        X.append(X_i)
    return X

def load_and_fit(model, algo_name, paths):
    if "Fast" not in algo_name:
        X = load_and_concat(paths)
    else:
        X = paths

    model.fit(X)
    return model

fitinfo = []
result_directory = "/storage/workspace/hrichard/masked_data/results/"
results = []
for config in ["gallant", "forrest", "raiders", "sherlock"]:
    print("Start experiment with config %s" % config)
    if config == "forrest":
        subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12,
                    13, 14, 15, 16, 17, 18, 19, 20]
        runs = [1, 2, 3, 4, 5, 6, 7]
    elif config == "gallant":
        subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
        runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05",
                "Trn06", "Trn07", "Trn08", "Trn09",
                "Val01", "Val02", "Val03", "Val04", "Val05",
                "Val06", "Val07", "Val08"]
    elif config == "sherlock":
        subjects = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        runs = [0, 1, 2, 3, 4]
    elif config == "raiders":
        subjects = np.array([1, 4, 5, 6, 7, 9, 11, 12, 13, 14])
        runs = np.array([1., 2., 3., 4., 5., 6., 7., 8., 10.])

    n_sessions = len(runs)
    n_subjects = len(subjects)

    paths = np.array([["/storage/workspace/hrichard/"
                       "masked_data/%s/subject_%i_session_%i.npy" % (config, i, j)
                       for j in range(len(runs))] for i in range(len(subjects))])

    for n_components in [5, 10, 20, 50, 100]:
        algorithms = [
            ("FastSRM_modl_1024", FastSRM(
                n_components=n_components,
                atlas=np.load("/storage/workspace/hrichard/masked_data/atlases/"
                      "modl_1024.npy"),
                verbose=True,
                random_state=0,
                n_jobs=5
            )),
            ("FastSRM_basc_444", FastSRM(
                n_components=n_components,
                atlas=np.load("/storage/workspace/hrichard/masked_data/atlases/"
                      "basc_444.npy"),
                verbose=True,
                random_state=0,
                n_jobs=5
            )),
            ("FastSRM_shaeffer_800", FastSRM(
                n_components=n_components,
                atlas=np.load("/storage/workspace/hrichard/masked_data/atlases/"
                      "shaeffer_800.npy"),
                verbose=True,
                random_state=0,
                n_jobs=5
            )),
            ("FastSRM_modl_512", FastSRM(
                n_components=n_components,
                atlas=np.load("/storage/workspace/hrichard/masked_data/atlases/"
                      "modl_512.npy"),
                verbose=True,
                random_state=0,
                n_jobs=5
            )),
            ("ProbSRM", SRM(features=n_components)),
            ("DetSRM", DetSRM(features=n_components))
        ]

        log.info("Number of components: %i" % n_components)
        cv = KFold(n_splits=5, shuffle=False, random_state=0)
        sessions_train, sessions_test = list(
            cv.split(np.arange(n_sessions))
        )[0]

        for n_repeat in range(3):
            for _ in range(len(algorithms)):
                name, algorithm = algorithms.pop()
                paths_train = paths[:, sessions_train]
                t0 = time()
                log.info(name)
                log.info("Fitting data")
                t0 = time()
                memory = np.max(memory_usage(
                    lambda: load_and_fit(algorithm, name, paths_train)))
                fit_time = time() - t0
                log.info("Done")

                results.append([config, name, n_repeat, n_components, fit_time, memory])
                print(name, n_repeat, n_components, fit_time, memory)

results = pd.DataFrame(results, columns=["dataset", "algo", "n_repeat", "number of components", "fit time", "memory usage"])
results.to_csv("/home/parietal/hrichard/hugo-richard-M2/"
                       "neuroimage/figures/speed_memory_real_data.csv")

