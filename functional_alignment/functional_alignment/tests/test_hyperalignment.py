import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
from functional_alignment.hyperalignment import hyperalign, zscore, Hyperalignment

def test_hyperalign_primal_dual():
    n, p = 100, 20
    X = np.random.randn(n, p)
    Y = np.random.randn(n, p)
    R1, s1 = hyperalign(X, Y, scale=True, primal=True)
    R2, s2 = hyperalign(X, Y, scale=True, primal=False)
    assert_array_almost_equal(R1, R2)
    n, p = 20, 100
    X = np.random.randn(n, p)
    Y = np.random.randn(n, p)
    R1, s1 = hyperalign(X, Y, scale=True, primal=True)
    R2, s2 = hyperalign(X, Y, scale=True, primal=False)
    assert_array_almost_equal(R1, R2)


def test_hyperalign_3Drotation():
    R = np.array([[1., 0., 0.], [0., np.cos(1), -np.sin(1)], [0., np.sin(1), np.cos(1)]])
    X = np.random.rand(3, 4)
    X = X - X.mean(axis=1, keepdims=True)
    Y = R.dot(X)

    R_test, _ = hyperalign(X, Y)
    assert_array_almost_equal(
        R.dot(np.array([0.,1.,0.])),
        np.array([0., np.cos(1), np.sin(1)])
    )
    assert_array_almost_equal(
        R.dot(np.array([0., 0., 1.])),
              np.array([0., -np.sin(1), np.cos(1)])
    )
    assert_array_almost_equal(R, R_test)


def test_hyperalign_orthogonalmatrix():
    v = 10
    k = 10
    rnd_matrix = np.random.rand(v, k)
    R, _ = np.linalg.qr(rnd_matrix)
    X = np.random.rand(10, 20)
    X = X - X.mean(axis=1, keepdims=True)
    Y = R.dot(X)
    R_test, _ = hyperalign(X, Y)
    assert_array_almost_equal(R_test, R)


def test_zscore():
    X = np.array([[1.,2.,3.,4.],
                  [5.,3.,4.,6.],
                  [7.,8.,-5.,-2.]])
    X2 = zscore(X)
    assert (X.shape == X2.shape)

    for i in range(len(X)):
        assert_array_almost_equal(X2[i].mean(), 0)
        assert_array_almost_equal(X2[i].std(), 1)


def test_hyperalign_multiplication():
    X = np.array([[1., 2., 3., 4.],
                  [5., 3., 4., 6.],
                  [7., 8., -5., -2.]])

    X = X - X.mean(axis=1, keepdims=True)

    Y = 2*X
    Y = Y - Y.mean(axis=1, keepdims=True)

    assert_array_almost_equal(hyperalign(X, Y, scale=True)[0], np.eye(3))
    assert_array_almost_equal(hyperalign(X, Y, scale=True)[1], 2)


def test_hyperalign_basis_orthogonal():
    X = np.random.rand(3, 4)
    X = X - X.mean(axis=1, keepdims=True)

    Y = np.random.rand(3, 4)
    Y = Y - Y.mean(axis=1, keepdims=True)

    R, _ = hyperalign(X, Y)
    assert_array_almost_equal(R.dot(R.T), np.eye(R.shape[0]))
    assert_array_almost_equal(R.T.dot(R), np.eye(R.shape[0]))


def test_hyperalignment_transform_inverse_transform():
    X =  []
    Y = []

    for i in range(10):
        X_ = np.random.rand(3, 4)
        X.append(X_)
        Y_ = np.random.rand(3, 4)
        Y.append(Y_)

    hyper = Hyperalignment(scale=True)
    hyper.fit(X)
    res = hyper.transform(Y)
    res_inv = hyper.inverse_transform(res)

    for i in range(len(res)):
        assert_array_almost_equal(Y[i], res_inv[i])
