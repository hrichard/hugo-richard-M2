import numpy as np
from sklearn.externals.joblib import Memory
from functional_alignment.cca_srm import cca_srm
from sklearn.decomposition.dict_learning import MiniBatchDictionaryLearning
from sklearn.decomposition.dict_learning import sparse_encode
import logging
from functional_alignment.deterministic_srm import fast_srm
from functional_alignment.baseSRM import BaseSRM, mask_and_reduce
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img

logger = logging.getLogger(__name__)


def _dictsrm_fit(masker,
                 imgs,
                 confounds=None,
                 n_components=10,
                 random_state=None,
                 memory=None,
                 memory_level=0,
                 n_jobs=1,
                 init_method="ortho_srm",
                 alpha=1.):

    masker.fit()
    shared_response = _reduced_data_sharedresponse(masker,
                                                   imgs,
                                                   confounds=confounds,
                                                   n_components=n_components,
                                                   random_state=random_state,
                                                   memory=memory,
                                                   memory_level=memory_level,
                                                   n_jobs=n_jobs,
                                                   init_method=init_method,
                                                   alpha=alpha
                                                   )

    basis = _find_basis(masker,
                        imgs,
                        shared_response,
                        confounds=confounds,
                        alpha=alpha,
                        n_jobs=n_jobs,
                        memory=memory,
                        memory_level=memory_level
                        )

    return masker, basis, shared_response


def _reduced_data_sharedresponse(masker,
                          imgs,
                          alpha=1.,
                          confounds=None,
                          n_components=10,
                          random_state=None,
                          memory=None,
                          memory_level=0,
                          n_jobs=1,
                          init_method="ortho_srm"
                          ):
    """
    Uses PCA to reduce data from n_voxels, n_timeframes to n_components, n_timeframes
    In reduced space, uses cca or srm to find an initial guess for shared space
    this guess is refined using dictionary learning.

    Returns
    -------
    dictionary: np array of shape n_components, n_timeframes
        the dictionary which is also the shared space.
    """
    svd_data = mask_and_reduce(
        masker,
        imgs,
        confounds=confounds,
        n_components=n_components,
        random_state=random_state,
        memory=memory,
        memory_level=memory_level,
        n_jobs=n_jobs
    )

    reduced_data = []
    for i in range(len(svd_data)):
        U, S, V = svd_data[i]
        reduced_data.append(S[:, np.newaxis] * V)
        svd_data[i] = None

    if init_method == "cca":
        _, _, shared_coord = cca_srm(
            reduced_data
        )
    elif init_method == "ortho_srm":
        _, _, shared_coord = fast_srm(reduced_data, random_state=None, max_iter=10, tol=1e-6, use_scaling=False)
    else:
        shared_coord = None
        ValueError("unknown init_method attribute of dictionary srm: %s" % init_method)

    n_subjects = len(imgs)
    dict = MiniBatchDictionaryLearning(n_components=n_components,
                                       alpha=alpha,
                                       n_iter=n_subjects,
                                       fit_algorithm="cd",
                                       n_jobs=n_jobs,
                                       dict_init=shared_coord
                                       )

    for subject in range(n_subjects):
        x_subject = reduced_data[subject]
        dict.partial_fit(x_subject)

    dictionary = dict.components_
    return dictionary


def _find_basis(masker, imgs, dictionary, confounds=None,
                alpha=1., n_jobs=1., memory=None, memory_level=None):
    """
    Finds a sparse coding of all subject basis to match the one in dictionary
    """
    n_subjects = len(imgs)
    codes = []
    for subject, confound in zip(range(n_subjects), confounds):
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )
        code = sparse_encode(x_subject, dictionary,
                             algorithm="lasso_cd",
                             alpha=alpha,
                             n_jobs=n_jobs)
        codes.append(code)
    return codes


class DictionarySRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2 
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components, alpha=1.,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 transform_method="mean", n_jobs=1,
                 init_method="ortho_srm"
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            Sparsity controlling parameter.
        init_method: str
            "cca" uses cca as initialization
            "ortho_srm" uses fast deterministic srm as initialization
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method)

        self.alpha = alpha
        self.init_method = init_method

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response = cache(
            _dictsrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          n_jobs=self.n_jobs,
          init_method=self.init_method,
          alpha=self.alpha
          )
        return self

