from glob import glob
import numpy as np
import joblib
import os
from sklearn.linear_model import RidgeCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import ShuffleSplit
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.svm import LinearSVR
from sklearn.model_selection import GridSearchCV


class DummyRegressor:
    def __init__(self):
        self.p = 0

    def fit(self, X, Y):
        self.p = np.mean(Y)
        return self

    def predict(self, X):
        return np.array([self.p for _ in range(len(X))])


def create_distance_matrix(basis_paths, res_path):
    if os.path.exists(res_path):
        return joblib.load(res_path)
    else:
        n_components = np.load(basis_paths[0]).shape[0]
        distance_matrix = np.zeros((n_components, len(basis_paths), len(basis_paths)))

        for i, path1 in enumerate(basis_paths):
            print(i, len(basis_paths), path1)
            for j, path2 in enumerate(basis_paths):
                basis_1 = np.load(path1)
                basis_2 = np.load(path2)

                for k in range(n_components):
                    distance_matrix[k, i, j] = basis_1[k].dot(basis_2[k])

        joblib.dump(distance_matrix, res_path)
    return distance_matrix


def predict_age(basis_name, random_state=None):
    paths = glob("/storage/tompouce/hrichard/retreat_2019/" + basis_name + "/basis*.npy")
    paths = ["/storage/tompouce/hrichard/retreat_2019/" + basis_name + "/basis_" + str(i) + ".npy"
             for i in range(len(paths))]
    fig_path = "/storage/tompouce/hrichard/retreat_2019/figures/" + basis_name + "_basis.pdf"
    Y = np.array(joblib.load("/storage/tompouce/hrichard/retreat_2019/liste_ages.gz"))

    shuffle = ShuffleSplit(n_splits=5, train_size=0.8, test_size=0.2, random_state=random_state)
    result = []

    for train, test in shuffle.split(paths):
        for name_algo in [
            ("ridge", RidgeCV(alphas=tuple(10 ** k for k in range(-5, 5)))),
            ("dummy", DummyRegressor()),
        ]:
            name, algo = name_algo
            print(name)
            X_preds_train = []
            X_preds_test = []
            n_components, n_voxels = np.load(paths[0]).shape
            for k in range(n_components):
                X_train = np.array([np.load(paths[i])[k] for i in train])
                X_test = np.array([np.load(paths[i])[k] for i in test])
                print(X_train.shape)
                algo.fit(X_train, Y[train])
                X_preds_train.append(algo.predict(X_train))
                Y_pred = algo.predict(X_test)
                X_preds_test.append(Y_pred)
                mae = np.mean(np.abs(Y[test] - Y_pred))
                result.append([mae, train.__str__(), name, k])

            X_preds_train = np.array(X_preds_train).T
            X_preds_test = np.array(X_preds_test).T
            rf = RandomForestRegressor(n_estimators=100)
            print("X_pred_train", X_preds_train.shape)
            print("Y_tes", Y[test].shape)

            rf.fit(X_preds_train, Y[train])
            Y_pred = rf.predict(X_preds_test)
            mae = np.mean(np.abs(Y[test] - Y_pred))
            for k in range(n_components):
                result.append([mae, train.__str__(), "RF_" + name, k])

    result = pd.DataFrame(result, columns=["mae", "split", "algo", "component"])
    plt.figure()
    sns.lineplot(x="component", y="mae", hue="algo", data=result)
    plt.savefig(fig_path)
    plt.close()


def predict_age_boost(basis_name, random_state=None):
    paths = glob("/storage/tompouce/hrichard/retreat_2019/" + basis_name + "/basis*.npy")
    paths = ["/storage/tompouce/hrichard/retreat_2019/" + basis_name + "/basis_" + str(i) + ".npy"
             for i in range(len(paths))]
    fig_path = "/storage/tompouce/hrichard/retreat_2019/figures/" + basis_name + "_basis.pdf"
    Y = np.array(joblib.load("/storage/tompouce/hrichard/retreat_2019/liste_ages.gz"))

    shuffle = ShuffleSplit(n_splits=5, train_size=0.8, test_size=0.2, random_state=random_state)
    shuffle2 = ShuffleSplit(n_splits=5, train_size=0.8, test_size=0.2, random_state=random_state)

    result = []
    for train, test in shuffle.split(paths):
        for name_algo in [
            ("ridge", RidgeCV(alphas=tuple(10 ** k for k in range(-5, 5)))),
            ("dummy", DummyRegressor()),
        ]:
            name, algo = name_algo

            forrest_X = []
            forrest_Y = []

            n_components, n_voxels = np.load(paths[0]).shape
            for i_train_train, i_test_train in shuffle2.split(train):
                train_train = train[i_train_train]
                test_train = train[i_test_train]

                f_x = []
                for k in range(n_components):
                    X_train_train = np.array([np.load(paths[i])[k] for i in train_train])
                    X_test_train = np.array([np.load(paths[i])[k] for i in test_train])
                    algo.fit(X_train_train, Y[train_train])
                    f_x.append(algo.predict(X_test_train))
                f_x = np.array(f_x).T

                forrest_X.append(f_x)
                forrest_Y.append(Y[test_train])

            xgb = RandomForestRegressor(n_estimators=100)
            forrest_X = np.concatenate(forrest_X, axis=0)
            forrest_Y = np.concatenate(forrest_Y, axis=0)
            xgb.fit(forrest_X, forrest_Y)

            f_x = []
            for k in range(n_components):
                X_train = np.array([np.load(paths[i])[k] for i in train])
                X_test = np.array([np.load(paths[i])[k] for i in test])
                algo.fit(X_train, Y[train])
                f_x.append(algo.predict(X_test))
            f_x = np.array(f_x).T
            Y_pred = xgb.predict(f_x)
            mae = np.mean(np.abs(Y[test] - Y_pred))
            result.append([mae, train.__str__(), "Boost" + name])

    result = pd.DataFrame(result, columns=["mae", "split", "algo"])
    plt.figure()
    sns.boxplot(x="algo", y="mae", hue="algo", data=result)
    plt.savefig(fig_path)
    plt.close()


for b in ["small_basis", "big_basis", "big_basis_w_hv_confounds",
          "smooth_big_basis", "smooth_big_basis_w_hv_confounds"]:
    predict_age_boost(b, random_state=0)








