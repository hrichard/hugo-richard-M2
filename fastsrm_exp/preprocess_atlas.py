import numpy as np
from nilearn.input_data import MultiNiftiMasker
from nilearn.image import new_img_like
import nibabel


def load_atlas(atlas):
    A = None
    if atlas is not None:
        atlas_masker = MultiNiftiMasker(
            mask_img='/home/parietal/hrichard/'
                     'cogspaces_data/mask/hcp_mask.nii.gz').fit()
        X = nibabel.load(atlas).get_data()
        if len(X.shape) == 3:
            # We do not want to resample the data
            # again so we resample the atlas instead (faster)
            n_components = len(np.unique(X)) - 1
            xa, ya, za = X.shape
            A = np.zeros((xa, ya, za, n_components + 1))
            for c in np.unique(X)[1:].astype(int):
                X_ = np.copy(X)
                X_[X_ != c] = 0.
                X_[X_ == c] = 1.
                A[:, :, :, c] = X_
            A = atlas_masker.transform(new_img_like(atlas, A))
            A = np.argmax(A, axis=0)
        else:
            A = atlas_masker.transform(atlas)
    return A


for atlas_name, atlas_path in [
    ("modl_1024", "/storage/store/data/cogspaces/modl/components_1024.nii.gz"),
    ("basc_444", '/storage/store/data/nilearn_data/basc_multiscale_2015/'
                 'template_cambridge_basc_multiscale_nii_sym/'
                 'template_cambridge'
                 '_basc_multiscale_sym_scale444.nii.gz'),
    ("shaeffer_800", "/storage/workspace/hrichard/M2_internship/"
                     "parcellations/Parcellations/MNI/"
                     "Schaefer2018_800Parcels_17"
                     "Networks_order_FSLMNI152_2mm.nii.gz"),
    ("modl_512", "/storage/store/data/cogspaces/modl/components_512.nii.gz")
]:
    print(atlas_path)
    atlas = load_atlas(atlas_path)
    print(atlas.shape)
    np.save("/storage/workspace/hrichard/masked_data/atlases/%s" % atlas_name,
            atlas)

