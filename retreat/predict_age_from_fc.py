import joblib
import matplotlib.pyplot as plt
from glob import glob
import numpy as np
from sklearn.linear_model import RidgeCV
from sklearn.model_selection import RepeatedKFold, ShuffleSplit
import pandas as pd
import seaborn as sns
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC, SVR
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import SGDRegressor


fc = joblib.load("/storage/tompouce/hrichard/retreat_2019/fonctional_connectivity")
X = []
for cm in fc:
    X.append(cm.flatten())
X = np.array(X)


class DummyRegressor:
    def __init__(self):
        self.p = 0
        self.alpha_ = -10

    def fit(self, X, Y):
        self.p = np.mean(Y)
        return self

    def predict(self, X):
        return self.p


results = []
Y = np.array(joblib.load("/storage/tompouce/hrichard/retreat_2019/liste_ages.gz"))
for name_algo in [
    ("ridge", RidgeCV(alphas=tuple(10**-k for k in range(-5, 5)))),
    ("dummy", DummyRegressor()),
]:
    shuffle = ShuffleSplit(n_splits=5, train_size=0.8, test_size=0.2, random_state=0)
    name, algo = name_algo
    for train, test in shuffle.split(X):
        algo.fit(X[train], Y[train])
        Y_test_pred = algo.predict(X[test])
        Y_train_pred = algo.predict(X[train])
        Y_test = Y[test]
        results.append([train, np.mean(np.abs(Y_test_pred - Y_test)), name])
plt.figure()
results = pd.DataFrame(results, columns=["split", "mae", "algo"])
sns.boxplot(x="algo", y="mae", data=results, hue="algo")
plt.savefig("/storage/tompouce/hrichard/retreat_2019/figures/MAE_fc_pred.pdf")
