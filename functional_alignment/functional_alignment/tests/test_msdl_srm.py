import numpy as np
import random
import nibabel
from sklearn.utils.testing import assert_array_almost_equal
from nilearn.input_data import NiftiMasker
import scipy.stats as stats
from functional_alignment.msdl_srm import MSDLSRM
import scipy.sparse as sp
from functional_alignment.msdl_srm import _compute_basis, _compute_common_basis, _update_dictionary, _srm_mdsl_step
from nilearn.decoding.objective_functions import _gradient, _unmask

affine = np.eye(4)
shape = (2, 2, 2, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = 8
n_components = 3
nb_time_frames = 10
n_samples = 2


def generate_group_sparse_matrix(n_voxels, n_components, n_splits=5, sparsity=0.5):
    """
    Generate a group sparse matrix
    Parameters
    ----------
    n_voxels
    n_components
    sparsity: float
        average proportion of zeros
    Returns
    -------
    X: ndarray of shape (n_voxels, n_components)
        sparse matrix
    """
    K = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(sparsity).rvs).toarray()
    M = np.zeros((n_voxels, n_components))

    split_sizes = np.zeros((n_splits, n_components))
    for k in range(n_components):
        for i in range(n_splits):
            split_sizes[i, k] = np.random.randint(5, 10)

    split_sizes = split_sizes / np.sum(split_sizes, axis=0, keepdims=True) * n_voxels
    split_sizes = np.cumsum(split_sizes, axis=0)
    split_sizes = np.round(split_sizes).astype(int)

    for k in range(n_components):
        for i in range(n_splits):
            if i ==0:
                start = 0
            else:
                start = split_sizes[i-1, k]
            stop = split_sizes[i, k]
            vv = stop - start
            variance = np.eye(vv)*0.1
            variance = variance.T.dot(variance)
            M[start:stop, k] = np.random.multivariate_normal(
                np.zeros(vv),
                variance
            ) * K[i, k]

    return M


def create_X():
    shared = np.random.rand(n_components, nb_time_frames) / 10
    X = []
    common_basis = generate_group_sparse_matrix(n_features, n_components, 4, 0.5)
    subjects_basis = []
    for i in range(n_samples):
        test_basis = common_basis + np.random.multivariate_normal(
            np.zeros(n_features),
            np.eye(n_features),
            n_components
        ).T

        X_ = test_basis.dot(shared)
        X.append([masker.inverse_transform(X_.T)])
        subjects_basis.append(test_basis)
    return X, common_basis, subjects_basis, shared


def cost_function(X, basis_subject, common_basis, shared_response, masker, alpha=1, mu=1, l1_ratio=0.5):
    mask = np.array(masker.mask_img.get_data(), dtype=bool)
    grad_cost = 0
    for k in range(common_basis.shape[1]):
        grad_buffer = _unmask(common_basis[:, k], mask)
        grad_mask = np.tile(mask, [mask.ndim] + [1] * mask.ndim)
        grad_section = _gradient(grad_buffer)[grad_mask]
        grad_cost += grad_section.dot(grad_section)

    cost = 0.5 * alpha * (1 - l1_ratio) * grad_cost
    for i in range(len(X)):
        X_i = masker.transform(X[i][0]).T
        cost += 0.5 * np.linalg.norm(X_i - basis_subject[i].dot(shared_response)) ** 2
        cost += 0.5 * mu * np.linalg.norm(common_basis - basis_subject[i]) ** 2
        cost += alpha * l1_ratio * np.sum(np.abs(common_basis))
    return cost


def test_cost_function():
    X, common_basis, subjects_basis, shared = create_X()
    c_real = cost_function(X, subjects_basis, common_basis, shared, masker, 1, 1, 0.5)

    for _ in range(100):
        common_basis_ = np.random.rand(n_features, n_components)
        subjects_basis_ = [np.random.rand(n_features, n_components) for _ in range(n_samples)]
        shared_ = np.random.rand(n_components, nb_time_frames)
        c = cost_function(X, subjects_basis_, common_basis_, shared_, masker, 1, 1, 0.5)
        assert c > c_real


def test_shapes_input():
    X, common_basis, subjects_basis, shared = create_X()
    assert len(X) == n_samples
    assert len(subjects_basis) == n_samples
    for i in range(len(X)):
        assert X[i][0].shape == shape
        assert subjects_basis[i].shape == (n_features, n_components)
    assert common_basis.shape == (n_features, n_components)
    assert shared.shape == (n_components, nb_time_frames)


def test_shapes():
    X, _, _, _ = create_X()
    srm = MSDLSRM(n_components,
                  max_iter=10,
                  mask=masker)
    srm.fit(X)
    basis = srm.basis
    shared = srm.fit_transform(X)
    for i in range(n_samples):
        assert basis[i].shape == (n_features, n_components)
    assert shared.shape == (n_components, nb_time_frames)

    shared2 = srm.transform(X)
    assert shared2.shape == (n_components, nb_time_frames)


def test_compute_basis():
    X, common_basis, subjects_basis, shared = create_X()

    X_ = []
    for i in range(len(X)):
        X_.append(masker.transform(X[i][0]).T)
    basis = _compute_basis(X_, common_basis, shared, mu=0.)
    for i in range(len(basis)):
        assert_array_almost_equal(basis[i], subjects_basis[i])


def test_compute_basis_mu():
    X, common_basis, subjects_basis, shared = create_X()
    c_real = cost_function(X, subjects_basis, common_basis, shared, masker, alpha=1, mu=1, l1_ratio=0.5)
    X_ = []
    for i in range(len(X)):
        X_.append(masker.transform(X[i][0]).T)
    basis = _compute_basis(X_, common_basis, shared, mu=1.)

    c = cost_function(X, basis, common_basis, shared, masker, alpha=1, mu=1, l1_ratio=0.5)
    assert c <= c_real


def test_compute_common_basis():
    X, common_basis, subjects_basis, shared = create_X()
    c_real = cost_function(X, subjects_basis, common_basis, shared, masker, alpha=1, mu=1, l1_ratio=0.5)

    common_basis_ = _compute_common_basis(subjects_basis,
                          alpha=1,
                          mask_img=masker.mask_img.get_data().astype(bool),
                          mu=1,
                          l1_ratio=0.5
                          )

    c = cost_function(X, subjects_basis, common_basis_, shared, masker, alpha=1, mu=1, l1_ratio=0.5)
    assert c <= c_real


def test_update_dictionary():
    X, common_basis, subjects_basis, shared = create_X()
    c_real = cost_function(X, subjects_basis, common_basis, shared, masker, alpha=1, mu=1, l1_ratio=0.5)

    X_ = []
    for i in range(len(X)):
        X_.append(masker.transform(X[i][0]).T)

    # Compute W_TW amd W_TX
    W_TW = np.zeros((n_components, n_components))
    W_TX = np.zeros((n_components, nb_time_frames))
    for i in range(len(subjects_basis)):
        W_TW += subjects_basis[i].T.dot(subjects_basis[i])
        W_TX += subjects_basis[i].T.dot(X_[i])
    shared_response_ = _update_dictionary(W_TW, W_TX)

    c = cost_function(X, subjects_basis, common_basis, shared_response_, masker, alpha=1, mu=1, l1_ratio=0.5)
    assert c <= c_real + 1e-10


def test_mdsl_step():
    X, common_basis, subjects_basis, shared = create_X()

    common_basis_ = np.random.rand(n_features, n_components)
    subjects_basis_ = [np.random.rand(n_features, n_components) for _ in range(n_samples)]
    shared_ = np.random.rand(n_components, nb_time_frames)

    c_real = cost_function(X, subjects_basis, common_basis, shared, masker, alpha=1, mu=1, l1_ratio=0.5)
    c_prec = c_real
    print(c_real)
    shared_response_ = shared_
    W_TW = np.zeros((n_components, n_components))
    W_TX = None
    for iter in range(2):
        for subject in range(n_samples):
            x_subject = masker.transform(X[subject][0]).T

            subjects_basis_, shared_response_, common_basis_, W_TX, W_TW = _srm_mdsl_step(x_subject,
                                                                                          masker,
                                                                                          shared_response_,
                                                                                          basis=subjects_basis_,
                                                                                          common_basis=common_basis_,
                                                                                          W_TW=W_TW,
                                                                                          W_TX=W_TX,
                                                                                          alpha=1,
                                                                                          mu=1,
                                                                                          l1_ratio=0.5,
                                                                                          subject=subject
                                                                                          )
        c = cost_function(X, subjects_basis_, common_basis_, shared_response_, masker, alpha=1, mu=1, l1_ratio=0.5)
        print(c)
        assert c < c_prec
        c_prec = c 