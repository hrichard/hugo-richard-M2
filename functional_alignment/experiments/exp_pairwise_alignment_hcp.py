
import time
from nilearn.input_data import NiftiMasker

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from functional_alignment.utils import score_save_table, make_train_test_images
import os

clustering_method = "k_means"
methods = ["optimal_transport", "RidgeCV", "scaled_orthogonal", "permutation"]
loss = "zero_mean_r2"
path_to_data = "/storage/store/data/HCP900/glm"
path_to_results = "/storage/tompouce/tbazeill/hcp"
reg = 1
n_jobs = 1
n_bootstraps = [20]
n_pieces = 150


mask_path = os.path.join(path_to_results, '3mm_mask_gm.nii.gz')
mask_memory = "/storage/tompouce/tbazeill/hcp_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory, mask_img=mask_path)
masker.fit()

subjects = [100206, 109123, 118528, 129634, 138534, 149741,
            158338, 168745, 178748, 191942, 201414, 211922, 257845, 322224]
contrasts = ["EMOTION/LR/z_maps/z_FACES.nii.gz", "EMOTION/LR/z_maps/z_SHAPES.nii.gz", "GAMBLING/LR/z_maps/z_PUNISH.nii.gz", "GAMBLING/LR/z_maps/z_REWARD.nii.gz", "LANGUAGE/LR/z_maps/z_MATH.nii.gz", "LANGUAGE/LR/z_maps/z_STORY.nii.gz", "MOTOR/LR/z_maps/z_AVG.nii.gz", "MOTOR/LR/z_maps/z_LH.nii.gz",  "MOTOR/LR/z_maps/z_RF.nii.gz", "MOTOR/LR/z_maps/z_LF.nii.gz", "MOTOR/LR/z_maps/z_T.nii.gz", "MOTOR/LR/z_maps/z_CUE.nii.gz",     "MOTOR/LR/z_maps/z_RH.nii.gz", "RELATIONAL/LR/z_maps/z_MATCH.nii.gz", "RELATIONAL/LR/z_maps/z_REL.nii.gz",
             "WM/LR/z_maps/z_0BK_PLACE.nii.gz",  "WM/LR/z_maps/z_2BK_FACE.nii.gz", "WM/LR/z_maps/z_0BK_BODY.nii.gz",  "WM/LR/z_maps/z_0BK_TOOL.nii.gz",   "WM/LR/z_maps/z_2BK.nii.gz", "WM/LR/z_maps/z_BODY.nii.gz", "WM/LR/z_maps/z_TOOL.nii.gz", "WM/LR/z_maps/z_0BK_FACE.nii.gz", "WM/LR/z_maps/z_2BK_PLACE.nii.gz", "WM/LR/z_maps/z_0BK.nii.gz",   "WM/LR/z_maps/z_2BK_BODY.nii.gz", "WM/LR/z_maps/z_2BK_TOOL.nii.gz", "WM/LR/z_maps/z_FACE.nii.gz",  "WM/LR/z_maps/z_PLACE.nii.gz", "SOCIAL/LR/z_maps/z_RANDOM.nii.gz", "SOCIAL/LR/z_maps/z_TOM.nii.gz"]
paths_train, paths_test = [], []
for subject in subjects:
    subject_paths_train = []
    subject_paths_test = []
    for contrast in contrasts:
        path_t = os.path.join(path_to_data, str(subject) + '/' + contrast)
        subject_paths_train.append(path_t)
        subject_paths_test.append(path_t.replace("LR", "RL"))
    paths_train.append(subject_paths_train)
    paths_test.append(subject_paths_test)

'''(3, 1), (9, 1), (6, 3), (7, 9), (10, 1),(5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)'''
subject_pair_list = [(12, 8), (2, 5), (2, 8), (3, 11), (4, 6), (7, 10)]

for n_bootstrap in n_bootstraps:
    for subject_pair in subject_pair_list:

        start_time = time.time()
        subj_1, subj_2 = subject_pair[0], subject_pair[1]

        X_1_train = masker.transform(paths_train[subj_1])
        X_2_train = masker.transform(paths_train[subj_2])
        X_1_test = masker.transform(paths_test[subj_1])
        X_2_test = masker.transform(paths_test[subj_2])

        # maybe necessary to do for loop then np.vstack(X_train)
        start_time = time.time()
        results_title = str(subj_1) + "_" + \
            str(subj_2) + "_" + loss + ".nii.gz"
        print("Aligning pair : %s" % str(subject_pair))
        Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
            masker, X_1_train, X_2_train, X_1_test)
        for method in methods:
            if method == "optimal_transport":
                piecewise_estim = PieceWiseAlignmentOT(
                    n_pieces=n_pieces, method="epsilon_scaling", metric="euclidean", reg=reg, mask=masker, n_jobs=n_jobs, clustering_method=clustering_method, n_bootstrap=n_bootstrap)
            else:
                piecewise_estim = PieceWiseAlignment(
                    n_pieces=n_pieces, method=method, mask=masker, clustering_method=clustering_method, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
            results_title = "%s_%s_%s_%s_%s.nii.gz" % (str(subj_1),
                                                       str(subj_2), str(reg), clustering_method, loss)
            piecewise_estim.fit(Im_1_train, Im_2_train)
            Im_1_pred = piecewise_estim.transform(Im_1_test)
            X_1_pred = masker.transform(Im_1_pred)
            method_header = "%s_bootstraps_piecewise_%s" % (
                str(n_bootstrap), method)
            score_save_table(
                masker, loss, path_to_results, method_header, results_title, X_2_test, X_1_pred)
