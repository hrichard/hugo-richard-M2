# Share response model
Based on the following paper Chen, P. H. C., Chen, J., Yeshurun,
 Y., Hasson, U., Haxby, J., & Ramadge, P. J. (2015). 
 A reduced-dimension fMRI shared response model. 
 In Advances in Neural Information Processing Systems 
 (pp. 460-468).

This work was started with the Parietal team 
(https://team.inria.fr/parietal/). 
It will be continued at Pillow lab 
(http://pillowlab.princeton.edu/).

I will study the performance of shared response model 
in various setting in order to understand its 
behaviour and its potential for improvement.

## Step 1: Generate fake data and run deterministic SRM on them
We generate fake data (5 subjects, 300 voxels per subject, 
50 timepoints) and let us assume that all this data 
can be recovered from a shared response which is composed 
of 10 timecourses.

Formally:
- X_i is a matrix of shape (200, 50)
- S is a matrix of shape (20, 50)

For all subject i it is true that:
 
 X_i = W_i S

We first generate S and W_i and it gives us X_i.

We can assume different constrains for W_i:
- sparsity constrains
- group sparsity constrains
- orthogonality constrains (SRM assumption)
- no constrains

<img src="https://github.com/hugorichard/shared_response_model/blob/master/figures/srm_perfs.png">
See results (in figures/srm_perfs.png)
</img>
