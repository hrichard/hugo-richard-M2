import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
import nibabel
from scipy.sparse import lil_matrix

from mvpa2.suite import SearchlightHyperalignment as Slh
from mvpa2.suite import AttrDataset, ArrayCollectable
import functional_alignment
from functional_alignment.searchlight_hyperalignment import get_data, SearchlightHyperalignment, searchlight_hyperalign

def test_slh_get_data():

    img = np.random.rand(10, 10, 1, 1)
    img = nibabel.Nifti1Image(img, np.eye(4))
    mask_img = nibabel.Nifti1Image(np.ones((10, 10, 1)), np.eye(4))

    #             25,
    #     33, 34, 35, 36, 37,
    #     43, 44, 45, 46, 47,
    # 52, 53, 54, 55, 56, 57, 58,
    #     63, 64, 65, 66, 67,
    #     73, 74, 75, 76, 77,
    #             85

    X, A = get_data(mask_img, mask_img, img, 3)

    assert A.rows[55] == [25,
                          33, 34, 35, 36, 37,
                          43, 44, 45, 46, 47,
                          52, 53, 54, 55, 56, 57, 58,
                          63, 64, 65, 66, 67,
                          73, 74, 75, 76, 77,
                          85]

    assert X.shape == (100, 1)

def test_searchlight_identical_pymvpa():

    # We just build some identical data using both PyMVPA and Searchlight class
    X_train = []
    ds_train = []

    X_test = []
    ds_test = []

    vxs = 4
    vys = 2
    vzs = 2

    n_subject = 4
    n_features = vxs * vys * vzs
    n_timeframes = 100
    radius = 3

    sample = np.ones((vxs, vys, vzs, n_timeframes))
    mask_img = nibabel.Nifti1Image(np.ones((vxs, vys, vzs)), np.eye(4))
    img = nibabel.Nifti1Image(sample, np.eye(4))

    _, A = get_data(mask_img, mask_img, img, radius)


    voxel_indices = []
    for vx in range(vxs):
        for vy in range(vys):
            for vz in range(vzs):
                voxel_indices.append([vx, vy, vz])

    voxel_indices = np.array(voxel_indices)

    for i in range(n_subject):
        sample = np.random.rand(n_features, n_timeframes)
        sample = sample - sample.mean(axis=1, keepdims=True)
        sample = sample / sample.std(axis=1, keepdims=True)
        ds_ = AttrDataset(sample.T)
        ds_.fa["voxel_indices"] = ArrayCollectable(value=voxel_indices)
        ds_train.append(ds_)
        X_train.append(sample)

    for i in range(n_subject):
        sample = np.random.rand(n_features, n_timeframes)
        sample = sample - sample.mean(axis=1, keepdims=True)
        sample = sample / sample.std(axis=1, keepdims=True)
        ds_ = AttrDataset(sample.T)
        ds_test.append(ds_)
        X_test.append(sample)

    # We now check that they behave the same way
    sl = Slh()
    maps = sl(ds_train)
    res1 = [m.forward(ds).samples.T for ds, m in zip(ds_test, maps)]

    slhyper = SearchlightHyperalignment(A)
    slhyper.fit(X_train)
    res2 = slhyper.transform(X_test)

    # We first test the direct transform
    assert len(res1) == len(res2)
    for i in range(len(res1)):
        assert res1[i].shape == res2[i].shape

    for i in range(len(res1)):
        assert_array_almost_equal(res1[i], res2[i], 5)

    # Then we check the reverse transform
    res1 = [m.forward(ds) for ds, m in zip(ds_test, maps)]
    reverse_res1 = [m.reverse(res).samples.T for res, m in zip(res1, maps)]

    reverse_res2 = slhyper.inverse_transform(res2)

    assert len(reverse_res1) == len(reverse_res2)
    for i in range(len(reverse_res1)):
        assert reverse_res1[i].shape == reverse_res2[i].shape
        assert_array_almost_equal(reverse_res1[i], reverse_res2[i], 4)



