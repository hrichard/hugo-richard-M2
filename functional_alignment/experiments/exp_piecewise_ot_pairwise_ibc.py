
import time

from nilearn.input_data import NiftiMasker
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from functional_alignment.load_data import load_ibc_3_mm_data, make_train_test_ibc_3_mm
from functional_alignment.utils import score_save_table, make_train_test_images

# Experiment parameters
# Methods tested
method = 'piecewise_ot'
# alignment method for piecewise
# in "scaled_orthogonal", "RidgeCV"
piecewise_method = "epsilon_scaling"
metrics = ['euclidean']
n_bootstraps = [1]  # bootstraping for piecewise

# Experiment crucial parameters
loss = "zero_mean_r2"  # "r2" or "zero_mean_r2"
n_jobs = 1

# Dataset and pairs of subjects tried
dataset = "ibc"  # number of timeframes in train set
all_ibc_pairs = [(12, 8), (2, 5), (2, 8), (3, 11),
                 (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
subject_pair_list = all_ibc_pairs

# Method specific parameters
n_pieces = 200
regs = [1e-5, 1e-3, 1e-2, 1e-1, 5 10]
# Data preparationcd
start_time = time.time()
image_list, mask_path = load_ibc_3_mm_data()

mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)

train_tasks = ["archi_standard", "archi_spatial",
               "archi_social", "archi_emotional", "rsvp_language"]
test_tasks = ["hcp_gambling",
              "hcp_motor", "hcp_emotion", "hcp_relational", "hcp_wm", "hcp_language", "hcp_social"]
print("Training tasks : %s" % train_tasks)
print("Testing tasks : %s" % test_tasks)
path_to_results = "/storage/tompouce/tbazeill/ibc/"

print("Path to results is %s " % path_to_results)
print("Dataset %s loaded in %s seconds " %
      (dataset, time.time() - start_time))
# Alignment method test : for each pair of subject, try all chosen methods

for subject_pair in subject_pair_list:
    start_time = time.time()
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
    X_train_list, X_test_list = make_train_test_ibc_3_mm(
        pair_image_list, train_tasks, test_tasks, masker)

    print("Data masked in : %s seconds" % str(time.time() - start_time))
    start_time = time.time()
    print("Aligning pair : %s" % str(subject_pair))
    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    # try every method in the library
    for metric in metrics:
        for n_bootstrap in n_bootstraps:
            for reg in regs:
                piecewise_ot_estim = PieceWiseAlignmentOT(
                    n_pieces=n_pieces, method=piecewise_method, metric=metric, reg=reg, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                piecewise_ot_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_ot_estim.transform(Im_1_test)
                results_title = str(n_bootstrap) + '_bootstraps_' + metric + "_" + str(subj_1) + "_" + \
                    str(subj_2) + "_" + str(reg) + "_" + loss + ".nii.gz"
                X_1_pred = masker.transform(Im_1_pred)
                score_save_table(masker, loss, path_to_results, method,
                                 results_title, X_2_test, X_1_pred)
    print("Methods fitted in %s seconds " %
          str(time.time() - start_time))
    # Creating aligned pair voxelwise R2 and correlation images
