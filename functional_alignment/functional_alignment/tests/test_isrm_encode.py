from functional_alignment.isrm_encode import laplacian_matrix
from functional_alignment.isrm_encode import similarity_matrix
from functional_alignment.isrm_encode import structure_matrix
from time import time

import numpy as np
from nilearn.decoding.objective_functions import _gradient
from numpy.testing import assert_array_almost_equal
from functional_alignment.isrm_encode import _solver_Ax_xB_C
from functional_alignment.isrm_encode import isrm_encode, online_isrm_encode
import scipy.sparse as sp
from sklearn.decomposition.dict_learning import sparse_encode


def build_mask(xrange, yrange, zrange, max_radius):
    mask = np.zeros((xrange, yrange, zrange))
    for x in range(xrange):
        for y in range(yrange):
            for z in range(zrange):
                radius = (x - xrange / 2) ** 2 + (y - yrange / 2)**2 + (z - zrange / 2) ** 2
                if np.sqrt(radius) < max_radius:
                    mask[x, y, z] = 1
    return mask.astype(bool)


def test_gradient():
    xrange = 60
    yrange = 60
    zrange = 60
    radius = 30
    mini_radius = 29

    mask = build_mask(xrange, yrange, zrange, radius)
    mini_mask = build_mask(xrange, yrange, zrange, mini_radius)
    X = np.random.rand(np.sum(mini_mask))
    X3D = np.zeros((xrange, yrange, zrange))
    X3D[mini_mask] = X

    X = X3D[mask]

    Lx, Ly, Lz = laplacian_matrix(mask)

    print(Lx)


    Gx1 = Lx.dot(X)
    Gy1 = Ly.dot(X)
    Gz1 = Lz.dot(X)

    Gx2, Gy2, Gz2 = _gradient(X3D)


    assert_array_almost_equal(np.linalg.norm(Gx1), np.linalg.norm(Gx2))
    assert_array_almost_equal(np.linalg.norm(Gy1), np.linalg.norm(Gy2))
    assert_array_almost_equal(np.linalg.norm(Gz1), np.linalg.norm(Gz2))


def test_common_weights():
    n_voxels = 1000
    n_subjects = 10

    X = []
    for i in range(n_subjects):
        X.append(np.random.rand(n_voxels))

    C = [np.linalg.norm(X[i] - np.mean(X, axis=0)) for i in range(n_subjects)]
    S = similarity_matrix(n_voxels, n_subjects)
    C2 = [np.linalg.norm(S[i].dot(np.concatenate(X))) for i in range(n_subjects)]

    assert_array_almost_equal(np.linalg.norm(C), np.linalg.norm(C2))


def test_solver_Ax_xB_C():
    A = np.random.rand(50, 50)
    A = A.T.dot(A)
    B = np.random.rand(300, 300)
    B = B.T.dot(B)
    C = np.random.rand(50, 300)

    X = _solver_Ax_xB_C(A, B, C)

    assert_array_almost_equal(A.dot(X) + X.dot(B), C)


def test_structure_matrix():
    xrange = 30
    yrange = 30
    zrange = 30
    radius = 15

    beta = 5.
    gamma = 10.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 5
    n_subjects = 5

    X = np.random.rand(n_timeframes, n_voxels * n_subjects)
    L = laplacian_matrix(mask)
    S = similarity_matrix(n_voxels, n_subjects)
    G = structure_matrix(beta, gamma, L, S)

    assert np.sum(G.T - G) == 0

    E1 = np.trace(X.dot(G.dot(X.T)))
    E2 = beta * np.sum((sp.kron(np.eye(n_subjects), L[0]).dot(X.T))**2)
    E2 += beta * np.sum((sp.kron(np.eye(n_subjects), L[1]).dot(X.T))**2)
    E2 += beta * np.sum((sp.kron(np.eye(n_subjects), L[2]).dot(X.T)) ** 2)

    for i in range(len(S)):
        E2 += gamma * np.sum((S[i].dot(X.T)) ** 2)

    assert_array_almost_equal(E1, E2)


def test_isrm_encode_convergence():
    xrange = 10
    yrange = 10
    zrange = 10
    radius = 5

    alpha = 3.
    beta = 3.
    gamma = 3.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 100
    n_subjects = 5
    n_components = 10

    G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))

    seed = np.random.randint(0, 1000)
    print(seed)
    np.random.seed(seed)
    S = np.random.rand(n_timeframes, n_components)
    X = np.random.rand(n_timeframes, n_voxels * n_subjects)

    W, dE, _, _ = isrm_encode(X, S, alpha, G, seed, tol=1e-12, max_iter=100)
    print(dE)
    assert dE > 0
    assert dE < 1e-6


def test_isrm_encode_vs_sparse_encode():
    xrange = 10
    yrange = 10
    zrange = 10
    radius = 5

    alpha = 3.
    beta = 0.
    gamma = 0.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 100
    n_subjects = 1
    n_components = 10

    G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))

    seed = np.random.randint(0, 1000)
    print(seed)
    np.random.seed(seed)
    S = np.random.rand(n_timeframes, n_components)
    X = np.random.rand(n_timeframes, n_voxels * n_subjects)

    W_1 = sparse_encode(X.T, S.T, alpha=alpha).T
    W_2, dE, _, _ = isrm_encode(X, S, alpha, G, seed,  tol=1e-15, max_iter=100)
    assert_array_almost_equal(W_1, W_2)


# def test_isrm_encode_vs_smooth_encode():
#     xrange = 10
#     yrange = 10
#     zrange = 10
#     radius = 5
#
#     alpha = 0.
#     beta = 3.
#     gamma = 3.
#
#     mask = build_mask(xrange, yrange, zrange, radius)
#
#     n_voxels = int(np.sum(mask))
#     n_timeframes = 100
#     n_subjects = 2
#     n_components = 10
#
#     G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))
#
#     seed = np.random.randint(0, 1000)
#     print(seed)
#     np.random.seed(seed)
#     S = np.random.rand(n_timeframes, n_components)
#     X = np.random.rand(n_timeframes, n_voxels * n_subjects)
#
#     batches = np.arange(n_voxels * n_subjects)
#     np.random.shuffle(batches)
#     batches = np.array_split(batches, n_subjects * 2)
#
#     W_1 = np.zeros((n_components, n_voxels * n_subjects))
#     W_2 = np.zeros((n_components, n_voxels * n_subjects))
#     for batch in batches:
#         print(batch.shape)
#         X_batch = X[:, batch]
#
#         t5 = time()
#         W_1 = online_smooth_encode(batch,
#                                    X_batch,
#                                    S,
#                                    W_1,
#                                    G
#                                    )
#         t6 = time()
#         W_2 = online_isrm_encode(batch, X_batch, S, W_2, alpha, G, seed, tol=1e-7)
#         t7 = time()
#
#         print(t7 - t6, t6 - t5)
#
#     assert_array_almost_equal(W_1, W_2)


def test_online_isrm_encode_as_batch_isrm_encode():
    xrange = 10
    yrange = 10
    zrange = 10
    radius = 5

    alpha = 3.
    beta = 3.
    gamma = 3.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 100
    n_subjects = 5
    n_components = 10

    G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))

    seed = np.random.randint(0, 1000)
    print(seed)
    np.random.seed(seed)
    S = np.random.rand(n_timeframes, n_components) * 100
    X = np.random.rand(n_timeframes, n_voxels * n_subjects) * 100

    W, dE, _ , _= isrm_encode(X, S, alpha, G, seed, tol=1e-15, max_iter=100)

    batch = np.arange(n_voxels * n_subjects)
    np.random.shuffle(batch)
    X_batch = X[:, batch]
    W2 = np.random.rand(n_components, n_voxels * n_subjects)
    W2 = online_isrm_encode(batch, X_batch, S, W2, alpha, G, seed, iter=100)
    assert_array_almost_equal(W, W2, 5)


def test_online_isrm_encode_vs_batch_isrm_encode():
    xrange = 10
    yrange = 10
    zrange = 10
    radius = 5

    alpha = 3.
    beta = 3.
    gamma = 3.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 100
    n_subjects = 5
    n_components = 10

    G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))

    seed = np.random.randint(0, 1000)
    print(seed)
    np.random.seed(seed)
    S = np.random.rand(n_timeframes, n_components) * 100
    X = np.random.rand(n_timeframes, n_voxels * n_subjects) * 100

    W, dE, _, _ = isrm_encode(X, S, alpha, G, seed, tol=0., max_iter=100)

    batches = np.arange(n_voxels * n_subjects)
    np.random.shuffle(batches)
    batches = np.split(batches, n_subjects)

    W2 = np.random.rand(n_components, n_voxels * n_subjects)
    for batch in batches:
        X_batch = X[:, batch]
        W2 = online_isrm_encode(batch, X_batch, S, W2, alpha, G, seed, iter=100)
    assert_array_almost_equal(W, W2, 4)


def test_online_isrm_encode_locality():
    xrange = 10
    yrange = 10
    zrange = 10
    radius = 5

    alpha = 3.
    beta = 3.
    gamma = 3.

    mask = build_mask(xrange, yrange, zrange, radius)

    n_voxels = int(np.sum(mask))
    n_timeframes = 100
    n_subjects = 5
    n_components = 10

    G = structure_matrix(beta, gamma, laplacian_matrix(mask), similarity_matrix(n_voxels, n_subjects))

    seed = np.random.randint(0, 1000)
    print(seed)
    np.random.seed(seed)
    S = np.random.rand(n_timeframes, n_components) * 100
    X = np.random.rand(n_timeframes, n_voxels * n_subjects) * 100

    batches = np.arange(n_voxels * n_subjects)
    np.random.shuffle(batches)
    batches = np.split(batches, n_subjects)
    batch = batches[0]
    W2 = np.zeros((n_components, n_voxels * n_subjects)) + np.pi
    W2_ = np.copy(W2)
    X_batch = X[:, batch]
    W2_ = online_isrm_encode(batch, X_batch, S, W2_, alpha, G, seed, iter=20)

    W2[:, batch] += 1
    assert_array_almost_equal(W2 != np.pi, W2_ != np.pi)



