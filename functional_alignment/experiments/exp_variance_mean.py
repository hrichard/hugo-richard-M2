from load_data import get_mask, load_data
import nibabel as nib
import numpy as np


print("Loading Data ...")
print("Testing Data")

dataset = "forrest"

masker = get_mask(dataset=dataset)
X, names = load_data(masker,
                 dataset=dataset)

X_test = X[1]
names = names[1]

n_features, n_components = X_test[0].shape

print("mean image computation")
mean_img = np.zeros((n_features, n_components))
n_half = len(X_test)//2 + len(X_test) % 2
for i in range(n_half):
    mean_img = mean_img + X_test[i]
mean_img /= n_half

print("explained variance computation")
for i in range(n_half, len(X_test)):
    var_e = (X_test[i] - mean_img).var(axis=1)
    exp_var = 1 - var_e
    img = masker.inverse_transform(exp_var)
    nib.save(
        img,
        "/storage/workspace/hrichard/results/exp4/" +
        dataset +
        names[i] +
        "_mean_img.nii"
    )
print("Done")
