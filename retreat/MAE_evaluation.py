# -*- coding: utf-8 -*-
import numpy
import pandas
def MAE_evaluation(X,y,algorithm,splits):
    
    ##################
    #Inputs 
    #X : data on which to train and test algorithm, 2D ndarray of dimensions (n_samples,n_features)
    #y : targets to use for fitting and evaluating the algorithm, 1D ndarray of dimension (n_samples)
    #algorithm : A python function which has a .fit and .predict method
    #splits : A list of tuples of two 1D lists or ndarrays, the first being the index of the training samples, the 
    #second the index of the test samples.
    
    #Outputs 
    #mae, the mean absolute error of the algorithm predictions of the test set
    #mae_train, the mean absolute error of the algorithm predictions on the train sample
    
    assert X.ndim == 2, 'Expected 2-dimensional data as input'
    mae=[]
    mae_train=[]
    for train,test in splits:
    
       
        X_train = X[train]
        X_test = X[test]
        Y_train = y[train]
        Y_test = y[test]
        algorithm.fit(X_train,Y_train)
        Y_pred = (algorithm.predict(X_test))
        mae.append(np.mean(np.abs(Y_pred-Y_test)))
        Y_pred_train = algorithm.predict(X_train)
        mae_train.append(np.mean(np.abs(Y_pred_train-Y_train)))

    return mae,mae_train

    def MAE_plotting(X,y,algorithms,splits,algorithm_names=None,plot_train_mae=False):
    #########
    #Inputs
    #X : Data to use as training as testing set, 2D ndarray
    #y : Targets, 1D ndarray
    #algorithms : algorithms for which to plot the MAE, python functions with a .fit and .predict method
    #splits : Train/test splits, list of tuples of indexes
    #algorithm_names : Names of the algorithms, list of str
    #plot_train_mae : Boolean, set to True if you want to plot the error on the training set
    
    #Outputs
    #Nice boxplots
    
    
    
    
    mae_list = []
    mae_train_list = []
    for algo in algorithms:
        mae,mae_train = MAE_evaluation(X,y,algo,splits)
        mae_list.append(mae)
        mae_train_list.append(mae_train)
        mae_list,mae_train_list = np.array(mae_list),np.array(mae_train_list)
    mae_df = pandas.DataFrame(mae_list.transpose(), columns=algorithm_names)
    mae_df.boxplot(algorithm_names)
    if plot_train_mae:
        mae_train_df = pandas.DataFrame(mae_train_list.transpose(),columns=algorithm_names)
        plt.figure(2)
        mae_train_df.boxplot(algorithm_names)
    