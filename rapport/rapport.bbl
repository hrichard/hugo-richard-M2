\newcommand{\etalchar}[1]{$^{#1}$}
\begin{thebibliography}{VEUA{\etalchar{+}}12}

\bibitem[APE{\etalchar{+}}14]{nilearn}
Alexandre Abraham, Fabian Pedregosa, Michael Eickenberg, Philippe Gervais,
  Andreas Mueller, Jean Kossaifi, Alexandre Gramfort, Bertrand Thirion, and
  Ga{\"e}l Varoquaux.
\newblock Machine learning for neuroimaging with scikit-learn.
\newblock {\em Frontiers in neuroinformatics}, 8, 2014.

\bibitem[BKM{\etalchar{+}}91]{belliveau1991functional}
JW~Belliveau, DN~Kennedy, RC~McKinstry, BR~Buchbinder, RMt Weisskoff, MS~Cohen,
  JM~Vevea, TJ~Brady, and BR~Rosen.
\newblock Functional mapping of the human visual cortex by magnetic resonance
  imaging.
\newblock {\em Science}, 254(5032):716--719, 1991.

\bibitem[BLNZ95]{byrd1995limited}
Richard~H Byrd, Peihuang Lu, Jorge Nocedal, and Ciyou Zhu.
\newblock A limited memory algorithm for bound constrained optimization.
\newblock {\em SIAM Journal on Scientific Computing}, 16(5):1190--1208, 1995.

\bibitem[CCY{\etalchar{+}}15]{srm}
Po-Hsuan~Cameron Chen, Janice Chen, Yaara Yeshurun, Uri Hasson, James Haxby,
  and Peter~J Ramadge.
\newblock A reduced-dimension fmri shared response model.
\newblock In {\em Advances in Neural Information Processing Systems}, pages
  460--468, 2015.

\bibitem[ECM{\etalchar{+}}93]{evans19933d}
Alan~C Evans, D~Louis Collins, SR~Mills, ED~Brown, RL~Kelly, and Terry~M
  Peters.
\newblock 3d statistical neuroanatomical models from 305 mri volumes.
\newblock In {\em Nuclear Science Symposium and Medical Imaging Conference,
  1993., 1993 IEEE Conference Record.}, pages 1813--1817. IEEE, 1993.

\bibitem[FBK11]{fedorenko2011functional}
Evelina Fedorenko, Michael~K Behr, and Nancy Kanwisher.
\newblock Functional specificity for high-level linguistic processing in the
  human brain.
\newblock {\em Proceedings of the National Academy of Sciences},
  108(39):16428--16433, 2011.

\bibitem[Fis12]{fischl2012freesurfer}
Bruce Fischl.
\newblock Freesurfer.
\newblock {\em Neuroimage}, 62(2):774--781, 2012.

\bibitem[GHH{\etalchar{+}}16]{hyper2}
J~Swaroop Guntupalli, Michael Hanke, Yaroslav~O Halchenko, Andrew~C Connolly,
  Peter~J Ramadge, and James~V Haxby.
\newblock A model of representational spaces in human cortex.
\newblock {\em Cerebral cortex}, 26(6):2919--2934, 2016.

\bibitem[GVR{\etalchar{+}}15]{neurovault}
Krzysztof~J Gorgolewski, Gael Varoquaux, Gabriel Rivera, Yannick Schwarz,
  Satrajit~S Ghosh, Camille Maumet, Vanessa~V Sochat, Thomas~E Nichols,
  Russell~A Poldrack, Jean-Baptiste Poline, et~al.
\newblock Neurovault. org: a web-based repository for collecting and sharing
  unthresholded statistical maps of the human brain.
\newblock {\em Frontiers in neuroinformatics}, 9, 2015.

\bibitem[HBI{\etalchar{+}}14]{forrest}
Michael Hanke, Florian~J Baumgartner, Pierre Ibe, Falko~R Kaule, Stefan
  Pollmann, Oliver Speck, Wolf Zinke, and J{\"o}rg Stadler.
\newblock A high-resolution 7-tesla fmri dataset from complex natural
  stimulation with an audio movie.
\newblock {\em Scientific data}, 1:140003, 2014.

\bibitem[Hes12]{hestenes2012conjugate}
Magnus~Rudolph Hestenes.
\newblock {\em Conjugate direction methods in optimization}, volume~12.
\newblock Springer Science \& Business Media, 2012.

\bibitem[HGC{\etalchar{+}}11]{hyperalignment}
James~V Haxby, J~Swaroop Guntupalli, Andrew~C Connolly, Yaroslav~O Halchenko,
  Bryan~R Conroy, M~Ida Gobbini, Michael Hanke, and Peter~J Ramadge.
\newblock A common, high-dimensional model of the representational space in
  human ventral temporal cortex.
\newblock {\em Neuron}, 72(2):404--416, 2011.

\bibitem[KAA{\etalchar{+}}09]{klein2009evaluation}
Arno Klein, Jesper Andersson, Babak~A Ardekani, John Ashburner, Brian Avants,
  Ming-Chang Chiang, Gary~E Christensen, D~Louis Collins, James Gee, Pierre
  Hellier, et~al.
\newblock Evaluation of 14 nonlinear deformation algorithms applied to human
  brain mri registration.
\newblock {\em Neuroimage}, 46(3):786--802, 2009.

\bibitem[MBPS09]{mairal2009online}
Julien Mairal, Francis Bach, Jean Ponce, and Guillermo Sapiro.
\newblock Online dictionary learning for sparse coding.
\newblock In {\em Proceedings of the 26th annual international conference on
  machine learning}, pages 689--696. ACM, 2009.

\bibitem[MZ11]{mukherjee2011reduced}
Ashin Mukherjee and Ji~Zhu.
\newblock Reduced rank ridge regression and its kernel extensions.
\newblock {\em Statistical Analysis and Data Mining: The ASA Data Science
  Journal}, 4(6):612--622, 2011.

\bibitem[NLG{\etalchar{+}}17]{nenning2017diffeomorphic}
Karl-Heinz Nenning, Hesheng Liu, Satrajit~S Ghosh, Mert~R Sabuncu, Ernst
  Schwartz, and Georg Langs.
\newblock Diffeomorphic functional brain surface alignment: Functional demons.
\newblock {\em NeuroImage}, 2017.

\bibitem[PBM{\etalchar{+}}13]{openfmri}
Russell~A Poldrack, Deanna~M Barch, Jason~P Mitchell, Tor~D Wager, Anthony~D
  Wagner, Joseph~T Devlin, Chad Cumba, Oluwasanmi Koyejo, and Michael~P Milham.
\newblock Toward open sharing of task-based fmri data: the openfmri project.
\newblock {\em Frontiers in neuroinformatics}, 7, 2013.

\bibitem[PVG{\etalchar{+}}11]{scikit}
Fabian Pedregosa, Ga{\"e}l Varoquaux, Alexandre Gramfort, Vincent Michel,
  Bertrand Thirion, Olivier Grisel, Mathieu Blondel, Peter Prettenhofer, Ron
  Weiss, Vincent Dubourg, et~al.
\newblock Scikit-learn: Machine learning in python.
\newblock {\em Journal of Machine Learning Research}, 12(Oct):2825--2830, 2011.

\bibitem[RS90]{roy1890regulation}
Charles~Smart Roy and Charles~S Sherrington.
\newblock On the regulation of the blood-supply of the brain.
\newblock {\em The Journal of physiology}, 11(1-2):85--158, 1890.

\bibitem[VEUA{\etalchar{+}}12]{van2012human}
David~C Van~Essen, Kamil Ugurbil, E~Auerbach, D~Barch, TEJ Behrens, R~Bucholz,
  Acer Chang, Liyong Chen, Maurizio Corbetta, Sandra~W Curtiss, et~al.
\newblock The human connectome project: a data acquisition perspective.
\newblock {\em Neuroimage}, 62(4):2222--2231, 2012.

\bibitem[W{\etalchar{+}}00]{wegelin2000survey}
Jacob~A Wegelin et~al.
\newblock A survey of partial least squares (pls) methods, with emphasis on the
  two-block case.
\newblock {\em University of Washington, Department of Statistics, Tech. Rep},
  2000.

\end{thebibliography}
