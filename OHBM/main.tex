\documentclass[a4paper]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[colorinlistoftodos]{todonotes}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{apacite}
\usepackage{multicol}
\usepackage{bbm}

\title{Interpretable shared response model}
\author{Hugo Richard, Bertrand Thirion, Jonathan Pillow}

\begin{document}
\maketitle

\begin{abstract}
The functional variability between subjects constitutes a fundamental challenge specific to human brain understanding. 

The probabilistic shared response model (ProbSRM) \cite{chen2015reduced} projects subjects data in a low dimensional shared space in which inter-subject variability is reduced. However it is challenging to interpret data in shared space as coordinates are linked to spatial maps that are not sparse, not smooth and not positive. 

In this work, we provide an interpretable shared response model (ISRM) with sparsity, smoothness and positivity constrains on spatial maps releasing the orthogonality constrain of the original SRM algorithm. We show ISRM yields more interpretable maps than SRM and better reconstruction error on two different datasets. 
\end{abstract}

\section{Methods}
The generic shared response model is a latent factor model. The brain images of subject $i$ are stored in a matrix $X_i \in\mathbb{R}^{V, T}$ where $V$ is the number of voxels and $T$ the number of timeframes (the number of acquired brain images). Each brain image $X_i[:, t]$ of subject $i$ at time $t$ is seen as a weighted sum of $K$ subject specific spatial maps $W_i \in\mathbb{R}^{V, K}$. The weights $S \in \mathbb{R}^{K, T}$ are shared between subjects.

Depending on assumptions made on the noise, on spatial maps and on the shared response, this model has various names. DeterministicSRM and ProbabilisticSRM impose orthogonality constrains on spatial maps and Gaussian noise but in contrast to ProbabilisticSRM, DeterministicSRM assumes same variance for all subjects.

We introduce the interpretable SRM (ISRM) relieving orthogonality constrains by imposing sparsity, smoothness and positivity constrains on spatial maps. Our optimization procedure is initialized by FastDetSRM. A faster implementation of DetSRM where the shared response is found using a low dimensional representation of the recorded bold response of each subject.

Our procedure has two parameters: $\alpha$ the sparsity controlling parameter and $\beta$ the smoothness controlling parameter.

\section{Results}
We use two freely available datasets namely the IBC dataset \cite{ibc} (video stimuli) and Forrest dataset \url{http://studyforrest.org} (audio stimuli). 

We use a cosmoothing setting. In a first step all brain recordings for all sessions but one are used for learning subjects spatial maps. In a second step we focus on the leftout session and use all subjects but one to compute a shared response for the leftout session. Finally we predict the brain activity of the leftout subject for the leftout session. We report the average of results for different left-out subjects.

We measure performance using the number of voxels that have coefficient of determination above $0.05$. This measures the number of well predicted voxels (higher is better). We measure sparsity by counting the mean number of zero coefficients in spatial maps (higher is more sparse) and smoothness error with the l2 norm of the gradient (lower is more smooth).

We first show the influence of each parameters on the interpretability of spatial maps and on performance on the train set of the forrest dataset. Our experiments show that there is a trade-off between interpretability and performance. Based on this we recommand $\alpha=10$ and $\beta=100$ as the default parameter choice. However when interpretability is not an issue, one can set $\alpha=\beta=0$ and only keep the positivity constrains.


We benchmarked ISRM against orthogonal SRM on the Forrest and IBC dataset using regions of interest where ProbSRM performs the best and smooth them using mathematical morphology techniques.

We see that our model produces the best results in region of interests and the more interpretable maps on two different datasets (see Figure~\ref{fig:ibc_roi_results} and Figure~\ref{fig:forrest_roi_results}). 

\begin{figure}
\centering
\includegraphics[scale=0.6]{figures/performance_landscape_positive_50.pdf}
\includegraphics[scale=0.6]{figures/sparsity_landscape_positive_50.pdf}
\includegraphics[scale=0.6]{figures/smoothness_landscape_positive_50.pdf}
\begin{tabular}{llll}
 &PCA  &ProbSRM & FastDetSRM  \\
 Performance & 7094  &8712  & 8727 \\
 Smoothness & -  & -  & 1.78 \\
 Sparsity & 0.78 & 0.78 & 0.78 
\end{tabular}
\caption{Performance, Sparsity and Smoothness of ISRM, PCA, ProbSRM and FastDetSRM: We see that ISRM achieves best performance for a large range of parameters. We can see that for a good choice of parameters such as $\alpha = 10$ and $\beta = 1000$ ISRM beats all other algorithms both in interpretability and performance.}
\label{fig:parameter_landscape}
\end{figure}

\begin{figure}
\centering
\caption{Forrest Dataset: Comparing performance (higher is better) of ProbSRM, PCA, ISRM with no constrains (only positivity constrains), ISRM with sparsity and smoothness constrains ($\alpha = 10.$ and $\beta=100.$) on IBC dataset (top) and Forrest dataset (bottom). ISRM outperforms ProbSRM and DetSRM on both datasets and is competitive with online PCA. Additional plots (not included) show that ISRM is faster than ProbSRM and PCA and also gives sparser, smoother and positive spatial maps than ProbSRM and PCA.}
\includegraphics[scale=0.6]{figures/nvoxels_forrest.pdf}
\includegraphics[scale=0.6]{figures/sparsity_forrest_roi.pdf}
\includegraphics[scale=0.6]{figures/smoothness_forrest_roi.pdf}
\label{fig:forrest_roi_results}
\end{figure}

\begin{figure}
\centering
\caption{IBC dataset: Comparing performance (top, higher is better), sparsity (middle, higher is better) and smoothness (bottom, lower is better) of ProbSRM, PCA, ISRM with no constrains (only positivity constrains), ISRM with sparsity and smoothness constrains ($\alpha = 10.$ and $\beta=100.$) on IBC dataset (top) and Forrest dataset (bottom). ISRM outperforms all other algorithm in terms of performance and interpretability. Note that we did not compute the smoothness of PCA because the differences in the norm of the spatial maps makes the comparison unfair.}
\includegraphics[scale=0.6]{figures/nvoxels_ibc.pdf}
\includegraphics[scale=0.6]{figures/sparsity_ibc_roi.pdf}
\includegraphics[scale=0.6]{figures/smoothness_ibc_roi.pdf}
\label{fig:ibc_roi_results}
\end{figure}

\section{Discussion}
Orthogonality constrains yield efficient computations but is not supported by any biological evidence, it also leads to spatial maps that are difficult to interpret. Our interpretable shared response model (ISRM) yields positive smooth and sparse maps. ISRM also yields better results than the original shared response model on the two dataset we tested. 

Future work should focus on using ISRM to produce a template for the entire brain.  One path to explore would be to apply the algorithm on brain parcellations (the computations on each parcel can be done in parallel).


\section{figures}

\cite{abraham2014machine}
\cite{beck2009ista}
\cite{beck2009mfista}
\cite{becker2011templates}
\cite{brainiak}
\cite{brett2002problem}
\cite{chen2015reduced}
\cite{chen2016convolutional}
\cite{dohmatob2016learning}
\cite{fischl2012freesurfer}
\cite{gorgolewski2015neurovault}
\cite{guntupalli2016model}
\cite{halko2009finding}
\cite{hanke2014high}
\cite{ibc}
\cite{mairal2009online}
\cite{manning2014topographic}
\cite{mensch2016compressed}
\cite{nishimoto2011reconstructing}
\cite{pedregosa2011scikit}
\cite{pinho2018individual}
\cite{poldrack2013toward}
\cite{scikitlearn}
\cite{shvartsman2017matrix}
\cite{spacenet}
\cite{varoquaux2011multi}


\bibliographystyle{apacite}
\bibliography{biblio}

\end{document}