"""
Experiment 5: We first select region of interest using previous algorithms (for forrest dataset)
Then we learn a basis for subject 1 to 10 usinhyperalignmentg .
Can we reconstruct the response for these subject for a similar stimuli ?
"""

import nibabel as nib
from nilearn.input_data import NiftiMasker
from sklearn.model_selection import ShuffleSplit
import numpy as np
from ridge_hyperalignment import RidgeHyperalignment
import os
import logging

logging.basicConfig(level=logging.DEBUG, filename="logfile_ridgehyperalignment", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.info("Loading Data ...")
dataset = "forrest"
subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
runs = ["task001_run001",
        "task001_run002"]

mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
mask_path = mask_dir + "bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"
mask_img = nib.load(mask_path)
masker = NiftiMasker(
    mask_img=mask_path,
    standardize=True,
    detrend=True,
    memory="/storage/workspace/hrichard/cache_forrest/"
).fit()

logging.info("Masker loaded")
logging.debug(masker.affine_)

X = []
for run in runs:
    X_run = []
    for subject in subjects:
        func_filename = ("/storage/data/openfmri/ds113/sub" +
                         "%03d" +
                         "/BOLD/" +
                         run +
                         "/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"
                         ) % subject
        sample = masker.transform(func_filename).T
        logging.info("run: %s, subject: %s" % (run, str(subject)))
        logging.debug(sample.shape)
        logging.debug(nib.load(func_filename).affine)
        X_run.append(sample)
    X.append(X_run)
logging.info("Done")

X_train1 = X[0]
X_test = X[1]

rs = ShuffleSplit(n_splits=10, test_size=.1)
rs.split(X_train1)


def update(algorithm, name=None):
    def func(X_train1, Y_train1, X_test):
            logging.info(X_train1.shape)
            rh = algorithm.fit(X_train1.T, Y_train1.T)
            return rh.transform(X_test.T).T

    if name is None:
        name = str(algorithm)

    return func, name


algorithms = [update(RidgeHyperalignment(alpha=1e6), name="RH_1e6")]

mean_exp_var = {}
for algorithm, name in algorithms:
    mean_exp_var[name] = []

for train_index, test_index in rs.split(X_train1):
    logging.info("TRAIN:" +
            train_index.__str__() +
            "TEST:" +
            test_index.__str__())
    X_train1_left_out = X_train1[test_index[0]]
    X_test_left_out = X_test[test_index[0]]

    for algorithm, name in algorithms:
        logging.info(name)
        Y = []
        for i in train_index:
            logging.info("TRAIN:" + str(i))
            Y.append(algorithm(X_train1[i],
                               X_train1_left_out,
                               X_test[i]
                               ))
        Y = np.array(Y).mean(axis=0)
        var_e = (X_test_left_out - Y).var(axis=1)
        mean_e = (X_test_left_out - Y).mean(axis=1).sum()
        logging.info((1 - var_e).mean())
        exp_var = 1 - var_e
        mean_exp_var[name].append(exp_var)

for algorithm, name in algorithms:
    mean_exp_var[name] = np.array(mean_exp_var[name]).mean(axis=0)
    img = masker.inverse_transform(mean_exp_var[name])
    nib.save(
        img,
        "/storage/workspace/hrichard/results/exp8/" +
        dataset +
        "alignment" +
        "_algo" +
        name +
        "_exp8.nii"
    )
logging.info("Done")
