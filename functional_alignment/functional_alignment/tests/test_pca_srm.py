from functional_alignment.pca_srm import PCASRM
import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
import scipy as np
import scipy.sparse as sp
import scipy.stats as stats
import nibabel
from nilearn.input_data import NiftiMasker
import os

n_features = 1000
n_components = 100
nb_time_frames = 400
n_samples = 3

# We generate train and test data (not normalized)
latent_variance = np.eye(n_components, n_components)

train_shared = np.random.multivariate_normal(
    np.zeros(n_components),
    latent_variance,
    nb_time_frames).T

train_shared = train_shared - np.mean(train_shared, axis=1, keepdims=True)

test_shared = np.random.multivariate_normal(
    np.zeros(n_components),
    latent_variance,
    nb_time_frames).T

test_shared = test_shared - np.mean(test_shared, axis=1, keepdims=True)

basis = [sp.random(
        n_features,
        n_components,
        1,
        data_rvs=stats.norm(loc=0, scale=1).rvs).toarray() for i in range(n_samples)]

train_X = []
for i in range(len(basis)):
    train_X.append(basis[i].dot(train_shared))

test_X = []
for i in range(len(basis)):
    test_X.append(basis[i].dot(test_shared))


affine = np.eye(4)
shape = (10, 10, 10, nb_time_frames)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine)).fit()

masked_train_X = []
for i in range(len(train_X)):
    masked_train_X.append([masker.inverse_transform(train_X[i][:, :int(nb_time_frames / 3)].T),
                          masker.inverse_transform(train_X[i][:, int(nb_time_frames / 3):int(2 * nb_time_frames / 3)].T),
                          masker.inverse_transform(train_X[i][:, int(2 * nb_time_frames / 3):].T)])

masked_test_X = []
for i in range(len(train_X)):
    masked_test_X.append([masker.inverse_transform(test_X[i][:, :int(nb_time_frames / 3)].T),
                          masker.inverse_transform(test_X[i][:, int(nb_time_frames / 3):int(2 * nb_time_frames / 3)].T),
                          masker.inverse_transform(test_X[i][:, int(2 * nb_time_frames / 3):].T)])


def test_shapes_input():
    assert len(train_X) == n_samples
    for i in range(len(train_X)):
        assert train_X[i].shape == (n_features, nb_time_frames)


def test_shapes_fit_transform():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    shared = srm.fit_transform(masked_train_X)
    basis = srm.basis
    for i in range(n_samples):
        assert basis[i].shape == (n_features, n_components)
    assert shared.shape == (n_components, nb_time_frames)


def test_shapes_transform():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    srm.fit(masked_train_X)
    assert srm.transform(masked_train_X).shape == (n_components, nb_time_frames)


def test_shapes_fit():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    srm.fit(masked_train_X)
    basis = srm.basis
    for i in range(n_samples):
        assert basis[i].shape == (n_features, n_components)


def test_shapes_inverse_transform():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    shared = srm.fit_transform(masked_train_X)
    base_data = srm.inverse_transform(shared)
    for i in range(n_samples):
        assert base_data[i].shape == (n_features, nb_time_frames)


def test_orthogonal_basis():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    srm.fit(masked_train_X)
    W = np.row_stack(srm.basis)
    assert_array_almost_equal(W.T.dot(W), np.eye(n_components))


def test_srm_property():
    srm = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    shared = srm.fit_transform(masked_train_X)
    basis = srm.basis

    srm2 = PCASRM(n_components, mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine))
    srm2.fit(masked_train_X)

    for i in range(n_samples):
        assert np.linalg.norm(train_X[i] - basis[i].dot(shared)) < 1e-6


def test_transform_fit_transform():
    for srm in [
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="mean"
        ),
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="stack"
        )
    ]:
        srm.fit(masked_train_X)
        shared = srm.transform(masked_train_X)
        basis = srm.basis
        for i in range(n_samples):
            assert np.linalg.norm(train_X[i] - basis[i].dot(shared)) < 1e-6


def test_findcommon_space():
    for srm in [
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="mean"
        ),
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="stack"
        )
    ]:
        srm.fit(masked_train_X)
        srm.basis = basis
        srm.shared_response = None
        S = srm.transform(masked_train_X)
        S2 = srm.transform(masked_test_X)
        assert_array_almost_equal(S, train_shared)
        assert_array_almost_equal(S2, test_shared)


def test_srm_property_testset():
    for srm in [
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="mean"
        ),
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="stack"
        )
    ]:
        srm.fit(masked_train_X)
        S = srm.transform(masked_test_X[:-1], index=range(len(test_X) - 1))
        Y = srm.inverse_transform(S, index=[len(test_X) - 1])[0]
        assert_array_almost_equal(Y, test_X[-1])


def test_transform_inverse_transform():
    for srm in [
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="mean"
        ),
        PCASRM(
            n_components,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="stack"
        )
    ]:
        srm.basis = basis
        for i in range(len(masked_train_X)):
            train_X_i = srm.inverse_transform(train_shared, index=[i])[0]
            assert_array_almost_equal(train_X_i, train_X[i])
        for i in range(len(test_X)):
            test_X_i = srm.inverse_transform(test_shared, index=[i])[0]
            assert_array_almost_equal(test_X_i, test_X[i])


def test_masked_property_testset():
    for srm in [PCASRM(n_components,
                       transform_method="mean",
                       mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine)),
                PCASRM(n_components,
                       transform_method="stack",
                       mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine)
                       )]:
        srm.fit(masked_train_X)
        S = srm.transform(masked_test_X[:-1], index=range(len(test_X) - 1))
        Y = srm.inverse_transform(S, index=[len(test_X) - 1])[0]
        assert_array_almost_equal(Y, np.column_stack([masker.transform(masked_test_X[-1][i]).T for i in range(3)]))


def test_setcomponents():
    for srm in [
        PCASRM(
            n_components + 1,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="mean"
        ),
        PCASRM(
            n_components + 1,
            mask=nibabel.Nifti1Image(np.ones(shape[:3], dtype=np.int8), affine),
            transform_method="stack"
        )
    ]:
        print(srm)
        srm.fit(masked_train_X)
        srm.set_components(n_components)
        S = srm.transform(masked_test_X[:-1], index=range(len(test_X) - 1))
        Y = srm.inverse_transform(S, index=[len(test_X) - 1])[0]
        assert_array_almost_equal(Y, test_X[-1])