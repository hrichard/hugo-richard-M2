import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
import nibabel
from nilearn.input_data import NiftiMasker
from functional_alignment.euclidian_template import euclidian_mean_with_index_img, euclidian_mean_with_masking

sub = np.tile(np.ones((5, 4)), (5, 5, 1, 1))

sub_1 = nibabel.Nifti1Image(sub, np.eye(4))
sub_2 = nibabel.Nifti1Image(2 * sub, np.eye(4))
sub_3 = nibabel.Nifti1Image(3 * sub, np.eye(4))

ref_template = 2 * sub

mask_img = nibabel.Nifti1Image(np.ones((5, 5, 5)), np.eye(4))

imgs = [sub_1, sub_2, sub_3]
masker = NiftiMasker(mask_img=mask_img)
masker.fit()


def test_euclidian_template_wt_index():

    # apply class
    euclidian_template = euclidian_mean_with_index_img(imgs)
    # compare both results
    assert_array_almost_equal(
        ref_template, euclidian_template.template.get_data())


def test_euclidian_template_wt_masking():
    euclidian_template = euclidian_mean_with_masking(imgs, masker)
    assert_array_almost_equal(
        ref_template, euclidian_template.get_data())
