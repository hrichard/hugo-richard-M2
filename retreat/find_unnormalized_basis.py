import joblib
import nilearn
from glob import glob
from nilearn.input_data import MultiNiftiMasker
import numpy as np

nilearn.CHECK_CACHE_VERSION = False
paths = glob("/storage/tompouce/hrichard/retreat_2019/camcan/*task-Movie*preproc*.nii.gz")


masker = MultiNiftiMasker(mask_img='/home/parietal/hrichard/cogspaces_data/mask/hcp_mask.nii.gz',
                          smoothing_fwhm=5,
                          standardize=True,
                          detrend=True,
                          t_r=2,
                          memory="/storage/tompouce/hrichard/retreat_2019/",
                          memory_level=1
                          ).fit()

S = joblib.load("/storage/tompouce/hrichard/retreat_2019/shared_response")[0]
pinv_S = np.linalg.pinv(S.T.dot(S)).dot(S.T)
for p in paths:
    name = p.split("/storage/tompouce/hrichard/retreat_2019/camcan/")[1].split(".nii.gz")[0]
    X = np.load("/storage/tompouce/hrichard/retreat_2019/data/" + name + ".npy")
    basis = pinv_S.dot(X)
    np.save("/storage/tompouce/hrichard/retreat_2019/unnormalized_big_basis/" + name, basis)
