import numpy as np
from scipy.spatial.distance import cdist
from sklearn.base import BaseEstimator, TransformerMixin
import copy
from joblib import Parallel, delayed
from time import time
from sklearn.externals.joblib import Memory
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from scipy.sparse import block_diag, bsr_matrix
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from nilearn.image import index_img, load_img


def sinkhorn_joint(n, M_list, reg, numItermax=500, stopThr=1e-9, verbose=True, print_period=10):
    s = len(M_list)
    b = 1 / n * np.ones(n * s)
    Ks_list = []
    for M in M_list:
        M = np.asarray(M, dtype=np.float64)
        Ks = np.empty_like(M)
        np.divide(M, -reg, out=Ks)
        np.exp(Ks, out=Ks)
        Ks_list.append(Ks)
    K = block_diag(Ks_list).tocsr()
    u, v = 1 / n * np.ones(n * s), 1 / n * np.ones(n * s)
    loop = 1
    cpt = 0
    err = 1
    while loop:
        print(cpt)
        # sinkhorn update
        uprev = u
        vprev = v
        np.divide(b, K.T.dot(u) + 1e-16, out=v)
        a = np.ones(len(b))
        Kv = K.dot(v) + 1e-16
        for i in range(len(M_list)):
            a *= np.roll(Kv, int(i * (len(b) / len(M_list))))
        np.divide(a, Kv, out=u)

        if np.any(np.isnan(u)) or np.any(np.isnan(v))or np.any(np.isinf(u)) or np.any(np.isinf(v)):
            # we have reached the machine precision
            # come back to previous solution and quit loop
            print('Warning: numerical errors at iteration', cpt)
            u = uprev
            v = vprev
            break

        if cpt % print_period == 0:
            # we can speed up the process by checking for the error only all
            # the 10th iterations
            err = np.linalg.norm(u - uprev)**2 / np.sum((u)**2) + \
                np.linalg.norm(v - vprev)**2 / np.sum((v)**2)
            if verbose:
                if cpt % (print_period * 20) == 0:
                    print(
                        '{:5s}|{:12s}'.format('It.', 'Improvement') + '\n' + '-' * 19)
                print('{:5d}|{:8e}|'.format(cpt, err))

        if err <= stopThr:
            loop = False
        if cpt >= numItermax:
            loop = False
        cpt = cpt + 1
    return bsr_matrix(K.T.multiply(u).T.multiply(v)).data


def sinkhorn_joint_stabilized(b, M_list, reg, numItermax=1000, tau=1e3, stopThr=1e-9, warmstart=None, verbose=False, print_period=20, log=False, **kwargs):
    Ks_list = []
    for M in M_list:
        M = np.asarray(M, dtype=np.float64)
        Ks = np.empty_like(M)
        np.divide(M, -reg, out=Ks)
        np.exp(Ks, out=Ks)
        Ks_list.append(Ks)
    K_base = block_diag(Ks_list).tocsr()
    b = np.asarray(b, dtype=np.float64)
    # init data
    cpt = 0
    # we assume that no distances are null except those of the diagonal of
    # distances

    def get_K(alpha, beta):
        """log space computation"""
        Alp = np.empty_like(alpha)
        np.divide(alpha, reg, out=Alp)
        np.exp(Alp, out=Alp)
        Bet = np.empty_like(beta)
        np.divide(beta, reg, out=Bet)
        np.exp(Bet, out=Bet)
        return K_base.T.multiply(Alp).T.multiply(Bet)

    def get_Gamma(alpha, beta, u, v):
        """log space gamma computation"""
        return get_K(alpha, beta).T.multiply(u).T.multiply(v)
    if warmstart is None:
        alpha, beta = np.zeros(len(b)), np.zeros(len(b))
        K = K_base
        transp = K_base
    else:
        alpha, beta = warmstart
        K = get_K(alpha, beta)
        transp = K
    u, v = np.ones(len(b)) / (len(b) / len(M_list)
                              ), np.ones(len(b)) / (len(b) / len(M_list))

    # print(np.min(K))

    loop = 1
    cpt = 0
    err = 1
    while loop:
        # sinkhorn update
        uprev = u
        vprev = v
        np.divide(b, K.T.dot(u) + 1e-16, out=v)
        a = np.ones(len(b))
        Kv = K.dot(v) + 1e-16
        for i in range(len(M_list)):
            a *= np.roll(Kv, int(i * (len(b) / len(M_list))))
        np.divide(a, Kv, out=u)

        # remove numerical problems and store them in K
        if np.abs(u).max() > tau or np.abs(v).max() > tau:
            alpha, beta = alpha + reg * np.log(u), beta + reg * np.log(v)
            u, v = np.ones(len(b)) / (len(b) / len(M_list)
                                      ), np.ones(len(b)) / (len(b) / len(M_list))
            K = get_K(alpha, beta)

        if cpt % print_period == 0:
            # we can speed up the process by checking for the error only all
            # the 10th iterations
            transp = get_Gamma(alpha, beta, u, v)
            err = np.linalg.norm((np.sum(transp, axis=0) - b))**2
            if verbose:
                if cpt % (print_period * 20) == 0:
                    print(
                        '{:5s}|{:12s}'.format('It.', 'Err') + '\n' + '-' * 19)
                print('{:5d}|{:8e}|'.format(cpt, err))

        if err <= stopThr:
            loop = False
        if cpt >= numItermax:
            loop = False

        if np.any(np.isnan(u)) or np.any(np.isnan(v)):
            # we have reached the machine precision
            # come back to previous solution and quit loop
            print('Warning: numerical errors at iteration', cpt)
            u = uprev
            v = vprev
            break

        cpt = cpt + 1
    G = get_Gamma(alpha, beta, u, v)
    alpha += reg * np.log(u)
    beta += reg * np.log(v)
    # print('err=',err,' cpt=',cpt)
    return G, alpha, beta


def sinkhorn_joint_epsilon_scaling(n, M_list, reg, numItermax=30, epsilon0=1e3, numInnerItermax=100,
                                   tau=1e3, stopThr=1e-9, warmstart=None, verbose=True, print_period=10):

    s = len(M_list)
    b = np.ones(n * s) * 1 / n
    # init data
    # nrelative umerical precision with 64 bits
    numItermin = 35
    numItermax = max(numItermin, numItermax)  # ensure that last velue is exact
    cpt = 0
    # we assume that no distances are null except those of the diagonal of
    # distances
    alpha, beta = np.zeros(n * s), np.zeros(n * s)

    def get_reg(iter):  # exponential decreasing
        return (epsilon0 - reg) * np.exp(-iter) + reg

    loop = 1
    cpt = 0
    err = 1
    while loop:
        n, M_list, reg,
        regi = get_reg(cpt)
        G, alpha, beta = sinkhorn_joint_stabilized(b, M_list, regi, numItermax=numInnerItermax, stopThr=1e-9, warmstart=(
            alpha, beta), verbose=True, print_period=10, tau=tau)

        if cpt >= numItermax:
            loop = False

        if cpt % (print_period) == 0:  # spsion nearly converged
            # we can speed up the process by checking for the error only all
            # the 10th iterations
            transp = G
            err = np.linalg.norm((np.sum(transp, axis=0) - b))**2

            if verbose:
                if cpt % (print_period * 10) == 0:
                    print(
                        '{:5s}|{:12s}'.format('It.', 'Err') + '\n' + '-' * 19)
                print('{:5d}|{:8e}|'.format(cpt, err))

        if err <= stopThr and cpt > numItermin:
            loop = False

        cpt = cpt + 1
    return bsr_matrix(G).data


def make_template_piece(X_i_list, mean_X_i, reg=5e-1, metric='euclidean', method='sinkhorn', n_iter=4):
    # create 1D histograms of length len(X_i) and where all elements have equal weights and sum to 1
    template_i = mean_X_i
    template_history_i = []
    iter = 0
    n = len(mean_X_i)
    while iter < n_iter:
        M_list = []
        for X_i in X_i_list:
            M_list.append(cdist(X_i, template_i, metric=metric))
        if method == 'epsilon_scaling':
            P_list = sinkhorn_joint_epsilon_scaling(n, M_list, reg)
        else:
            P_list = sinkhorn_joint(n, M_list, reg)
        template_i = np.mean([P.dot(X)
                              for P, X in zip(P_list, X_i_list)], axis=0)
        if iter < n_iter - 1:
            template_history_i.append(template_i)
        iter += 1
    return template_i, template_history_i


def fast_dot(sp_X, Y):
    return sp_X.dot(Y)


def piecewise_dot(labels, tf, X):
    """ piecewise dot product (avoids forming large matrices)"""
    X_ = np.zeros_like(X)
    for i in np.unique(labels):
        X_[labels == i] = tf[i].dot(X[labels == i])
    return X_


def hierarchical_k_means(X, n_clusters):
    """ use a recursive k-means to cluster X"""
    from sklearn.cluster import MiniBatchKMeans
    n_big_clusters = int(np.sqrt(n_clusters))
    mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_big_clusters, batch_size=1000,
                          n_init=10, max_no_improvement=10, verbose=0,
                          random_state=0).fit(X)
    coarse_labels = mbk.labels_
    fine_labels = np.zeros_like(coarse_labels)
    q = 0
    for i in range(n_big_clusters):
        n_small_clusters = int(
            n_clusters * np.sum(coarse_labels == i) * 1. / X.shape[0])
        n_small_clusters = np.maximum(1, n_small_clusters)
        mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_small_clusters,
                              batch_size=1000, n_init=10, max_no_improvement=10, verbose=0,
                              random_state=0).fit(X[coarse_labels == i])
        fine_labels[coarse_labels == i] = q + mbk.labels_
        q += n_small_clusters
    return fine_labels


def create_labels(X, mask, n_pieces, memory=Memory(cachedir=None)):
    """
    Separates input data into pieces

    Parameters
    ----------
    X: numpy array of shape (n_samples, n_features)
        input data
    mask: numpy array
        the mask from which X is produced
    n_pieces: int
        number of different labels

    Returns
    -------
    labels: numpy array of shape (n_samples)
        labels[i] is the label of array i
        (0 <= labels[i] < n_samples)
    """
    """
    shape = mask.shape
    connectivity = grid_to_graph(*shape, mask=mask).tocsc()
    ward = AgglomerativeClustering(
        n_clusters=n_pieces, connectivity=connectivity, memory=memory)
    ward.fit(X)
    labels = ward.labels_
    """
    labels = hierarchical_k_means(X, n_pieces)
    return labels


def generate_Xi(labels_values, labels, X_list, mean_X):
    for k in range(len(labels_values)):
        X_i_list = []
        label = labels_values[k]
        i = label == labels
        if (k + 1) % 25 == 0:
            print("Fitting area: " + str(k + 1) +
                  "/" + str(len(labels_values)))
        for X_ in X_list:
            X_i_list.append(X_[i])
        yield X_i_list, mean_X[i]


def make_template_one_parcellation(X_list, mean_X, mask, n_pieces, metric, regularization_param, train_index, joint_clustering, mem, n_iter=4, n_jobs=1, verbose=True, infos=None):
    """Do one parcellation + call of fit on the data"""

    if n_pieces > 1:
        if joint_clustering:
            clustering_data = []
            for X_ in X_list:
                clustering_data.append(X_[:, train_index])
            clustering_data = np.hstack(clustering_data)
        else:
            clustering_data = X_list[0][:, train_index]
        labels = create_labels(clustering_data, mask, n_pieces, memory=mem)
    else:
        labels = np.zeros(mask.sum())

    unique_labels, counts = np.unique(labels, return_counts=True)
    print(counts)
    fit_list = Parallel(n_jobs, backend="threading", verbose=verbose)(
        delayed(make_template_piece)(
            X_i_list, mean_X_i, reg=regularization_param, metric=metric, n_iter=n_iter
        ) for X_i_list, mean_X_i in generate_Xi(unique_labels, labels, X_list, mean_X))

    template = [fit[0] for fit in fit_list]
    template_history = [fit[1] for fit in fit_list]

    return labels, template, template_history


def map_template_to_image(img, train_index, template, n_pieces, mapping_method, mapping_bags, masker, n_jobs):
    mapping_image = index_img(img, train_index)
    if mapping_method == "optimal_transport":
        mapping = PieceWiseAlignmentOT(n_pieces=n_pieces, method="epsilon_scaling", metric="euclidean", reg=.1, n_bootstrap=mapping_bags,
                                       mask=masker, n_jobs=n_jobs)
    else:
        mapping = PieceWiseAlignment(
            n_pieces=n_pieces, method=mapping_method, n_bootstrap=mapping_bags, mask=masker, n_jobs=n_jobs)
    mapping.fit(mapping_image, img)
    return mapping


def predict_from_template_and_mapping(template, test_index,  mapping):
    image_to_transform = index_img(template, test_index)
    transformed_image = mapping.transform(image_to_transform)
    return transformed_image


class OptimalTransportTemplate(BaseEstimator, TransformerMixin):
    """
    Decompose the source and target images into source and target regions
     Use ot alignment algorithms to align source and target regions independantly.
    """

    def __init__(self, n_pieces=200, metric="euclidean", reg=10e-1, n_bootstrap=1,
                 joint_clustering=False,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Decompose the source and target images into source and target regions
        Use alignment algorithms to align source and target regions independantly.

        Parameters
        ----------
        n_pieces: int
            number of regions
        method: 'regularized' or 'exact'
        n_bootstrap: int, optional
            How many bootstrap averages are used. 1 by default
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing ised.
        """
        self.n_pieces = n_pieces
        self.metric = metric
        self.regularization_param = reg
        self.n_bootstrap = n_bootstrap

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs
        self.joint_clustering = joint_clustering

        self.verbose = verbose

        self.infos_ = None

    def fit(self, imgs, n_iter=4):
        """
        Fit data X and Y and learn transformation to map X to Y
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Y: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           target data
         Returns
        -------
        self
        """
        from sklearn.model_selection import ShuffleSplit
        '''
        self.masker_ = check_embedded_nifti_masker(self)
        self.masker_.n_jobs = self.n_jobs
        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit([imgs[0]])
        else:
            self.masker_.fit()
        self.mask_img_ = load_img(self.masker_.mask_img)
        for now mask is a masker
        '''
        X_list = []
        for img in imgs:
            X_ = self.mask.transform(img)
            if type(X_) == list:
                X_ = np.concatenate(X_, axis=0)
            X_ = X_.T
            X_list.append(X_)

        mean_X_ = np.mean(X_list, axis=0)

        '''
        labels = create_labels(
            X_, self.mask_img_.get_data(), self.n_pieces, memory=self.memory)
        labels_values = np.unique(labels)
        '''
        self.template, self.template_history, self.labels_, self.infos_ = [], [], [], []
        rs = ShuffleSplit(n_splits=self.n_bootstrap,
                          test_size=.8, random_state=0)

        outputs = Parallel(n_jobs=self.n_jobs)(
            delayed(make_template_one_parcellation)(
                X_list, mean_X_, self.mask.mask_img_.get_data(
                ), self.n_pieces, self.metric, self.regularization_param,
                train_index, self.joint_clustering, self.memory, n_jobs=self.n_jobs, verbose=self.verbose, n_iter=n_iter) for train_index, _ in rs.split(mean_X_.T))

        self.labels_ = [output[0] for output in outputs]
        self.template = [output[1] for output in outputs]
        self.template_history = [output[2] for output in outputs]

        return self

    def transform(self, imgs, train_index, test_index, mapping_method="optimal_transport", mapping_bags=1):
        """
        Predict data from X
        Parameters
        ----------
        imgs: List of Niimg-like objects
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data. Every img must have the same length (number of sample) as imgs used in the fit()
           and as the template.
        train_index : list of ints
            indexes of the 3D samples used to map each img to the template
        test_index : list of ints
            indexes of the 3D samples to predict from the template and the mapping
        Returns
        -------
        predicted_imgs: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           predicted data
           List of 4D images, each of them has the same length as the list test_index
        """

        fitted_mappings = Parallel(self.n_jobs, backend="threading", verbose=self.verbose)(
            delayed(map_template_to_image)(
                img, train_index, self.template, self.n_pieces, mapping_method, mapping_bags, self.mask, self.n_jobs
            ) for img in imgs
        )

        predicted_imgs = Parallel(self.n_jobs, backend="threading", verbose=self.verbose)(
            delayed(predict_from_template_and_mapping)(self.template, test_index, mapping
                                                       ) for mapping in fitted_mappings
        )
        return predicted_imgs
