import numpy as np
import pandas as pd
import logging
import sys
from glob import glob
import seaborn as sns
import matplotlib.pyplot as plt

log = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
log.addHandler(out_hdlr)
log.setLevel(logging.INFO)

result_directory = "/storage/workspace/hrichard/masked_data/results/"
res = []
for config in ["gallant", "forrest", "raiders", "sherlock"]:
    for n_components in [5, 10, 20, 50, 100]:
        algorithm_names = [
            "ProbSRM",
            "FastSRM_modl_1024",
            "FastSRM_basc_444",
            "FastSRM_shaeffer_800",
            "FastSRM_modl_512",
            "DetSRM"
        ]
        log.info("Number of components: %i" % n_components)

        mean_res = {}
        for name in algorithm_names:
            paths = glob(result_directory +
                            config +
                            "atlas_vs_prob" +
                            "_algo" +
                            name +
                            "components_%i" % n_components +
                            "subject_*")
            mean_res[name] = np.mean([np.load(path)
                                for path in paths], axis=0)

        mask = mean_res["ProbSRM"] > 0.05

        for name in algorithm_names:
            res.append([config, n_components, name, np.mean(mean_res[name][mask])])
res = pd.DataFrame(res, columns=["dataset", "number of components", "algo", "mean_r2"])

for config in ["gallant", "forrest", "raiders", "sherlock"]:
    plt.figure()
    sns.lineplot(x="number of components", y="mean_r2", data=res[res["dataset"] == config], hue="algo")
    plt.savefig("/home/parietal/hrichard/hugo-richard-M2/"
                        "neuroimage/figures/perfs_" + config + ".pdf")

