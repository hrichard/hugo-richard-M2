import numpy as np
import scipy.sparse as sp
import scipy.stats as stats
import matplotlib.pyplot as plt


def create_orthogonal_matrix(rows, cols):
    """
    Creates matrix W with orthogonal columns:
    W.T.dot(W) = I
    Parameters
    ----------
    rows: int
        number of rows
    cols: int
        number of columns
    Returns
    ---------
    Matrix W of shape (rows, cols) such that W.T.dot(W) = T
    """
    if cols > rows:
        return np.eye(rows, rows)
    else:
        return np.eye(rows, cols)


def generate_random_orthogonal_matrix(rows, cols, random_state=None):
    """
    Creates matrix W with orthogonal columns:
    W.T.dot(W) = I
    Parameters
    ----------
    rows: int
        number of rows
    cols: int
        number of columns
    random_state : int or RandomState
        Pseudo number generator state used for random sampling.
    Returns
    ---------
    Matrix W of shape (rows, cols) such that W.T.dot(W) = T
    """
    v = rows
    k = cols
    if random_state == None:
        rnd_matrix = np.random.rand(v, k)
    else:
        rnd_matrix = random_state.rand(v, k)
    q, r = np.linalg.qr(rnd_matrix)
    return q


def generate_group_sparse_matrix(n_voxels, n_components, n_splits=5, sparsity=0.5):
    """
    Generate a group sparse matrix
    Parameters
    ----------
    n_voxels
    n_components
    sparsity: float
        average proportion of zeros
    Returns
    -------
    X: ndarray of shape (n_voxels, n_components)
        sparse matrix
    """
    K = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(sparsity).rvs).toarray()
    M = np.zeros((n_voxels, n_components))

    split_sizes = np.zeros((n_splits, n_components))
    for k in range(n_components):
        for i in range(n_splits):
            split_sizes[i, k] = np.random.randint(5, 10)

    split_sizes = split_sizes / np.sum(split_sizes, axis=0, keepdims=True) * n_voxels
    split_sizes = np.cumsum(split_sizes, axis=0)
    split_sizes = np.round(split_sizes).astype(int)

    for k in range(n_components):
        for i in range(n_splits):
            if i ==0:
                start = 0
            else:
                start = split_sizes[i-1, k]
            stop = split_sizes[i, k]
            vv = stop - start
            variance = np.eye(vv)*0.1
            variance = variance.T.dot(variance)
            M[start:stop, k] = np.random.multivariate_normal(
                np.zeros(vv),
                variance
            ) * K[i, k]

    return M


def generate_close_group_sparse_matrices(n_voxels, n_components, sparsity=0.5, difference=0.05, n_splits=10, n_subjects=10):
    """
    Generate a group sparse matrix
    Parameters
    ----------
    n_voxels
    n_components
    sparsity: float
        average proportion of zeros
    Returns
    -------
    X: ndarray of shape (n_voxels, n_components)
        sparse matrix
    """

    K = []
    K_ = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(sparsity).rvs).toarray().astype(int)
    for n in range(n_subjects):
        diff = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(difference).rvs).toarray().astype(int)
        K.append(np.bitwise_xor(K_, diff))

    split_sizes = np.zeros((n_splits, n_components, n_subjects))
    for k in range(n_components):
        for i in range(n_splits):
            r_ki = np.random.randint(5, 10)
            for n in range(n_subjects):
                split_sizes[i, k, n] = r_ki + np.random.randint(-2, 2)

    split_sizes = split_sizes / np.sum(split_sizes, axis=0, keepdims=True) * n_voxels
    split_sizes = np.cumsum(split_sizes, axis=0)
    split_sizes = np.round(split_sizes).astype(int)

    Ms = []
    for n in range(n_subjects):
        M = np.zeros((n_voxels, n_components))
        for k in range(n_components):
            for i in range(n_splits):
                if i ==0:
                    start = 0
                else:
                    start = split_sizes[i-1, k, n]
                stop = split_sizes[i, k, n]
                vv = stop - start
                variance = np.eye(vv)*0.1
                variance = variance.T.dot(variance)
                M[start:stop, k] = np.random.multivariate_normal(
                    np.zeros(vv),
                    variance
                ) * K[n][i, k]
        Ms.append(M)

    return Ms


def generate_fake_data(noise_scale=None,
                       latent_variance=None,
                       n_subjects = 5,
                       n_components = 20,
                       n_voxels = 200,
                       n_timeframes = 500,
                       masker=None
                       ):
    train_noise = [np.random.multivariate_normal(
        np.zeros(n_voxels),
        noise_scale[i] * np.eye(n_voxels),
        n_timeframes
    ).T for i in range(n_subjects)]
    train_noise = [train_noise[i] - np.mean(train_noise[i], axis=1, keepdims=True) for i in range(n_subjects)]

    train_S = np.random.multivariate_normal(
        np.zeros(n_components),
        latent_variance,
        n_timeframes).T
    train_S = train_S - np.mean(train_S, axis=1, keepdims=True)

    train_W = {
        "orthogonal": [generate_random_orthogonal_matrix(n_voxels, n_components) for _ in range(n_subjects)],
        "random": [sp.random(
            n_voxels,
            n_components,
            1,
            data_rvs=stats.norm(loc=0, scale=0.1).rvs).toarray() for _ in range(n_subjects)],
        "sparse": [sp.random(
            n_voxels,
            n_components,
            0.5,
            data_rvs=stats.norm(loc=0, scale=0.1).rvs).toarray() for _ in range(n_subjects)],
        "group_sparse": [generate_group_sparse_matrix(n_voxels, n_components, sparsity=0.5, n_splits=10)
                        for _ in range(n_subjects)],
        "close_group_sparse": generate_close_group_sparse_matrices(n_voxels, n_components,
                                                                  sparsity=0.5, n_splits=10, n_subjects=n_subjects)
    }

    test_S = np.random.multivariate_normal(
        np.zeros(n_components),
        latent_variance,
        n_timeframes).T

    test_S = test_S - np.mean(test_S, axis=1, keepdims=True)

    test_noise = [np.random.multivariate_normal(
        np.zeros(n_voxels),
        noise_scale[i] * np.eye(n_voxels),
        n_timeframes
    ).T for i in range(n_subjects)]

    test_noise = [test_noise[i] - np.mean(test_noise[i], axis=1, keepdims=True) for i in range(n_subjects)]

    return {"train_noise": train_noise,
            "train_S": train_S,
            "train_W": train_W,
            "test_S": test_S,
            "test_noise": test_noise}
